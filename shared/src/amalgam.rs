
use std::{
	cmp::{
		Ordering,
		PartialOrd
	},
	hash::{
		Hash,
		Hasher,
	},
	io::{
		Cursor,
		Write
	}
};
use serde::{
	Deserialize,
	Serialize
};
use snap::{
	read::FrameDecoder,
	write::FrameEncoder
};
use crate::job::Job;

#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
pub struct PhaseOffsets {
	pub start_offset: i32,
	pub end_offset: i32
}

impl Default for PhaseOffsets {
	fn default() -> Self {
		PhaseOffsets {
			start_offset: 0,
			end_offset: 0
		}
	}
}

#[derive(Clone, Copy, Debug, Deserialize, Serialize)]
pub struct MechanicId {
	pub hash: u128,
	pub index: u32
}

impl MechanicId {
	pub fn create<F>(phase: &str, mechanic: &str, time: i32, generator: &mut F) -> Self where F: FnMut() -> u32 {
		let index = generator();
		let label = format!("{}-{}-{}", phase, mechanic, time);
		let hash = murmur3::murmur3_x64_128(&mut Cursor::new(label), 0).unwrap();
		MechanicId {
			hash,
			index
		}
	}
}

impl PartialEq for MechanicId {
	fn eq(&self, other: &Self) -> bool {
		self.hash == other.hash
	}
}

impl Eq for MechanicId {}

// needs to be PartialOrd for end shifting
impl PartialOrd for MechanicId {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		self.index.partial_cmp(&other.index)
	}
}

impl Hash for MechanicId {
	fn hash<H>(&self, state: &mut H) where H: Hasher {
		self.index.hash(state)
	}
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub enum MechanicTag {
	Physical,
	Magical,
	Darkness,
	Percent,
	Untargetable
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Mechanic {
	pub name: String,
	pub time: i32,
	pub tags: Vec<MechanicTag>,
	pub id: MechanicId
}

impl Mechanic {
	pub fn adjusted_time(&self, offsets: &PhaseOffsets) -> i32 {
		self.time + offsets.start_offset
	}
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Phase {
	pub name: String,
	pub enrage: i32,
	pub mechanics: Vec<Mechanic>
}

impl Phase {
	pub fn adjusted_end(&self, offsets: &PhaseOffsets) -> i32 {
		self.enrage + offsets.start_offset + offsets.end_offset
	}

	pub fn adjusted_enrage(&self, offsets: &PhaseOffsets) -> i32 {
		self.enrage + offsets.start_offset
	}
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Encounter {
	pub sync: u32,
	pub phases: Vec<Phase>
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct TimelineItem {
	pub ability: String,
	pub mechanic: MechanicId,
	pub time: i32
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Amalgam {
	pub preset: String,
	pub jobs: [Job; 8],
	pub items: [Vec<TimelineItem>; 8],
	pub filters: [bool; 3],
	pub offsets: Vec<PhaseOffsets>
}

impl Amalgam {
	pub fn compress(&self) -> Result<Vec<u8>, String> {
		// can't use to_writer because it doesn't seem possible to retrieve the value after
		let string = serde_json::to_string(&self);
		if let Err(e) = string {
			return Err(e.to_string());
		}

		let mut writer = FrameEncoder::new(Vec::<u8>::new());
		if let Err(e) = writer.write_all(string.unwrap().as_bytes()) {
			return Err(e.to_string());
		}

		writer.into_inner().map_err(|e| { e.error().to_string() })
	}

	pub fn decompress(compressed: Vec<u8>) -> Result<Self, String> {
		let reader = FrameDecoder::new(compressed.as_slice());
		serde_json::from_reader(reader).map_err(|e| { e.to_string() })
	}
}

// there are issues with the Serialize trait if it's a static variable
pub fn dummy() -> Amalgam {
	let id = MechanicId {
		hash: 0,
		index: 1
	};
	Amalgam {
		preset: "UCOB".to_string(),
		jobs: [Job::SCH, Job::DUM, Job::DUM, Job::DUM, Job::DUM, Job::DUM, Job::DUM, Job::DUM],
		items: [
			vec![
				TimelineItem {
					ability: "not reprisal".to_string(),
					mechanic: id,
					time: 10
				}
			],
			vec![],
			vec![],
			vec![],
			vec![],
			vec![],
			vec![],
			vec![],
		],
		filters: [true, false, false],
		offsets: vec![
			PhaseOffsets {
				start_offset: 5,
				end_offset: 7
			}
		]
	}
}

#[cfg(test)]
pub mod tests {
	use super::*;

	#[test]
	fn compressed_is_smaller() {
		let amalgam = dummy();
		let original = serde_json::to_vec(&amalgam).expect("Failed to serialize");
		let compressed = amalgam.compress().expect("Failed to compress");
		assert!(compressed.len() < original.len(), "Compressed size ({}) was not smaller than original size ({})", compressed.len(), original.len());
	}

	#[test]
	fn restores_value() {
		let original = dummy();
		let compressed = original.compress().expect("Failed to compress");
		let restored = Amalgam::decompress(compressed).expect("Failed to decompress");
		assert_eq!(original, restored);
	}
}
