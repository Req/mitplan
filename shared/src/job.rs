
use std::fmt::{
	self,
	Display,
	Formatter
};
use serde::{
	Deserialize,
	Serialize
};

#[derive(Clone, Copy, Debug, Deserialize, Eq, Hash, PartialEq, Serialize)]
pub enum Job {
	DUM,
	PLD, WAR, DRK, GNB,
	WHM, SCH, AST,
	MNK, DRG, NIN, SAM,
	BRD, MCH, DNC,
	BLM, SMN, RDM,
	Tank, Healer, Melee, Ranged, Caster,
	SGE, RPR, VPR, PCT
	// order must be preserved for proper deserialization. add new entries below here
}

impl Display for Job {
	fn fmt(&self, f: &mut Formatter) -> fmt::Result {
		write!(f, "{:?}", self)
	}
}

impl Default for Job {
	fn default() -> Self {
		Job::DUM
	}
}
