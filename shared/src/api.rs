
use serde::{
	Deserialize,
	Serialize
};
use crate::amalgam::Amalgam;

pub type AmalgamId = u32;

#[derive(Debug, Deserialize, PartialEq, Serialize)]
pub enum ApiResult<T> {
	Success(T),
	Error(String)
}

impl<T> ApiResult<T> {
	pub fn unwrap(self) -> T {
		match self {
			ApiResult::Success(value) => value,
			ApiResult::Error(error) => panic!("Tried to unwrap ApiResult::Error(\"{}\")", error)
		}
	}

	pub fn unwrap_err(self) -> String {
		match self {
			ApiResult::Success(_) => panic!("Tried to unwrap_err ApiResult::Success"),
			ApiResult::Error(error) => error
		}
	}
}

impl<T> ApiResult<T> {
	pub fn from(result: Result<T, String>) -> Self {
		match result {
			Ok(value) => ApiResult::Success(value),
			Err(e) => ApiResult::Error(e)
		}
	}
}

#[derive(Deserialize, Serialize)]
pub struct AmalgamWithPassword {
	pub amalgam: Amalgam,
	pub password: String
}

#[derive(Deserialize, Serialize)]
pub struct JustPassword {
	pub password: String
}
