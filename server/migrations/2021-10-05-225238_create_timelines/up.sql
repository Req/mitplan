
CREATE TABLE timelines (
	-- this is autoincremented by sqlite
	id INTEGER PRIMARY KEY NOT NULL,
	hashed_password TEXT NOT NULL,
	payload BLOB NOT NULL
);
