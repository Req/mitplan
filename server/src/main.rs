
#![feature(proc_macro_hygiene, decl_macro)]

#[macro_use]
extern crate diesel;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate rocket;
#[macro_use]
extern crate rocket_contrib;

mod api;
mod cli;
mod schema;

use std::path::Path;
use diesel::{
	connection::Connection,
	sqlite::SqliteConnection
};
use rocket::{
	config::Environment,
	fairing::{
		Fairing,
		Info,
		Kind
	},
	http::Header,
	Request,
	response::NamedFile,
	Response
};
use rocket_contrib::serve::StaticFiles;
use crate::{
	api::DbConn,
	cli::Cli
};

pub struct CORS;

impl Fairing for CORS {
	fn info(&self) -> Info {
		Info {
			name: "Makes CORS not be annoying when localhost:3000 requests from localhost:8000",
			kind: Kind::Response
		}
	}

	fn on_response(&self, _: &Request, response: &mut Response) {
		response.set_header(Header::new("Access-Control-Allow-Origin", "*"));
		response.set_header(Header::new("Access-Control-Allow-Methods", "POST, GET, PATCH, OPTIONS"));
		response.set_header(Header::new("Access-Control-Allow-Headers", "*"));
		response.set_header(Header::new("Access-Control-Allow-Credentials", "true"));
	}
}

const PUBLIC_PATH: &str = if cfg!(debug_assertions) {
	"./dist"
} else {
	"/home/public/deploy"
};

fn main() {
	if let Some(cli) = Cli::from_cli_args() {
		match cli {
			Ok(cli) => match SqliteConnection::establish("db.sqlite") {
				Ok(conn) => {
					if let Err(s) = cli.exec(conn) {
						println!("Command failed: {}", s);
					}
				},
				Err(e) => println!("Failed to open a database connection: {}", e)
			},
			Err(s) => println!("{}", s)
		}
		return;
	}

	let routes = routes![
		api::get_timeline,
		api::create_timeline,
		api::create_timeline_cors,
		api::edit_timeline,
		api::edit_timeline_cors,
		api::delete_timeline,
		api::delete_timeline_cors,
		index::edit,
		index::readonly
	];

	let mut rocket = rocket::ignite();
	let env = Environment::active();
	if env.is_err() {
		println!("bad env: {:?}", env.unwrap_err());
		return;
	}
	let is_dev = env.unwrap().is_dev();
	if is_dev {
		rocket = rocket.attach(CORS);
	}
	rocket
		.attach(DbConn::fairing())
		.mount("/", StaticFiles::from(PUBLIC_PATH))
		.mount("/", routes)
		.launch();
}

// garbage endpoints to make the server play nicely with the router agent
mod index {
	use super::*;

	fn serve_index() -> Option<NamedFile> {
		NamedFile::open(Path::new(PUBLIC_PATH).join("index.html")).ok()
	}

	#[get("/edit/<_id>")]
	pub fn edit(_id: u32) -> Option<NamedFile> {
		serve_index()
	}

	#[get("/readonly/<_id>")]
	pub fn readonly(_id: u32) -> Option<NamedFile> {
		serve_index()
	}
}
