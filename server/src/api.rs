
use std::sync::{
	Arc,
	Mutex,
};
use bcrypt::{
	DEFAULT_COST,
	hash,
	verify
};
use diesel::{
	prelude::*,
	result::Error,
	sqlite::SqliteConnection,
	sql_types::Integer
};
use rocket::response::status::NoContent;
use rocket_contrib::json::Json;
use serde::Deserialize;
use mitplan_shared::{
	amalgam::Amalgam,
	api::{
		// not using AmalgamId because of the schema
		AmalgamWithPassword,
		ApiResult,
		JustPassword
	}
};
use crate::schema::timelines;

// possibly useful in the future
// https://stackoverflow.com/questions/57123453/how-to-use-diesel-with-sqlite-connections-and-avoid-database-is-locked-type-of

#[database("sqlite_timelines")]
pub struct DbConn(SqliteConnection);

#[derive(Deserialize, Queryable)]
pub struct Timeline {
	pub id: i32,
	pub hashed_password: String,
	pub payload: Vec<u8>
}

fn get_timeline_impl(conn: &SqliteConnection, key: i32) -> Result<Amalgam, String> {
	match timelines::table.find(key).get_result::<Timeline>(conn) {
		Ok(timeline) => {
			let result = Amalgam::decompress(timeline.payload);
			if let Err(e) = result {
				return Err(format!("Failed to decompress stored timeline - {}", e));
			}
			Ok(result.unwrap())
		},
		Err(_) => Err(format!("Timeline '{}' not found", key))
	}
}

/**
 * Retrieves a stored timeline using the given ID.
 * Returns an error if:
 * - No such timeline exists.
 * - The specified timeline could not be loaded.
 */
#[get("/api/timeline/<key>")]
pub fn get_timeline(conn: DbConn, key: i32) -> Json<ApiResult<Amalgam>> {
	Json(ApiResult::from(get_timeline_impl(&*conn, key)))
}

fn load_timeline_authorized(conn: &SqliteConnection, key: i32, password: &str) -> Result<Timeline, String> {
	let timeline = timelines::table.find(key).get_result::<Timeline>(conn);
	if timeline.is_err() {
		return Err(format!("Timeline '{}' not found", key));
	}

	let timeline = timeline.unwrap();
	match verify(password, &timeline.hashed_password) {
		Err(e) => Err(format!("Failed to hash password - {}", e.to_string())),
		Ok(same) => {
			if !same {
				return Err("Incorrect password".to_string());
			}
			Ok(timeline)
		}
	}
}

fn edit_timeline_impl(conn: &SqliteConnection, key: i32, data: AmalgamWithPassword) -> Result<(), String> {
	let _authorization = load_timeline_authorized(conn, key, &data.password)?;

	let payload = data.amalgam.compress();
	if let Err(e) = payload {
		return Err(format!("Failed to compress timeline - {}", e));
	}
	let result =
		diesel::update(timelines::table.filter(timelines::id.eq(key)))
			.set(timelines::payload.eq(payload.unwrap()))
			.execute(&*conn);
	if let Err(e) = result {
		Err(format!("Update query failed - {}", e))
	} else {
		Ok(())
	}
}

/**
 * Edits an existing timeline by replacing the payload.
 * Returns an error if:
 * - No such timeline exists.
 * - The replacement timeline could not be compressed.
 * - The password is incorrect.
 * - The query could not be fulfilled.
 */
#[post("/api/timeline/<key>/edit", format="json", data="<payload>")]
pub fn edit_timeline(conn: DbConn, key: i32, payload: Json<AmalgamWithPassword>) -> Json<ApiResult<()>> {
	Json(ApiResult::from(edit_timeline_impl(&*conn, key, payload.into_inner())))
}

#[options("/api/timeline/<_key>/edit")]
pub fn edit_timeline_cors(_key: i32) -> NoContent {
	NoContent
}

fn delete_timeline_impl(conn: &SqliteConnection, key: i32, data: JustPassword) -> Result<(), String> {
	let _authorization = load_timeline_authorized(conn, key, &data.password)?;

	match diesel::delete(timelines::table.filter(timelines::id.eq(key))).execute(&*conn) {
		Err(e) => Err(format!("Delete query failed - {}", e.to_string())),
		Ok(_) => Ok(())
	}
}

/**
 * Deletes an existing timeline.
 * Returns an error if:
 * - No such timeline exists.
 * - The password is incorrect.
 * - The query could not be fulfilled.
 */
#[post("/api/timeline/<key>/delete", format="json", data="<payload>")]
pub fn delete_timeline(conn: DbConn, key: i32, payload: Json<JustPassword>) -> Json<ApiResult<()>> {
	Json(ApiResult::from(delete_timeline_impl(&*conn, key, payload.into_inner())))
}

#[options("/api/timeline/<_key>/delete")]
pub fn delete_timeline_cors(_key: i32) -> NoContent {
	NoContent
}

lazy_static! {
	static ref MUTEX: Arc<Mutex<i8>> = Arc::new(Mutex::new(0));
}

no_arg_sql_function!(last_insert_rowid, Integer, "last_insert_rowid()");

fn insert_then_find_id(conn: &SqliteConnection, hashed_password: String, amalgam: Amalgam) -> Result<i32, String> {
	let payload = amalgam.compress()?;
	/*
	 * There's no support from the SQLite driver for get_result on an insert,
	 * and we can't know the ID until after inserting since it's autoincremented.
	 * So we have to insert and then select the last ID.
	 * The mutex prevents a second insert from happening before the select.
	 */
	let columns = (timelines::hashed_password.eq(hashed_password), timelines::payload.eq(payload));
	let guard = MUTEX.lock().unwrap();
	let mut message: Option<String> = None;
	let result = conn.transaction::<i32, Error, _>(|| {
		let result = diesel::insert_into(timelines::table).values(columns).execute(&*conn);
		if let Err(e) = result {
			message = Some(format!("Insert query failed - {}", e.to_string()));
			return Err(Error::RollbackTransaction);
		}
		let result = timelines::table.select(last_insert_rowid).limit(1).load(&*conn);
		if let Err(e) = result {
			message = Some(format!("Select query failed - {}", e.to_string()));
			return Err(Error::RollbackTransaction);
		}

		let values = result.unwrap();
		if values.len() != 1 {
			message = Some(format!("Unexpected result count ({})", values.len()));
			return Err(Error::RollbackTransaction);
		}

		Ok(values[0])
	});
	std::mem::drop(guard);

	result.map_err(|_| format!("Transaction failed - {}", message.unwrap()))
}

fn create_timeline_impl(conn: &SqliteConnection, data: AmalgamWithPassword) -> Result<i32, String> {
	if data.password.is_empty() {
		return Err("Empty password".to_string());
	}

	// i don't care about salting. this isn't sensitive data
	match hash(data.password, DEFAULT_COST) {
		Err(e) => Err(format!("Failed to hash password - {}", e.to_string())),
		Ok(hashed) => insert_then_find_id(conn, hashed, data.amalgam)
	}
}

/**
 * Creates a timeline.
 * Returns an error if:
 * - The password is empty.
 * - The password could not be hashed.
 * - The query could not be fulfilled.
 * On success, returns the ID of the newly-created timeline.
 */
#[post("/api/timeline/create", format="json", data="<payload>")]
pub fn create_timeline(conn: DbConn, payload: Json<AmalgamWithPassword>) -> Json<ApiResult<i32>> {
	Json(ApiResult::from(create_timeline_impl(&*conn, payload.into_inner())))
}

#[options("/api/timeline/create")]
pub fn create_timeline_cors() -> NoContent {
	NoContent
}

#[cfg(test)]
mod tests {
	use super::*;
	use std::{
		ffi::OsString,
		fs::{
			read_dir,
			read_to_string
		}
	};
	use diesel::dsl::sql;
	use mitplan_shared::{
		amalgam::dummy,
		job::Job
	};

	fn collect_migrations() -> Vec<String> {
		let mut migrations: Vec<(OsString, String)> = read_dir("./migrations").expect("Failed to traverse ./migrations").filter_map(|dir| {
			let path = dir.expect("Failed to traverse an entry of ./migrations").path();
			let date = path.file_name().unwrap().to_os_string();
			if !path.is_dir() {
				// realistically this only excludes .gitkeep
				return None;
			}

			let path = path.join("up.sql");
			let sql = read_to_string(&path).expect(&format!("Failed to read {:?}", path));
			Some((date, sql))
		}).collect();
		migrations.sort_by(|lhs, rhs| {
			lhs.0.partial_cmp(&rhs.0).unwrap()
		});
		migrations.into_iter().map(|pair| pair.1).collect()
	}

	lazy_static! {
		static ref MIGRATIONS: Vec<String> = collect_migrations();

		static ref DUMMY1: Amalgam = dummy();
		static ref DUMMY2: Amalgam = (|| {
			let mut amalgam = dummy();
			amalgam.jobs[5] = Job::DRK;
			amalgam
		})();

		static ref SEED_DATA: [(&'static str, Vec<u8>); 2] = [
			("password123", DUMMY1.compress().unwrap()),
			("password!23", DUMMY2.compress().unwrap()),
		];
	}

	struct TestDatabase {
		conn: SqliteConnection
	}

	impl TestDatabase {
		fn new() -> Self {
			let conn = SqliteConnection::establish(":memory:").expect("Failed to create an in-memory SQLite database");
			for migration in MIGRATIONS.iter() {
				sql::<()>(migration).execute(&conn).expect("Failed to run migrations");
			}
			Self {
				conn
			}
		}

		fn seed(&mut self) {
			for (password, blob) in SEED_DATA.iter() {
				let hashed = hash(password, DEFAULT_COST).expect("Failed to hash password");
				let columns = (timelines::hashed_password.eq(hashed), timelines::payload.eq(blob));
				diesel::insert_into(timelines::table).values(columns).execute(&self.conn).expect("Failed to insert seed data");
			}
		}

		fn new_seeded() -> Self {
			let mut db = Self::new();
			db.seed();
			db
		}
	}

	#[ignore]
	#[test]
	fn get_timeline() {
		let db = TestDatabase::new_seeded();

		// shouldn't find something that doesn't exist
		assert!(SEED_DATA.len() < 500);
		let result = get_timeline_impl(&db.conn, 500);
		assert!(result.is_err());
		assert!(result.unwrap_err().contains("not found"));

		// should retrieve the 2nd seeded row
		assert!(SEED_DATA.len() >= 2);
		let result = get_timeline_impl(&db.conn, 2);
		assert!(result.is_ok());
		assert_eq!(result.unwrap(), *DUMMY2);
	}

	#[ignore]
	#[test]
	fn edit_timeline() {
		let db = TestDatabase::new_seeded();

		// shouldn't find something that doesn't exist
		assert!(SEED_DATA.len() < 500);
		let result = edit_timeline_impl(&db.conn, 500, AmalgamWithPassword {
			amalgam: DUMMY1.clone(),
			password: "".to_string()
		});
		assert!(result.is_err());
		assert!(result.unwrap_err().contains("not found"));

		// should deny edits with the wrong password
		let result = edit_timeline_impl(&db.conn, 1, AmalgamWithPassword {
			amalgam: DUMMY2.clone(),
			password: SEED_DATA[1].0.to_string()
		});
		assert!(result.is_err());
		assert!(result.unwrap_err().contains("password"));

		// should accept edits with the correct password
		let result = edit_timeline_impl(&db.conn, 1, AmalgamWithPassword {
			amalgam: DUMMY2.clone(),
			password: SEED_DATA[0].0.to_string()
		});
		assert!(result.is_ok());
		let result = get_timeline_impl(&db.conn, 1);
		assert_eq!(result.unwrap(), *DUMMY2);
	}

	#[ignore]
	#[test]
	fn delete_timeline() {
		let db = TestDatabase::new_seeded();

		// shouldn't delete something with the wrong password
		let result = delete_timeline_impl(&db.conn, 1, JustPassword {
			password: SEED_DATA[1].0.to_string()
		});
		assert!(result.is_err());
		assert!(result.unwrap_err().contains("password"));

		// should delete with the correct password
		let result = delete_timeline_impl(&db.conn, 1, JustPassword {
			password: SEED_DATA[0].0.to_string()
		});
		assert!(result.is_ok());

		// deleted things should be gone
		let result = get_timeline_impl(&db.conn, 1);
		assert!(result.is_err());
		assert!(result.unwrap_err().contains("not found"));

		// shouldn't delete something that doesn't exist
		let result = delete_timeline_impl(&db.conn, 1, JustPassword {
			password: "".to_string()
		});
		assert!(result.is_err());
		assert!(result.unwrap_err().contains("not found"));
	}

	#[ignore]
	#[test]
	fn create_timeline() {
		let db = TestDatabase::new_seeded();

		// should create with valid arguments
		let result = create_timeline_impl(&db.conn, AmalgamWithPassword {
			password: "new password".to_string(),
			amalgam: DUMMY1.clone()
		});
		assert!(result.is_ok());

		// should respond with the same payload as it was created with
		let result = get_timeline_impl(&db.conn, 3);
		assert_eq!(result.unwrap(), *DUMMY1);

		let result = create_timeline_impl(&db.conn, AmalgamWithPassword {
			password: "".to_string(),
			amalgam: DUMMY1.clone()
		});
		assert!(result.is_err());
		assert!(result.unwrap_err().contains("password"));
	}
}
