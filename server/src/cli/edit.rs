
use diesel::{
	prelude::*,
	sqlite::SqliteConnection
};
use mitplan_shared::amalgam::Amalgam;
use crate::{
	api::Timeline,
	schema::timelines
};

#[derive(Debug, PartialEq)]
pub enum EditTarget {
	Preset(String, String)
}

fn edit_preset_impl(from: &str, to: &str, index: i32, count: i64, conn: &SqliteConnection) -> Result<i32, Option<String>> {
	let results = timelines::table.filter(timelines::id.gt(index))
		.order_by(timelines::id.asc())
		.limit(count)
		.load::<Timeline>(conn)
		.map_err(|e| Some(e.to_string()))?;
	if results.is_empty() {
		return Err(None);
	}
	let mut updated = 0;
	for result in results {
		let mut amalgam = Amalgam::decompress(result.payload).map_err(|s| Some(format!("Failed to decompress id {}: {}", result.id, s)))?;
		if amalgam.preset == from {
			amalgam.preset = to.to_string();
			let payload = amalgam.compress().map_err(|s| Some(s))?;
			diesel::update(timelines::table.filter(timelines::id.eq(result.id)))
				.set(timelines::payload.eq(payload))
				.execute(conn)
				.map_err(|e| Some(format!("Failed to update id {}: {}", result.id, e.to_string())))?;
			updated += 1;
		}
	}
	Ok(updated)
}

impl EditTarget {
	pub fn parse(args: &[String]) -> Result<Self, String> {
		if args.is_empty() {
			return Err(String::from("No arguments found for edit"));
		}
		match args[0].as_str() {
			"preset" => {
				if args.len() == 3 {
					Ok(Self::Preset(args[1].clone(), args[2].clone()))
				} else {
					Err(format!("Edit-preset requires 2 arguments (from, to), but {} were found", args.len() - 1))
				}
			},
			other => Err(format!("Unknown edit intent: {}", other))
		}
	}

	pub fn exec(self, conn: SqliteConnection) -> Result<(), String> {
		match self {
			Self::Preset(from, to) => {
				const STEP: i64 = 20;
				let mut index = -1;
				let mut updated = 0;
				let result = loop {
					match edit_preset_impl(&from, &to, index, STEP, &conn) {
						Ok(count) => {
							index += STEP as i32;
							updated += count;
						},
						Err(None) => break Ok(()),
						Err(Some(s)) => break Err(s)
					}
				};
				println!("Edited {} rows", updated);
				result
			}
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::cli::tests::wrap;

	#[test]
	fn parsing() {
		let args = &[];
		assert!(EditTarget::parse(args).is_err());

		let args = wrap(&["preset"]);
		assert!(EditTarget::parse(&args).is_err());

		let args = wrap(&["preset", "foo"]);
		assert!(EditTarget::parse(&args).is_err());

		let args = wrap(&["preset", "foo", "bar"]);
		let result = EditTarget::parse(&args);
		assert!(result.is_ok());
		assert_eq!(result.unwrap(), EditTarget::Preset(String::from("foo"), String::from("bar")));

		let args = wrap(&["foo", "bar", "baz"]);
		assert!(EditTarget::parse(&args).is_err());
	}
}
