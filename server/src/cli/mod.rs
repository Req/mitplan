
// ./server cli edit preset P4S P4S-1

mod edit;

use diesel::sqlite::SqliteConnection;
use crate::cli::edit::EditTarget;

#[derive(Debug, PartialEq)]
enum Intent {
	Edit(EditTarget)
}

impl Intent {
	fn parse(args: &[String]) -> Result<Self, String> {
		match args[0].as_str() {
			"edit" => {
				if args.len() == 1 {
					Err(format!("Edit requires arguments"))
				} else {
					EditTarget::parse(&args[1..]).map(|intent| Self::Edit(intent))
				}
			},
			other => Err(format!("Unknown intent: {}", other))
		}
	}

	fn exec(self, conn: SqliteConnection) -> Result<(), String> {
		match self {
			Self::Edit(target) => target.exec(conn)
		}
	}
}

#[derive(Debug, PartialEq)]
pub struct Cli {
	intent: Intent
}

impl Cli {
	pub fn from_cli_args() -> Option<Result<Self, String>> {
		let args: Vec<String> = std::env::args().collect();
		Self::from(&args)
	}

	pub fn from(args: &[String]) -> Option<Result<Self, String>> {
		if args.len() <= 1 || args[1] != "cli" {
			None
		} else if args[1] == "cli" {
			Some(
				if args.len() == 2 {
					Err(String::from("No arguments provided"))
				} else {
					Intent::parse(&args[2..]).map(|intent| Self { intent })
				}
			)
		} else {
			unreachable!("Somehow hit an illogical branch")
		}
	}

	pub fn exec(self, conn: SqliteConnection) -> Result<(), String> {
		self.intent.exec(conn)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	pub fn wrap(args: &[&str]) -> Vec<String> {
		args.iter().map(|s| s.to_string()).collect()
	}

	#[test]
	fn top_level_parsing() {
		let args = wrap(&["./"]);
		assert_eq!(Cli::from(&args), None);

		let args = wrap(&["./", "cli"]);
		let result = Cli::from(&args);
		assert!(result.is_some());
		assert!(result.unwrap().is_err());

		let args = wrap(&["./", "cli", "bad command"]);
		let result = Cli::from(&args);
		assert!(result.is_some());
		assert!(result.unwrap().is_err());

		let args = wrap(&["./", "cli", "edit"]);
		let result = Cli::from(&args);
		assert!(result.is_some());
		assert!(result.unwrap().is_err());
	}
}
