#!/bin/bash

set -e

cd sprite
cargo build
cargo run
cd ..

cd parser
cargo build
for file in presets/*.txt; do
	name=$(basename "${file}" | cut -f 1 -d '.')
	cargo run "presets/${name}.txt" "presets/${name}.json"
done
cd ..
mkdir -p ../server/dist
cp parser/presets/*.json ../server/dist
