
mod parser;

use std::{
	fs::File,
	io::{
		BufRead,
		BufReader
	}
};
use mitplan_shared::amalgam::{
	Encounter,
	Mechanic,
	MechanicId,
	Phase
};
use parser::{
	Entry,
	Outcome
};

const USAGE: &str = "Usage: parser path/to/input.txt path/to/output.json";

fn parse_args() -> Result<(String, String), &'static str> {
	let mut args: Vec<String> = std::env::args().collect();
	if args.len() != 3 {
		return Err(USAGE);
	}
	let input = args.remove(1);
	let output = args.remove(1);
	Ok((input, output))
}

fn generator() -> impl FnMut() -> u32 {
	let mut x = 0;
	move || {
		x += 1;
		x
	}
}

fn build_encounter(lines: &[String]) -> Result<Encounter, String> {
	const NO_SYNC: u32 = u32::MAX;
	const NO_ENRAGE: i32 = i32::MAX;
	let mut encounter = Encounter {
		sync: NO_SYNC,
		phases: vec![]
	};
	let mut generator = generator();
	let mut cumulative_time = 0;
	for (index, line) in lines.iter().enumerate() {
		match Entry::from_line(line) {
			Outcome::None => {},
			Outcome::Error(error) => return Err(format!("Line {}: {}", index + 1, error)),
			Outcome::Some(item) => match item {
				Entry::Sync(sync) => {
					if encounter.sync != NO_SYNC {
						return Err(format!("Line {}: sync is already set - cannot set again", index + 1));
					}
					encounter.sync = sync as u32;
				},
				Entry::Phase(name) => {
					if let Some(phase) = encounter.phases.last() {
						if phase.enrage == NO_ENRAGE {
							return Err(format!("Line {}: cannot start a new phase when no enrage is defined for the previous phase ({})", index + 1, phase.name));
						}
					}
					encounter.phases.push(Phase {
						name: name.to_string(),
						mechanics: vec![],
						enrage: NO_ENRAGE
					});
				},
				Entry::Mechanic(name, time, tags) => {
					if encounter.phases.is_empty() {
						return Err(format!("Line {}: cannot add a new mechanic when no phase has been declared", index + 1));
					}
					let phase = encounter.phases.last_mut().unwrap();
					phase.mechanics.push(Mechanic {
						name: name.to_string(),
						time: cumulative_time + time,
						tags,
						id: MechanicId::create(&phase.name, name.as_str(), time, &mut generator)
					});
				},
				Entry::Enrage(time) => {
					if let Some(phase) = encounter.phases.last_mut() {
						if phase.enrage != NO_ENRAGE {
							return Err(format!("Line {}: enrage for phase {} is already set - cannot set again", index + 1, phase.name));
						}
						cumulative_time += time;
						phase.enrage = cumulative_time;
					} else {
						return Err(format!("Line {}: global enrages are unsupported (no phase has been declared)", index + 1));
					}
				}
			}
		}
	}
	if encounter.sync == NO_SYNC {
		return Err(String::from("Failed to find a sync directive"));
	}
	Ok(encounter)
}

fn main() {
	let args = parse_args();
	if args.is_err() {
		println!("{}", args.unwrap_err());
		return;
	}
	let args = args.unwrap();

	let file = File::open(&args.0);
	if file.is_err() {
		println!("Cannot open {}", args.0);
		return;
	}
	let lines: Vec<String> = BufReader::new(file.unwrap()).lines().filter_map(|line| line.ok()).collect();
	let encounter = build_encounter(&lines);
	if encounter.is_err() {
		println!("Failed to parse timeline: {}", encounter.unwrap_err());
		return;
	}

	let file = File::create(&args.1);
	if file.is_err() {
		println!("Cannot create {}", args.1);
		return;
	}
	let result = serde_json::to_writer(&file.unwrap(), &encounter.unwrap());
	if result.is_err() {
		println!("Cannot write to {}: {}", args.1, result.unwrap_err());
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use mitplan_shared::amalgam::MechanicTag;

	#[test]
	fn calculates_time_cumulatively() {
		let result = build_encounter(&[
			"sync 80".to_string(),
			"phase 1".to_string(),
			"enrage 0:01".to_string(),
			"phase 2".to_string(),
			"mechanic @ 0:02".to_string(),
			"enrage 0:03".to_string(),
			"phase 3".to_string(),
			"mechanic @ 0:01".to_string(),
			"enrage 0:02".to_string()
		]);
		let encounter = result.unwrap();
		assert_eq!(encounter.phases[0].enrage, 1);
		assert_eq!(encounter.phases[1].enrage, 4);
		assert_eq!(encounter.phases[2].enrage, 6);
		assert_eq!(encounter.phases[1].mechanics[0].time, 3);
		assert_eq!(encounter.phases[2].mechanics[0].time, 5);
	}

	#[test]
	fn rejects_missing_sync() {
		let result = build_encounter(&[
			"phase 1".to_string(),
			"enrage 0:01".to_string()
		]);
		assert!(result.is_err());
		assert!(result.unwrap_err().contains("sync"));
	}

	#[test]
	fn rejects_bad_enrage() {
		let result = build_encounter(&["enrage 1:20".to_string()]);
		assert!(result.is_err());
		let error = result.unwrap_err();
		assert!(error.contains("phase"));
		assert!(error.contains("enrage"));
	}

	#[test]
	fn accepts_valid_timeline() {
		let lines: Vec<String> = [
			"sync 80",
			"phase 1",
			"Fluid Swing @ 0:10 [physical]",
			"Jagd Spawn @ 0:35",
			"Protean (Tornado) @ 0:38 [magical untargetable]",
			"enrage 2:23"
		].iter().map(|s| s.to_string()).collect();

		let result = build_encounter(&lines);
		assert!(result.is_ok());

		let mut generator = generator();
		assert_eq!(result.unwrap(), Encounter {
			sync: 80,
			phases: vec![
				Phase {
					name: "1".to_string(),
					enrage: 143,
					mechanics: vec![
						Mechanic {
							name: "Fluid Swing".to_string(),
							time: 10,
							tags: vec![MechanicTag::Physical],
							id: MechanicId::create("1", "Fluid Swing", 10, &mut generator)
						},
						Mechanic {
							name: "Jagd Spawn".to_string(),
							time: 35,
							tags: vec![],
							id: MechanicId::create("1", "Jagd Spawn", 35, &mut generator)
						},
						Mechanic {
							name: "Protean (Tornado)".to_string(),
							time: 38,
							tags: vec![MechanicTag::Magical, MechanicTag::Untargetable],
							id: MechanicId::create("1", "Protean (Tornado)", 38, &mut generator)
						}
					]
				}
			]
		});
	}
}
