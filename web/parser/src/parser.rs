
use mitplan_shared::amalgam::MechanicTag;
use parser_shared::parse_timestamp;

#[derive(Debug, PartialEq)]
enum Token<'a> {
	Word(&'a str),
	Timestamp(i32),
	At,
	OpenBracket,
	CloseBracket,
	Comment(&'a str)
}

pub enum Entry {
	Sync(u32),
	Phase(String),
	Mechanic(String, i32, Vec<MechanicTag>),
	Enrage(i32)
}

pub enum Outcome {
	Some(Entry),
	None,
	Error(String)
}

#[allow(unused)]
impl Outcome {
	pub fn is_some(&self) -> bool {
		matches!(self, Outcome::Some(..))
	}

	pub fn is_none(&self) -> bool {
		matches!(self, Outcome::None)
	}

	pub fn is_error(&self) -> bool {
		matches!(self, Outcome::Error(..))
	}

	pub fn unwrap(self) -> Entry {
		match self {
			Outcome::Some(value) => value,
			Outcome::None => panic!("Tried to unwrap a None value"),
			Outcome::Error(value) => panic!("Tried to unwrap an Error value ({})", value)
		}
	}

	pub fn unwrap_err(self) -> String {
		match self {
			Outcome::Error(value) => value,
			Outcome::Some(_) => panic!("Tried to unwrap_err a Some value"),
			Outcome::None => panic!("Tried to unwrap a None value")
		}
	}
}

fn tokenize<'a>(line: &'a str, mut tokens: Vec<Token<'a>>) -> Vec<Token<'a>> {
	if line.is_empty() {
		return tokens;
	}
	match line.chars().next().unwrap() {
		'@' => {
			tokens.push(Token::At);
			tokenize(&line[1..], tokens)
		},
		'[' => {
			tokens.push(Token::OpenBracket);
			tokenize(&line[1..], tokens)
		},
		']' => {
			tokens.push(Token::CloseBracket);
			tokenize(&line[1..], tokens)
		},
		'#' => {
			tokens.push(Token::Comment(line[1..].trim()));
			tokens
		},
		c => {
			if c.is_whitespace() {
				return tokenize(&line[1..], tokens);
			}
			if ('0'..='9').contains(&c) {
				// try parsing it as a timestamp before falling back to a word otherwise
				let potential_timestamp = line.split(char::is_whitespace).next().unwrap_or("");
				if let Ok(timestamp) = parse_timestamp(potential_timestamp) {
					let length = potential_timestamp.len();
					tokens.push(Token::Timestamp(timestamp as i32));
					return tokenize(&line[length..], tokens);
				}
			}
			// wasn't numerical or failed to parse as a timestamp, so this is a word
			let length = line.chars().take_while(|c| !c.is_whitespace() && !matches!(c, '@' | '[' | ']' | '#')).count();
			tokens.push(Token::Word(&line[..length]));
			tokenize(&line[length..], tokens)
		}
	}
}

fn uncomment(tokens: Vec<Token>) -> Vec<Token> {
	tokens.into_iter().filter(|t| !matches!(t, Token::Comment(..))).collect()
}

fn break_after_words<'a>(tokens: &'a [Token<'a>]) -> (&'a [Token<'a>], &'a [Token<'a>]) {
	let count = tokens.iter().take_while(|t| matches!(t, Token::Word(..))).count();
	(&tokens[..count], &tokens[count..])
}

fn reconstruct(tokens: &[Token]) -> String {
	tokens.iter().map(|t| match t {
		Token::Word(w) => w.to_string(),
		Token::Timestamp(time) => format!("{}:{:02}", time / 60, time % 60),
		Token::At => String::from("a"),
		Token::OpenBracket => String::from("["),
		Token::CloseBracket => String::from("]"),
		Token::Comment(c) => format!("# {}", c)
	}).collect::<Vec<String>>().join(" ")
}

fn try_tags(tokens: &[Token]) -> Result<Vec<MechanicTag>, String> {
	match tokens.get(0) {
		Some(Token::OpenBracket) => {
			let (unchecked_tags, rest) = break_after_words(&tokens[1..]);
			if rest.is_empty() {
				Err(String::from("Missing ']' following tag block"))
			} else if rest.len() > 1 || rest[0] != Token::CloseBracket {
				Err(format!("Unexpected tokens following tag block: {}", reconstruct(&rest[1..])))
			} else {
				let unchecked_tags = unchecked_tags.iter().map(|t| match t {
					Token::Word(w) => w,
					_ => unreachable!()
				});
				let mut tags = Vec::with_capacity(unchecked_tags.len());
				for tag in unchecked_tags {
					match *tag {
						"physical" => tags.push(MechanicTag::Physical),
						"magical" => tags.push(MechanicTag::Magical),
						"darkness" => tags.push(MechanicTag::Darkness),
						"percent" => tags.push(MechanicTag::Percent),
						"untargetable" => tags.push(MechanicTag::Untargetable),
						other => return Err(format!("Unrecognized mechanic tag: {}", other))
					}
				}
				Ok(tags)
			}
		},
		Some(other) => Err(format!("Found token ({:?}) after mechanic, but it failed to parse as a tag block", other)),
		None => Ok(vec![])
	}
}

impl Entry {
	pub fn from_line(line: &str) -> Outcome {
		if !line.is_ascii() {
			println!("Warning: this line contains non-ascii characters\n\t-> {}", line);
		}
		let line = line.trim();
		if line.is_empty() {
			return Outcome::None;
		}
		let tokens = uncomment(tokenize(line, Default::default()));
		if tokens.is_empty() {
			return Outcome::None;
		}
		match &tokens[0] {
			Token::At => Outcome::Error(String::from("Unexpected '@'")),
			Token::OpenBracket => Outcome::Error(String::from("Unexpected '['")),
			Token::CloseBracket => Outcome::Error(String::from("Unexpected ']'")),
			Token::Timestamp(_) => Outcome::Error(String::from("Unexpected timestamp at start of line")),
			// shouldn't happen because comments have been stripped, but here for completeness
			Token::Comment(_) => Outcome::None,
			Token::Word(w) => match *w {
				"sync" => {
					// expecting only a numerical word
					match tokens.get(1) {
						Some(Token::Word(x)) => {
							if let Ok(level) = x.parse::<u32>() {
								if let Some(bad) = tokens[2..].iter().find(|t| !matches!(t, Token::Comment(..))) {
									Outcome::Error(format!("Unexpected token following sync level: {:?}", bad))
								} else {
									Outcome::Some(Entry::Sync(level))
								}
							} else {
								Outcome::Error(format!("Expected level following 'sync', got '{}' (a word)", x))
							}
						},
						Some(other) => Outcome::Error(format!("Expected level following 'sync', got {:?}", other)),
						None => Outcome::Error(String::from("Expected level following 'sync'"))
					}
				},
				"phase" => {
					let (name, rest) = break_after_words(&tokens[1..]);
					if name.is_empty() {
						Outcome::Error(String::from("Expecting name following 'phase'"))
					} else if !rest.is_empty() {
						Outcome::Error(format!("Unexpected tokens following phase name: {}", reconstruct(rest)))
					} else {
						Outcome::Some(Entry::Phase(reconstruct(name)))
					}
				},
				"enrage" => {
					match tokens.get(1) {
						Some(Token::Timestamp(time)) => Outcome::Some(Entry::Enrage(*time)),
						Some(other) => Outcome::Error(format!("Expected timestamp (mm:ss) following 'enrage', got {:?}", other)),
						None => Outcome::Error(String::from("Expected timestamp (mm:ss) following 'enrage'"))
					}
				},
				_ => {
					let (name, rest) = break_after_words(&tokens);
					match rest.get(0) {
						Some(Token::At) => {
							match rest.get(1) {
								Some(Token::Timestamp(time)) => match try_tags(&rest[2..]) {
									Ok(tags) => Outcome::Some(Entry::Mechanic(reconstruct(name), *time, tags)),
									Err(e) => Outcome::Error(e)
								},
								Some(other) => Outcome::Error(format!("Expected timestamp following '@', got {:?}", other)),
								None => Outcome::Error(String::from("Expected timestamp following '@'"))
							}
						},
						Some(other) => Outcome::Error(format!("Expected '@' following mechanic name, got {:?}", other)),
						None => Outcome::Error(String::from("Expected '@' following mechanic name"))
					}
				}
			}
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn tokenizer() {
		assert_eq!(tokenize("", vec![]), vec![]);
		assert_eq!(tokenize("@", vec![]), vec![Token::At]);
		assert_eq!(tokenize("[", vec![]), vec![Token::OpenBracket]);
		assert_eq!(tokenize("]", vec![]), vec![Token::CloseBracket]);
		assert_eq!(tokenize("1:23", vec![]), vec![Token::Timestamp(83)]);
		assert_eq!(tokenize("01:23", vec![]), vec![Token::Timestamp(83)]);
		assert_eq!(tokenize("001:23", vec![]), vec![Token::Timestamp(83)]);
		assert_eq!(tokenize("1:023", vec![]), vec![Token::Word("1:023")]);
		assert_eq!(tokenize("#foo", vec![]), vec![Token::Comment("foo")]);
		assert_eq!(tokenize("# 1:23 ", vec![]), vec![Token::Comment("1:23")]);

		assert_eq!(tokenize("foo bar @ 2:45 [a tag]", vec![]), vec![
			Token::Word("foo"),
			Token::Word("bar"),
			Token::At,
			Token::Timestamp(165),
			Token::OpenBracket,
			Token::Word("a"),
			Token::Word("tag"),
			Token::CloseBracket
		]);
	}

	#[test]
	fn parse_sync() {
		assert!(Entry::from_line("sync abc").unwrap_err().contains("sync"));
		assert!(Entry::from_line("sync").unwrap_err().contains("level"));
		assert!(matches!(Entry::from_line("sync 123").unwrap(), Entry::Sync(123)));
	}

	#[test]
	fn parse_phase() {
		assert!(Entry::from_line("phase").unwrap_err().contains("phase"));
		let _expected = Entry::Phase(String::from("abc123"));
		assert!(matches!(Entry::from_line("phase abc123").unwrap(), _expected));
	}

	#[test]
	fn parse_enrage() {
		assert!(Entry::from_line("enrage abc").unwrap_err().contains("timestamp"));
		assert!(Entry::from_line("enrage 123").unwrap_err().contains("timestamp"));
		assert!(Entry::from_line("enrage 1:1").unwrap_err().contains("timestamp"));
		assert!(Entry::from_line("enrage").unwrap_err().contains("timestamp"));
		assert!(matches!(Entry::from_line("enrage 10:23").unwrap(), Entry::Enrage(623)));
	}

	#[test]
	fn parse_basic_mechanic() {
		assert!(Entry::from_line("name 10:23").is_error());
		assert!(Entry::from_line("name @ ").unwrap_err().contains("timestamp"));
		assert!(Entry::from_line("name @ 1023").unwrap_err().contains("timestamp"));
		let _none: Vec<MechanicTag> = vec![];
		let _name = String::from("name");
		assert!(matches!(Entry::from_line("name @ 10:23").unwrap(), Entry::Mechanic(_name, 623, _none)));
	}

	#[test]
	fn parse_tagged_mechanic() {
		assert!(Entry::from_line("name @ 10:23 [").unwrap_err().contains(']'));
		assert!(Entry::from_line("name @ 10:23 [foo]").unwrap_err().contains("tag"));
		let mut _tags: Vec<MechanicTag> = vec![];
		let _name = String::from("name");
		assert!(matches!(Entry::from_line("name @ 10:23 []").unwrap(), Entry::Mechanic(_name, 623, _tags)));
		_tags.push(MechanicTag::Physical);
		assert!(matches!(Entry::from_line("name @ 10:23 [physical]").unwrap(), Entry::Mechanic(_name, 623, _tags)));
		_tags.push(MechanicTag::Magical);
		assert!(matches!(Entry::from_line("name @ 10:23 [physical magical]").unwrap(), Entry::Mechanic(_name, 623, _tags)));
	}
}
