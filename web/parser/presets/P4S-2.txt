sync 90

phase 1
Searing Stream @ 0:16 [magical]
Akanthai: Act 1 @ 0:31
Searing Stream @ 0:40  [magical]
Wreath of Thorns @ 0:55
Hemitheos's Fire IV @ 0:58 [magical untargetable]
Hemitheos's Thunder III (Towers) @ 1:01  [magical untargetable]
Hemitheos's Fire IV @ 1:04 [magical untargetable]
Nearsight/Farsight @ 1:11 [magical]
Akanthai: Act 2 @ 1:23
Demigod Double @ 1:32 [magical]
Wreath of Thorns @ 1:42
Hemitheos's Dark IV (Tether) @ 1:50  [magical untargetable]
Dark Design @ 1:54
Hemitheos's Thunder III (Towers) + Hemitheos's Fire IV @ 1:55  [magical untargetable]
Hemitheos's Fire III (Tether) @ 1:56  [magical untargetable]
Hemitheos's Thunder III (Towers) + Hemitheos's Fire IV @ 2:02  [magical untargetable]
Hemitheos's Fire III (Tether) @ 2:03  [magical untargetable]
Hemitheos's Aero III (Tether) @ 2:04
Ultimate Impulse @ 2:10  [magical]
Akanthai: Act 3 @ 2:23
Wreath of Thorns @ 2:35
Kothornos Kick @ 2:43 [physical]
Kothornos Quake @ 2:47  [magical]
Hemitheos's Thunder III (Towers) @ 2:48  [magical]
Hemitheos's Water IV @  2:50  [magical untargetable]
Kothornos Kick @ 2:53 [physical]
Hemitheos's Thunder III (Towers) @ 2:54  [magical untargetable]
Kothornos Quake @ 2:58  [magical]
Nearsight/Farsight @ 3:08 [magical]
Heart Stake (1) @ 3:22  [magical]
Heart Stake (2) @ 3:25  [magical]
Akanthai: Act 4 @ 3:34
Searing Stream @ 3:43  [magical]
Wreath of Thorns @ 3:53
Searing Stream @ 4:01  [magical]
Hemitheos's Water III (Tether) @ 4:02  [magical untargetable]
Hemitheos's Thunder III (Towers) @ 4:03  [magical untargetable]
Hemitheos's Dark (Tether) (1) @ 4:10  [magical untargetable]
Hemitheos's Dark (Tether) (2) @ 4:16  [magical untargetable]
Hemitheos's Dark (Tether) (3) @ 4:20  [magical untargetable]
Hemitheos's Dark (Tether) (4) @ 4:27  [magical untargetable]
Ultimate Impulse @ 4:36 [magical]
Searing Stream @ 4:50  [magical]
Akanthai: Finale @ 4:59
Wreath of Thorns @ 5:09
Fleeting Impulse (1) @ 5:17  [magical]
Fleeting Impulse (2) @ 5:18  [magical]
Fleeting Impulse (3) @ 5:19  [magical]
Fleeting Impulse (4) @ 5:21  [magical]
Fleeting Impulse (5) @ 5:22  [magical]
Fleeting Impulse (6) @ 5:23  [magical]
Fleeting Impulse (7) @ 5:24  [magical]
Fleeting Impulse (8) @ 5:25  [magical]
Hemitheos's Aero III (Tether) @ 5:27
Wreath of Thorns @ 5:36
Hemitheos's Thunder IV (1) @ 5:39  [magical untargetable]
Hemitheos's Thunder IV (2) @ 5:40  [magical untargetable]
Hemitheos's Thunder IV (3) @ 5:42  [magical untargetable]
Hemitheos's Thunder IV (4) @ 5:43  [magical untargetable]
Hemitheos's Thunder IV (5) @ 5:44  [magical untargetable]
Hemitheos's Thunder IV (6) @ 5:46  [magical untargetable]
Hemitheos's Thunder IV (7) @ 5:47  [magical untargetable]
Hemitheos's Thunder IV (8) @ 5:48  [magical untargetable]
Nearsight/Farsight @ 5:53 [magical]
Searing Stream @ 6:05  [magical]
Demigod Double @ 6:15 [magical]
Akanthai: Curtain Call @ 6:28
Hemitheos's Dark IV (Tether) (1) @ 6:40  [magical untargetable]
Hell's Sting (1) @ 6:41
Hell's Sting (2) @ 6:43
Hemitheos's Dark IV (Tether) (2) @ 6:44  [magical untargetable]
Hemitheos's Dark IV (Tether) (3) @ 6:49  [magical untargetable]
Hemitheos's Dark IV (Tether) (4) @ 6:54  [magical untargetable]
Hemitheos's Dark IV (Tether) (5) @ 7:00  [magical untargetable]
Hell's Sting (1) @ 7:01
Hemitheos's Dark IV (Tether) (6) @ 7:02  [magical untargetable]
Hell's Sting (2) @ 7:03
Hemitheos's Dark IV (Tether) (7) @ 7:07  [magical untargetable]
Hemitheos's Dark IV (Tether) (8) @ 7:14  [magical untargetable]
Ultimate Impulse @ 7:20 [magical]
Hell's Sting (1) @ 7:31
Hemitheos's Dark IV (Tether) (1) @ 7:32  [magical untargetable]
Hell's Sting (2) @ 7:33
Hemitheos's Dark IV (Tether) (2) @ 7:36  [magical untargetable]
Hemitheos's Dark IV (Tether) (3) @ 7:40  [magical untargetable]
Hemitheos's Dark IV (Tether) (4) @ 7:45  [magical untargetable]
Hemitheos's Dark IV (Tether) (5) @ 7:49  [magical untargetable]
Hell's Sting (1) @ 7:51
Hemitheos's Dark IV (Tether) (6) @ 7:52  [magical untargetable]
Hell's Sting (2) @ 7:53
Hemitheos's Dark IV (Tether) (7) @ 7:54  [magical untargetable]
Hemitheos's Dark IV (Tether) (8) @ 8:04  [magical untargetable]
Ultimate Impulse @ 8:10 [magical]
enrage 8:25
