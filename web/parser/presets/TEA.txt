sync 80

phase Living Liquid
Fluid Swing @ 0:10 [physical]
Cascade @ 0:18 [magical]
Jagd Spawn @ 0:35
Fluid Swing/Strike @ 0:37 [physical]
Protean (Tornado) @ 0:38 [magical untargetable]
Exhaust @ 0:39 [magical untargetable]
Exhaust @ 0:49 [magical untargetable]
Fluid Swing/Strike @ 0:55 [physical]
Protean Wave @ 1:11 [magical]
Protean Wave @ 1:14 [magical]
Splash @ 1:20 [magical]
Drainage @ 1:25 [magical]
Cascade @ 1:32 [magical]
Protean Wave @ 1:48 [magical untargetable]
Protean Wave @ 1:51 [magical]
Protean Wave @ 1:54 [magical untargetable]
Splash @ 2:07 [magical]
Fluid Swing/Strike @ 2:15 [physical]
enrage 2:23

phase Limit Cut
enrage 0:31

phase BJ/CC
J-Kick @ 0:00 [magical untargetable]
Whirlwind @ 0:12 [magical]
Judgment Nisi @ 0:15
Link Up @ 0:21
Optical Sight @ 0:30
Photon @ 0:37
Spin Crusher @ 0:47
Water/Lightning 1 @ 0:50 [magical untargetable]
Missile Command @ 0:59 [magical untargetable]
Enumeration @ 1:07
Water/Lightning 2 @ 1:21 [magical untargetable]
Verdict @ 1:23
Limit Cut @ 1:28
Flarethrower @ 1:30 [magical]
Whirlwind @ 1:42 [magical]
Water/Lightning 3 @ 1:51 [magical untargetable]
Propeller Wind @ 2:09
Gavel @ 2:12
Photon @ 2:22
Double Rocket Punch @ 2:30 [physical]
Super Jump @ 2:37 [physical]
Whirlwind @ 2:50
Whirlwind @ 2:58
enrage 3:14

phase Alexander
# ~16s from bosses leaving until stasis hits
Temporal Stasis @ 0:16
Chastening Heat @ 0:27 [magical]
Divine Spear @ 0:31 [magical]
Divine Spear @ 0:33 [magical]
Divine Spear @ 0:35 [magical]
Inception Formation @ 0:44
# perhaps we need an [unshieldable] attribute as well
Heart Moves @ 1:11 [magical untargetable]
Flarethrower @ 1:16 [magical untargetable]
Flarethrower @ 1:18 [magical untargetable]
Flarethrower @ 1:20 [magical untargetable]
Debuffs Resolve @ 1:35 [magical untargetable]
Super Jump @ 1:44 [physical untargetable]
Chastening Heat @ 1:56 [magical]
Divine Spear @ 2:01 [magical]
Divine Spear @ 2:03 [magical]
Divine Spear @ 2:05 [magical]
Wormhole Formation @ 2:14
# in lieu of listing everything during wormhole i'm just listing puddle soaks
# you wouldn't assign something like "sacred soil #3" anyway
Repentance @ 2:45 [physical magical untargetable]
Repentance @ 2:49 [physical magical untargetable]
Repentance @ 2:53 [physical magical untargetable]
Incinerating Heat @ 2:59 [magical untargetable]
Mega Holy @ 3:12 [magical]
Mega Holy @ 3:20 [magical]
J Storm @ 3:37 [magical untargetable]
J Wave (1) @ 3:42 [magical]
J Wave (2) @ 3:45 [magical]
J Wave (3) @ 3:48 [magical]
J Wave (4) @ 3:51 [magical]
J Wave (5) @ 3:54 [magical]
J Wave (6) @ 3:57 [magical]
J Wave (7) @ 4:00 [magical]
J Wave (8) @ 4:03 [magical]
J Wave (9) @ 4:06 [magical]
J Wave (10) @ 4:09 [magical]
J Wave (11) @ 4:12 [magical]
J Wave (12) @ 4:15 [magical]
J Wave (13) @ 4:18 [magical]
J Wave (14) @ 4:21 [magical]
J Wave (15) @ 4:24 [magical]
# there are more but healing past 15 is pretty unreasonable
enrage 4:52

phase Perfect Alexander
# 15s from cast to stun + 57s from stun to targetable = 1:12 downtime
The Final Word @ 1:25
Optical Sight @ 1:52 [magical]
Optical Sight @ 1:58 [magical]
Fate Calibration Alpha @ 2:38
Debuffs Resolve @ 2:49 [magical untargetable]
Ordained Capital Punishment @ 3:06 [magical]
Ordained Capital Punishment @ 3:07 [magical]
Ordained Capital Punishment @ 3:08 [magical]
Ordained Punishment @ 3:12 [magical]
Fate Calibration Beta @ 4:00
# marked as magical for things like illumination precasts
Final Word Activates @ 4:06 [magical untargetable]
Debuffs Resolve @ 4:12 [magical untargetable]
J Jumps @ 4:13 [physical untargetable]
Optical Sight @ 4:17 [magical untargetable]
Ordained Capital Punishment @ 4:40 [magical]
Ordained Capital Punishment @ 4:41 [magical]
Ordained Capital Punishment @ 4:42 [magical]
Ordained Punishment @ 4:46 [magical]
Irresistable Grace @ 5:13 [magical]
Ordained Capital Punishment @ 5:26 [magical]
Ordained Capital Punishment @ 5:27 [magical]
Ordained Capital Punishment @ 5:28 [magical]
Ordained Punishment @ 5:32 [magical]
Irresistable Grace @ 5:59 [magical]
enrage 7:09
