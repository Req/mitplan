
use js_sys::RegExp;
use wasm_bindgen::JsValue;

pub fn parse_timestamp(s: &str) -> Result<u32, String> {
	let regex = RegExp::new("(?:^([0-9]+):([0-9]{2})$)", "");
	if let Some(parts) = regex.exec(s) {
		let parts = parts.iter().filter_map(|part: JsValue| part.as_string()).collect::<Vec<String>>();
		println!("{:?}", parts);
		if parts.len() >= 3 {
			if let Ok(minutes) = parts[1].parse::<u32>() {
				if let Ok(seconds) = parts[2].parse::<u32>() {
					return Ok(minutes * 60 + seconds);
				}
			}
		}
	}
	Err(format!("Invalid timestamp format (expecting mm:ss, found {})", s))
}
