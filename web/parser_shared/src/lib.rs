
#[cfg_attr(not(target_arch = "wasm32"), macro_use)]
extern crate lazy_static;

/*
 * The regex crate adds ~750kb to the wasm binary in release mode.
 * That's 0.75mb that the user shouldn't have to download because JS
 * has native regex support. On wasm targets this uses wasm bindings
 * and on regular targets it uses the regex crate.
 */
#[cfg_attr(target_arch = "wasm32", path = "wasm.rs")]
#[cfg_attr(not(target_arch = "wasm32"), path = "std.rs")]
mod internal;
pub use internal::*;

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn timestamp() {
		assert!(matches!(parse_timestamp("1:23"), Ok(83)));
		assert!(matches!(parse_timestamp("01:23"), Ok(83)));
		assert!(matches!(parse_timestamp("012:34"), Ok(754)));
		assert!(parse_timestamp(":23").is_err());
		assert!(parse_timestamp("1:2").is_err());
		assert!(parse_timestamp(":123").is_err());
		assert!(parse_timestamp("0123").is_err());
		assert!(parse_timestamp("01:234").is_err());
	}
}
