
use regex::Regex;

lazy_static! {
	static ref TIMESTAMP_REGEX: Regex = Regex::new(r"(?:^([0-9]+):([0-9]{2})$)").unwrap();
}

pub fn parse_timestamp(s: &str) -> Result<u32, String> {
	if let Some(parts) = TIMESTAMP_REGEX.captures(s) {
		let minutes = parts.get(1).unwrap().as_str().parse::<u32>().unwrap();
		let seconds = parts.get(2).unwrap().as_str().parse::<u32>().unwrap();
		return Ok(minutes * 60 + seconds);
	}
	Err(format!("Invalid timestamp format (expecting mm:ss, found {})", s))
}
