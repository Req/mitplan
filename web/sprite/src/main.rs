
use std::{
	fs::write,
	path::Path
};
use image::{
	DynamicImage,
	imageops::{
		FilterType,
		overlay,
		resize
	},
	RgbaImage
};

struct Manifest {
	name: &'static str,
	input_directory: &'static str,
	output_directory: &'static str,
	scale_to: (u32, u32)
}

impl Manifest {
	fn init(self) -> Sprite {
		let files = std::fs::read_dir(self.input_directory).unwrap().map(|file| file.unwrap().path());
		let segments = files.into_iter().map(|input|
			Segment {
				name: input.file_stem().unwrap().to_str().unwrap().to_string(),
				image: image::open(input).unwrap()
			}
		).collect();

		Sprite {
			manifest: self,
			segments
		}
	}
}

struct Segment {
	name: String,
	image: DynamicImage
}

struct Sprite {
	manifest: Manifest,
	segments: Vec<Segment>
}

impl Sprite {
	fn generate(&self) {
		let output = Path::new(self.manifest.output_directory);

		let css = self.make_css();
		write(output.join(Path::new(&format!("{}.scss", self.manifest.name))), css).unwrap();

		let map = self.make_map();
		map.save(output.join(Path::new(&format!("{}.png", self.manifest.name)))).unwrap();
	}

	fn make_css(&self) -> String {
		self.segments.iter().enumerate().map(|(index, segment)| {
			[
				format!(".pseudo-image[alt=\"{}\"] {{", segment.name),
				format!("	width: {}px;", self.manifest.scale_to.0),
				format!("	height: {}px;", self.manifest.scale_to.1),
				format!("	background: url(\"/{}.png\");", self.manifest.name),
				format!("	background-position-y: -{}px;", index as u32 * self.manifest.scale_to.1),
				"}".to_string()
			].join("\n")
		}).collect::<Vec<String>>().join("\n")
	}

	fn make_map(&self) -> RgbaImage {
		let mut map = RgbaImage::new(self.manifest.scale_to.0, self.manifest.scale_to.1 * self.segments.len() as u32);
		for (index, segment) in self.segments.iter().enumerate() {
			let resized = resize(&segment.image, self.manifest.scale_to.0, self.manifest.scale_to.1, FilterType::Lanczos3);
			overlay(&mut map, &resized, 0, index as u32 * self.manifest.scale_to.1);
		}
		map
	}
}

fn main() {
	let manifests = vec![
		Manifest {
			name: "abilities",
			input_directory: "assets/abilities",
			output_directory: "assets",
			scale_to: (32, 32)
		},
		Manifest {
			name: "jobs",
			input_directory: "assets/jobs",
			output_directory: "assets",
			scale_to: (48, 48)
		}
	];
	for manifest in manifests {
		manifest.init().generate();
	}
}
