
#[derive(Debug, PartialEq)]
enum State {
	AllReady,
	Charging(i32)
}

pub type Period = (i32, i32);

#[derive(Debug)]
pub struct Tracker {
	state: State,
	max_charges: i32,
	cd: i32,
	last_gained_at: i32,
	markers: Vec<i32>
}

impl Tracker {
	pub fn begin(charges: u32, cd: u32) -> Self {
		// ~30 usages is sufficient to prevent reallocation for most things with a cd of ~30s+
		let mut markers = Vec::with_capacity(32);
		markers.push(i32::MIN);
		Self {
			state: State::AllReady,
			max_charges: charges as i32,
			cd: cd as i32,
			last_gained_at: 0,
			markers
		}
	}

	pub fn end(self) -> Vec<Period> {
		let mut periods = Vec::with_capacity(self.markers.len() / 2);
		let mut markers = self.markers;
		markers.push(i32::MAX);
		/*
		 * `markers` is an even-length list where:
		 * - Starting at index 0, each pair of 2 elements denotes a period where the ability might be available.
		 * - Starting at index 1, each pair of 2 elements denotes a period where the ability is not available.
		 */
		for i in (0..markers.len()).step_by(2) {
			// create naive available periods...
			let mut available: Period = (markers[i], markers[i + 1]);
			// ...and use unavailable periods to adjust them until they're correct
			for k in (1..markers.len() - 1).step_by(2) {
				let unavailable: Period = (markers[k], markers[k + 1]);
				let left = unavailable.0 < available.0;
				let right = unavailable.1 > available.1;
				if left && right {
					// `unavailable` is a superset of `available`
					available.0 = i32::MAX;
					available.1 = i32::MIN;
				} else if left && unavailable.1 > available.0 {
					// `unavailable` overlaps `available` on the left
					available.0 = unavailable.1;
				} else if right && unavailable.0 < available.1 {
					// `unavailable` overlaps `available` on the right
					available.1 = unavailable.0;
				}
				// `available` cannot be a superset of or equivalent to `unavailable`
			}
			if available.1 >= available.0 {
				periods.push(available);
			}
		}
		periods
	}

	pub fn use_at(&mut self, time: i32) {
		match self.state {
			State::AllReady => {
				self.state = State::Charging(self.max_charges - 1);
				self.last_gained_at = time;
			},
			// tldr: evaluating state changes that occurred in [last_gained_at, time] to regenerate charges
			State::Charging(mut charges) => {
				// checking for full charges generated
				let gained = (time - self.last_gained_at) / self.cd;
				charges = self.max_charges.min(charges + gained);
				if charges == self.max_charges {
					self.state = State::AllReady;
					self.use_at(time);
					return;
				}
				if charges == 0 {
					// this should only be possible if the tracker indirectly lies to itself by providing incorrect periods to the caller
					unreachable!("Attempted to use an ability when no charges were available");
				}
				self.last_gained_at += gained * self.cd;
				self.state = State::Charging(charges - 1);
			}
		}
		// if there are no charges left after the usage, markers need to be created
		if let State::Charging(0) = self.state {
			// tldr: [the most recent time that full charges could've been possible, time of next charge]
			if self.max_charges == 1 {
				self.markers.push(time - self.cd);
				self.markers.push(time + self.cd);
			} else {
				self.markers.push(self.last_gained_at - (self.max_charges - 1) * self.cd);
				self.markers.push(self.last_gained_at + self.cd);
			}
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	#[should_panic]
	fn disallows_bad_usages() {
		let mut tracker = Tracker::begin(1, 5);
		tracker.use_at(1);
		tracker.use_at(2);
	}

	#[test]
	fn single_charges() {
		// single usage
		let mut tracker = Tracker::begin(1, 5);
		tracker.use_at(1);
		assert_eq!(tracker.end(), vec![
			(i32::MIN, -4),
			(6, i32::MAX)
		]);

		// multi usage
		let mut tracker = Tracker::begin(1, 5);
		tracker.use_at(1);
		tracker.use_at(6);
		tracker.use_at(12);
		tracker.use_at(23);
		assert_eq!(tracker.end(), vec![
			(i32::MIN, -4),
			(17, 18),
			(28, i32::MAX)
		]);
	}

	#[test]
	fn multiple_charges() {
		// not exhausting charges
		let mut tracker = Tracker::begin(3, 5);
		tracker.use_at(1);
		tracker.use_at(4);
		assert_eq!(tracker.end(), vec![(i32::MIN, i32::MAX)]);

		let mut tracker = Tracker::begin(3, 5);
		tracker.use_at(1);
		tracker.use_at(3);
		tracker.use_at(8);
		tracker.use_at(9);
		assert_eq!(tracker.end(), vec![
			(i32::MIN, -4),
			(11, i32::MAX)
		]);
	}
}
