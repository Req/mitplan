
use serde::{
	de::DeserializeOwned,
	Serialize
};
use mitplan_shared::{
	amalgam::{
		Amalgam,
		Encounter
	},
	api::{
		AmalgamId,
		AmalgamWithPassword,
		ApiResult,
		JustPassword
	}
};
use crate::agents::route::Path;

fn prefix() -> String {
	const URL_PREFIX: Option<&'static str> = if cfg!(debug_assertions) {
		Some("http://localhost:8000")
	} else {
		None
	};

	match URL_PREFIX {
		Some(prefix) => prefix.to_string(),
		None => Path::current().origin
	}
}

async fn api_call_impl<A, T>(url: &str, args: Option<&A>) -> Result<T, reqwest::Error> where A: Serialize, T: DeserializeOwned {
	match args {
		Some(args) => reqwest::Client::new().post(url).json(args).send().await?.json().await,
		None => reqwest::get(url).await?.json().await
	}
}

#[async_recursion(?Send)]
async fn api_call<A, T>(url: &str, args: Option<&'async_recursion A>) -> ApiResult<T> where A: Serialize, T: DeserializeOwned {
	let prefixed = format!("{}{}", prefix(), url);
	match api_call_impl::<A, ApiResult<T>>(&prefixed, args).await {
		Ok(ApiResult::Success(value)) => ApiResult::Success(value),
		Ok(ApiResult::Error(error)) => {
			/*
			 * This error originates in SQLite.
			 * I do not wish to debug this error.
			 * I'm choosing to ignore it and try again when it occurs.
			 */
			if error.contains("database is locked") {
				return api_call::<A, T>(url, args).await;
			}
			ApiResult::Error(error)
		},
		Err(e) => ApiResult::Error(e.to_string())
	}
}

pub async fn get_timeline(id: AmalgamId) -> ApiResult<(Amalgam, Encounter)> {
	match api_call(&format!("/api/timeline/{}", id), Option::<&()>::None).await as ApiResult<Amalgam> {
		ApiResult::Error(e) => ApiResult::Error(e),
		ApiResult::Success(amalgam) => {
			match load_preset(&amalgam.preset).await {
				ApiResult::Success(encounter) => ApiResult::Success((amalgam, encounter)),
				ApiResult::Error(e) => ApiResult::Error(format!("The stored timeline data was successfully retrieved but the timeline preset itself failed to load: {}", e))
			}
		}
	}
}

pub async fn edit_timeline(id: AmalgamId, amalgam: Amalgam, password: String) -> ApiResult<()> {
	api_call(&format!("/api/timeline/{}/edit", id), Some(&AmalgamWithPassword {
		amalgam,
		password
	})).await
}

pub async fn delete_timeline(id: AmalgamId, password: String) -> ApiResult<()> {
	api_call(&format!("/api/timeline/{}/delete", id), Some(&JustPassword {
		password
	})).await
}

pub async fn create_timeline(amalgam: Amalgam, password: String) -> ApiResult<AmalgamId> {
	api_call("/api/timeline/create", Some(&AmalgamWithPassword {
		amalgam,
		password
	})).await
}

async fn load_preset_impl(name: &str) -> Result<Encounter, reqwest::Error> {
	let url = format!("{}/{}.json", Path::current().origin, name);
	reqwest::Client::new().get(url).send().await?.json().await
}

pub async fn load_preset(name: &str) -> ApiResult<Encounter> {
	ApiResult::from(load_preset_impl(name).await.map_err(|e| e.to_string()))
}

#[cfg(test)]
mod tests {
	use super::*;
	use std::time::Duration;
	use mitplan_shared::{
		amalgam::PhaseOffsets,
		job::Job
	};

	macro_rules! wait {
		($e: expr) => {
			tokio_test::block_on($e)
		}
	}

	macro_rules! succeed {
		($e: expr) => {
			match $e {
				ApiResult::Success(v) => v,
				ApiResult::Error(e) => panic!("Expected success, found failure ({:?})", e)
			}
		}
	}

	macro_rules! fail {
		($e: expr) => {
			match $e {
				ApiResult::Success(v) => panic!("Expected failure, found success ({:?})", v),
				ApiResult::Error(ref e) => e.to_lowercase()
			}
		}
	}

	macro_rules! assert_contains {
		($lhs: expr, $rhs: expr) => {
			if !$lhs.contains($rhs) {
				panic!("assertion failed: \"{}\" does not contain \"{}\"", $lhs, $rhs);
			}
		}
	}

	lazy_static! {
		static ref DUMMY1: Amalgam = Amalgam {
			preset: "UCOB".to_string(),
			jobs: [Job::DUM; 8],
			items: [vec![], vec![], vec![], vec![], vec![], vec![], vec![], vec![]],
			filters: [true, false, false],
			offsets: vec![
				PhaseOffsets {
					start_offset: 5,
					end_offset: 7
				}
			]
		};

		static ref DUMMY2: Amalgam = Amalgam {
			preset: "TEA".to_string(),
			jobs: [Job::SCH, Job::DUM, Job::DUM, Job::DUM, Job::DUM, Job::DUM, Job::DUM, Job::DUM],
			items: [vec![], vec![], vec![], vec![], vec![], vec![], vec![], vec![]],
			filters: [false, true, false],
			offsets: vec![
				PhaseOffsets {
					start_offset: 5,
					end_offset: 8
				}
			]
		};

		static ref PASSWORD123: String = "password123".to_string();
		static ref NOT_PASSWORD123: String = "password!23".to_string();
	}

	/*
	 * Trying to operate on a timeline immediately after it's creation
	 * causes spurious failures because changes haven't properly propagated
	 * yet. This performs a call until changes are propagated. In longer
	 * tests, this only has to be called for the first (not every) operation.
	 */
	fn loop_until_exists<T, F>(f: F) -> ApiResult<T> where F: Fn() -> ApiResult<T> {
		match f() {
			ApiResult::Error(error) if error.contains("not found") => loop_until_exists(f),
			otherwise => otherwise
		}
	}

	#[test]
	#[ignore]
	fn cannot_create_timeline_with_empty_password() {
		let result = wait!(create_timeline(DUMMY1.clone(), String::new()));
		assert_contains!(fail!(result), "password");
	}

	#[test]
	#[ignore]
	fn can_create_then_get_then_validate() {
		let id = succeed!(wait!(create_timeline(DUMMY1.clone(), PASSWORD123.clone())));
		let result = loop_until_exists(|| wait!(get_timeline(id)));
		assert_eq!(succeed!(result).0, *DUMMY1);
	}

	#[test]
	#[ignore]
	fn editing_requires_correct_password_and_works() {
		let result = wait!(create_timeline(DUMMY1.clone(), PASSWORD123.clone()));
		let id = succeed!(result);
		// should fail with the wrong password
		let result = loop_until_exists(|| wait!(edit_timeline(id, DUMMY2.clone(), NOT_PASSWORD123.clone())));
		assert_contains!(fail!(result), "password");
		// should return the unchanged state
		let result = wait!(get_timeline(id));
		assert_eq!(succeed!(result).0, *DUMMY1);
		// should succeed
		let result = wait!(edit_timeline(id, DUMMY2.clone(), PASSWORD123.clone()));
		assert_eq!(succeed!(result), ());
		// should return the updated state
		// loop_until_exists can't copy with editing an existing timeline. sleeping in tests sucks, but at least tests are multithreaded
		std::thread::sleep(Duration::from_secs(1));
		let result = wait!(get_timeline(id));
		assert_eq!(succeed!(result).0, *DUMMY2);
	}

	#[test]
	#[ignore]
	fn cannot_delete_oob_id() {
		// need to find the last index first
		let result = wait!(create_timeline(DUMMY1.clone(), PASSWORD123.clone()));
		let id = succeed!(result);
		// /threading gang/ might cause problems with just id + 1
		let result = wait!(delete_timeline(id + 500, PASSWORD123.clone()));
		assert_contains!(fail!(result), "not found");
	}

	#[test]
	#[ignore]
	fn deleting_requires_correct_password_and_works() {
		let result = wait!(create_timeline(DUMMY1.clone(), PASSWORD123.clone()));
		let id = succeed!(result);
		// should fail with the wrong password
		let result = loop_until_exists(|| wait!(delete_timeline(id, NOT_PASSWORD123.clone())));
		assert_contains!(fail!(result), "password");
		// should still exist
		let result = wait!(get_timeline(id));
		assert_eq!(succeed!(result).0, *DUMMY1);
		// should succeed
		let result = wait!(delete_timeline(id, PASSWORD123.clone()));
		assert_eq!(succeed!(result), ());
		// should be gone
		let result = wait!(get_timeline(id));
		assert_contains!(fail!(result), "not found");
	}
}
