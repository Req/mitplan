
use std::collections::HashMap;
use mitplan_shared::amalgam::{
	Mechanic,
	MechanicId,
	MechanicTag
};
use crate::ability::{
	Ability,
	DamageType,
	Effect
};

fn mechanic_ability_filter_impl(tags: &[MechanicTag], ability: &Ability) -> bool {
	let effects = ability.effects();

	if effects.is_empty() {
		return true;
	}

	// shields and healing (bonus) work on everything
	for effect in effects {
		match effect {
			Effect::Heal => return true,
			Effect::HealingBonus => return true,
			Effect::Shield => return true,
			_ => {}
		}
	}
	// only solely-reduction abilities are left now

	// percent damage is peepoSad
	if tags.contains(&MechanicTag::Percent) {
		return false;
	}

	// abilities that require a target require a target :peepoThink:
	if ability.requires_enemy && tags.contains(&MechanicTag::Untargetable) {
		return false;
	}

	let required_types = effects.iter().filter_map(|effect|
		match effect {
			Effect::DamageReduction { required_type, ..} => Some(*required_type),
			_ => None
		}
	).collect::<Vec<DamageType>>();
	if required_types.contains(&DamageType::Any) {
		return true;
	}
	if tags.contains(&MechanicTag::Darkness) {
		return false;
	}

	let affects_physical = required_types.contains(&DamageType::Physical);
	let is_physical = tags.contains(&MechanicTag::Physical);
	if affects_physical && is_physical {
		return true;
	}
	let affects_magical = required_types.contains(&DamageType::Magical);
	let is_magical = tags.contains(&MechanicTag::Magical);
	if affects_magical && is_magical {
		return true;
	}
	if affects_physical || affects_magical {
		// implicit: !affects_physical && !affects_magical
		return false
	}

	// really grasping at straws here
	true
}

type MechanicAbilityFilterCache = HashMap<(MechanicId, &'static str), bool>;

pub fn mechanic_ability_filter(mechanic: &Mechanic, ability: &Ability) -> bool {
	unsafe {
		// yes, yes, "static mut = bad". access is single-threaded and there shouldn't be reentrance
		static mut CACHE: Option<MechanicAbilityFilterCache> = None;
		if CACHE.is_none() {
			CACHE = Some(HashMap::new());
		}
		if let Some(cache) = &mut CACHE {
			let key = (mechanic.id, ability.name());
			if let Some(value) = cache.get(&key) {
				return *value;
			}
			let value = mechanic_ability_filter_impl(&mechanic.tags, ability);
			cache.insert(key, value);
			value
		} else {
			unreachable!()
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::{
		ability::{
			Category,
			PHYSICAL_DAMAGE_REDUCTION,
			Syncable::Always
		},
		jobs::{
			drk::{
				LIVING,
				TBN
			},
			gnb::HOL,
			mch::TACTICIAN,
			sch::ILLUMINATION,
			tank::REPRISAL
		}
	};

	#[test]
	fn filters() {
		let any_reduction_targeted = &*REPRISAL;
		let any_reduction_untargeted = &*TACTICIAN;
		let magical_reduction_untargeted = &*HOL;
		// no such ability exists
		let physical_reduction_untargeted = &Ability {
			name: Always(""),
			level: 1,
			cd: Always(1),
			effects: Always(vec![PHYSICAL_DAMAGE_REDUCTION]),
			requires_enemy: false,
			charges: Always(1),
			duration: Always(1),
			category: Category::Personal
		};
		let no_effect = &*LIVING;
		let shield = &*TBN;
		let healing_with_specific_reduction = &*ILLUMINATION;

		// no effects and no tags
		assert!(mechanic_ability_filter_impl(&vec![], no_effect));
		// physical can only be used on physical
		assert!(mechanic_ability_filter_impl(&vec![MechanicTag::Physical], physical_reduction_untargeted));
		assert!(!mechanic_ability_filter_impl(&vec![], physical_reduction_untargeted));
		// magical can only be used on magical
		assert!(mechanic_ability_filter_impl(&vec![MechanicTag::Magical], magical_reduction_untargeted));
		assert!(!mechanic_ability_filter_impl(&vec![], magical_reduction_untargeted));
		// if a specific reduction has a secondary effect it should still pass
		assert!(mechanic_ability_filter_impl(&vec![], healing_with_specific_reduction));
		// shields work on percent damage
		assert!(mechanic_ability_filter_impl(&vec![MechanicTag::Percent], shield));
		// reductions don't work on percent damage
		assert!(!mechanic_ability_filter_impl(&vec![MechanicTag::Percent], any_reduction_untargeted));
		// targeted things require targets
		assert!(!mechanic_ability_filter_impl(&vec![MechanicTag::Untargetable], any_reduction_targeted));

		// misc combinations
		assert!(mechanic_ability_filter_impl(&vec![MechanicTag::Physical], shield));
	}
}
