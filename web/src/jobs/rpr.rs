
use crate::{
	ability::{
		Ability,
		Category,
		Effect,
		Syncable::{
			Always,
			Traited
		}
	},
	jobs::melee
};

lazy_static! {
	static ref CREST: Ability = Ability {
		name: Always("Arcane Crest"),
		level: 40,
		cd: Always(30),
		effects: Traited(
			vec![Effect::Shield],
			84,
			vec![
				Effect::Shield,
				Effect::Heal
			]
		),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(5),
		category: Category::Personal
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*melee::BLOODBATH,
		&*melee::FEINT,
		&*melee::SECOND_WIND,
		&*CREST
	]
}
