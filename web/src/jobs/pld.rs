
use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Effect,
		Syncable::{
			Always,
			DoubleTraited,
			Traited
		}
	},
	jobs::tank
};

lazy_static! {
	static ref SENTINEL: Ability = tank::generic_sentinel("Sentinel", 38);

	static ref VEIL: Ability = Ability {
		name: Always("Divine Veil"),
		level: 56,
		cd: Always(90),
		effects: Traited(
			vec![Effect::Shield],
			88,
			vec![
				Effect::Shield,
				Effect::Heal
			]
		),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(30 + 30),
		category: Category::Default
	};

	static ref PASSAGE: Ability = Ability {
		name: Always("Passage of Arms"),
		level: 70,
		cd: Always(120),
		effects: Always(vec![ALL_DAMAGE_REDUCTION]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(18),
		category: Category::Default
	};

	pub static ref INTERVENTION: Ability = Ability {
		name: Always("Intervention"),
		level: 62,
		cd: Always(10),
		effects: Always(vec![
			ALL_DAMAGE_REDUCTION,
			Effect::Heal
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(8),
		category: Category::Personal
	};

	static ref COVER: Ability = Ability {
		name: Always("Cover"),
		level: 45,
		cd: Always(120),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(12),
		category: Category::Personal
	};

	pub static ref HALLOWED: Ability = Ability {
		name: Always("Hallowed Ground"),
		level: 50,
		cd: Always(420),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10),
		category: Category::Personal
	};

	pub static ref SHELTRON: Ability = Ability {
		name: Traited("Sheltron", 82, "Holy Sheltron"),
		level: 35,
		/*
		 * Swing timers outside of ARR are normalized to 2.24, so 50 gauge is generated in
		 * 22.4 seconds. We'll round this up to 23 to make sure it can't be used consecutively
		 * at 22, 44, and 66 seconds. However, this assumes full uptime.
		 */
		cd: Always(23),
		effects: Traited(
			vec![ALL_DAMAGE_REDUCTION],
			82,
			vec![
				ALL_DAMAGE_REDUCTION,
				Effect::Heal
			]
		),
		requires_enemy: false,
		charges: Always(2),
		duration: DoubleTraited(5, 74, 6, 82, 8),
		category: Category::Personal
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*tank::REPRISAL,
		&*tank::RAMPART,
		&*SENTINEL,
		&*VEIL,
		&*PASSAGE,
		&*INTERVENTION,
		&*COVER,
		&*HALLOWED
	]
}
