
use crate::{
	ability::Ability,
	jobs::melee
};

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*melee::BLOODBATH,
		&*melee::FEINT,
		&*melee::SECOND_WIND
	]
}
