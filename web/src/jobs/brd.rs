
use crate::{
	ability::{
		Ability,
		Category,
		Effect,
		Syncable::Always
	},
	jobs::ranged
};

lazy_static! {
	static ref TROUBADOUR: Ability = ranged::generic_tactician("Troubadour", 62);

	static ref PAEAN: Ability = Ability {
		name: Always("The Warden's Paean"),
		level: 35,
		cd: Always(45),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(30),
		category: Category::Personal
	};

	static ref MINNE: Ability = Ability {
		name: Always("Nature's Minne"),
		level: 66,
		cd: Always(90),
		effects: Always(vec![Effect::HealingBonus]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::HealerThroughput
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*ranged::SECOND_WIND,
		&*TROUBADOUR,
		&*PAEAN,
		&*MINNE
	]
}
