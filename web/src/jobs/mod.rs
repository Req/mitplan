
// they don't need to be pub, but i'm exposing them for tests
pub mod ast;
pub mod blm;
pub mod brd;
pub mod dnc;
pub mod drg;
pub mod drk;
pub mod gnb;
pub mod mch;
pub mod mnk;
pub mod nin;
pub mod pct;
pub mod pld;
pub mod rdm;
pub mod rpr;
pub mod sam;
pub mod sch;
pub mod sge;
pub mod smn;
pub mod vpr;
pub mod war;
pub mod whm;

pub mod caster;
pub mod healer;
pub mod melee;
pub mod ranged;
pub mod tank;

use std::collections::HashMap;
use mitplan_shared::job::Job;
use crate::ability::Ability;

pub const MAX_LEVEL: u32 = 90;

lazy_static! {
	pub static ref ABILITIES: HashMap<Job, Vec<&'static Ability>> = HashMap::from([
		(Job::DUM, vec![]),
		(Job::PLD, pld::abilities()),
		(Job::WAR, war::abilities()),
		(Job::DRK, drk::abilities()),
		(Job::GNB, gnb::abilities()),
		(Job::WHM, whm::abilities()),
		(Job::SCH, sch::abilities()),
		(Job::AST, ast::abilities()),
		(Job::SGE, sge::abilities()),
		(Job::MNK, mnk::abilities()),
		(Job::DRG, drg::abilities()),
		(Job::NIN, nin::abilities()),
		(Job::SAM, sam::abilities()),
		(Job::RPR, rpr::abilities()),
		(Job::VPR, vpr::abilities()),
		(Job::BRD, brd::abilities()),
		(Job::MCH, mch::abilities()),
		(Job::DNC, dnc::abilities()),
		(Job::BLM, blm::abilities()),
		(Job::SMN, smn::abilities()),
		(Job::RDM, rdm::abilities()),
		(Job::PCT, pct::abilities()),
		(Job::Tank, tank::abilities()),
		(Job::Healer, healer::abilities()),
		(Job::Melee, melee::abilities()),
		(Job::Ranged, ranged::abilities()),
		(Job::Caster, caster::abilities()),
	]);
}
