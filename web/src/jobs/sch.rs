
use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Effect,
		MAGICAL_DAMAGE_REDUCTION,
		Syncable::{
			Always,
			Traited
		}
	},
	jobs::healer
};

lazy_static! {
	pub static ref SOIL: Ability = Ability {
		name: Always("Sacred Soil"),
		level: 50,
		cd: Always(30),
		effects: Always(vec![
			ALL_DAMAGE_REDUCTION,
			Effect::Heal
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::Default
	};

	pub static ref ILLUMINATION: Ability = Ability {
		name: Always("Fey Illumination"),
		level: 40,
		cd: Always(120),
		effects: Always(vec![
			MAGICAL_DAMAGE_REDUCTION,
			Effect::HealingBonus
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(20),
		category: Category::Default
	};

	static ref SERAPH: Ability = Ability {
		name: Always("Summon Seraph"),
		level: 80,
		cd: Always(120),
		effects: Always(vec![Effect::Shield]),
		requires_enemy: false,
		// this is seraph, not consolation
		charges: Always(1),
		duration: Always(20 /* seraph */ + 30, /* shield */),
		category: Category::Default
	};

	static ref WHISPERING: Ability = Ability {
		name: Always("Whispering Dawn"),
		level: 20,
		cd: Always(60),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(21),
		category: Category::HealerThroughput
	};

	static ref INDOM: Ability = Ability {
		name: Always("Indomitability"),
		level: 52,
		cd: Always(30),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(1),
		category: Category::HealerThroughput
	};

	static ref DEPLOY: Ability = Ability {
		name: Always("Deployment Tactics"),
		level: 56,
		cd: Traited(120, 88, 90),
		effects: Always(vec![Effect::Shield]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(30),
		category: Category::HealerThroughput
	};

	static ref EMERGENCY: Ability = Ability {
		name: Always("Emergency Tactics"),
		level: 58,
		cd: Always(15),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::HealerThroughput
	};

	static ref DISSIPATION: Ability = Ability {
		name: Always("Dissipation"),
		level: 60,
		cd: Always(180),
		effects: Always(vec![Effect::HealingBonus]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(30),
		category: Category::HealerThroughput
	};

	static ref EXCOG: Ability = Ability {
		name: Always("Excogitation"),
		level: 62,
		cd: Always(45),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(45),
		category: Category::HealerThroughput
	};

	static ref RECITATION: Ability = Ability {
		name: Always("Recitation"),
		level: 74,
		cd: Traited(90, 98, 60),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::HealerThroughput
	};

	static ref BLESSING: Ability = Ability {
		name: Always("Fey Blessing"),
		level: 76,
		cd: Always(60),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(1),
		category: Category::HealerThroughput
	};

	static ref PROTRACTION: Ability = Ability {
		name: Always("Protraction"),
		level: 86,
		cd: Always(60),
		effects: Always(vec![
			// see thrill for reasoning
			Effect::Heal,
			Effect::HealingBonus
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10),
		category: Category::HealerThroughput
	};

	static ref EXPEDIENT: Ability = Ability {
		name: Always("Expedient"),
		level: 90,
		cd: Always(120),
		effects: Always(vec![ALL_DAMAGE_REDUCTION]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(20),
		category: Category::Default
	};

	static ref SUCCOR: Ability = Ability {
		name: Always("Succor"),
		level: 35,
		cd: Always(1),
		effects: Always(vec![Effect::Shield]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(30),
		category: Category::Default
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*healer::SWIFTCAST,
		&*SOIL,
		&*ILLUMINATION,
		&*SERAPH,
		&*WHISPERING,
		&*INDOM,
		&*DEPLOY,
		&*EMERGENCY,
		&*DISSIPATION,
		&*EXCOG,
		&*RECITATION,
		&*BLESSING,
		&*PROTRACTION,
		&*EXPEDIENT,
		&*SUCCOR
	]
}
