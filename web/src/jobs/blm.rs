
use crate::{
	ability::{
		Ability,
		Category,
		Effect,
		Syncable::Always
	},
	jobs::caster
};

lazy_static! {
	static ref MANAWARD: Ability = Ability {
		name: Always("Manaward"),
		level: 30,
		cd: Always(120),
		effects: Always(vec![Effect::Shield]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(20),
		category: Category::Personal
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*caster::ADDLE,
		&*MANAWARD
	]
}
