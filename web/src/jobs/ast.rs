
use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Effect,
		Syncable::{
			Always,
			DoubleTraited,
			Traited
		}
	},
	jobs::healer
};

lazy_static! {
	static ref NEUTRAL: Ability = Ability {
		name: Always("Neutral Sect"),
		level: 80,
		cd: Always(120),
		effects: Always(vec![
			Effect::HealingBonus,
			Effect::Shield
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(20 /* neutral */ + 30 /* shield */),
		// neutral is effectively a shield, so it goes under default instead of personal
		category: Category::Default
	};

	static ref OPPOSITION: Ability = Ability {
		name: Always("Celestial Opposition"),
		level: 60,
		cd: Always(60),
		effects: Always(vec![
			Effect::Heal,
			Effect::Shield
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(30),
		category: Category::Default
	};

	static ref UNCONSCIOUS: Ability = Ability {
		name: Always("Collective Unconscious"),
		level: 58,
		cd: Always(60),
		effects: Always(vec![
			ALL_DAMAGE_REDUCTION,
			Effect::Heal
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(18),
		category: Category::Default
	};

	static ref LIGHTSPEED: Ability = Ability {
		name: Always("Lightspeed"),
		level: 6,
		cd: Always(90),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::Personal
	};

	static ref SYNASTRY: Ability = Ability {
		name: Always("Synastry"),
		level: 50,
		cd: Always(120),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(20),
		category: Category::HealerThroughput
	};

	static ref STAR: Ability = Ability {
		name: Always("Earthly Star"),
		level: 62,
		cd: Always(60),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(20),
		category: Category::HealerThroughput
	};

	static ref INTERSECTION: Ability = Ability {
		name: Always("Celestial Intersection"),
		level: 74,
		cd: Always(30),
		effects: Always(vec![
			Effect::Heal,
			Effect::Shield
		]),
		requires_enemy: false,
		charges: Traited(1, 88, 2),
		duration: Always(30),
		category: Category::HealerThroughput
	};

	static ref HOROSCOPE: Ability = Ability {
		name: Always("Horoscope"),
		level: 76,
		cd: Always(60),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10 + 30),
		category: Category::HealerThroughput
	};

	static ref EXALTATION: Ability = Ability {
		name: Always("Exaltation"),
		level: 86,
		cd: Always(60),
		effects: Always(vec![
			Effect::Heal,
			ALL_DAMAGE_REDUCTION
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(8),
		category: Category::HealerThroughput
	};

	static ref COSMOS: Ability = Ability {
		name: Always("Macrocosmos"),
		level: 90,
		cd: Always(180),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::HealerThroughput
	};

	static ref DIGNITY: Ability = Ability {
		name: Always("Essential Dignity"),
		level: 15,
		cd: Always(40),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: DoubleTraited(1, 78, 2, 98, 3),
		duration: Always(1),
		category: Category::HealerThroughput
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*healer::SWIFTCAST,
		&*NEUTRAL,
		&*OPPOSITION,
		&*UNCONSCIOUS,
		&*LIGHTSPEED,
		&*SYNASTRY,
		&*STAR,
		&*HOROSCOPE,
		&*INTERSECTION,
		&*EXALTATION,
		&*COSMOS,
		&*DIGNITY
	]
}
