
use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Syncable::Always
	},
	jobs::melee
};

lazy_static! {
	static ref EYE: Ability = Ability {
		name: Always("Third Eye"),
		level: 6,
		cd: Always(15),
		effects: Always(vec![ALL_DAMAGE_REDUCTION]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(3),
		category: Category::Personal
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*melee::BLOODBATH,
		&*melee::FEINT,
		&*melee::SECOND_WIND,
		&*EYE
	]
}
