
use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Effect,
		Syncable::{
			Always,
			Traited
		}
	},
	jobs::tank
};

lazy_static! {
	pub static ref HOL: Ability = tank::generic_missionary("Heart of Light", 64);
	static ref NEBULA: Ability = tank::generic_sentinel("Nebula", 38);

	static ref CAMO: Ability = Ability {
		name: Always("Camouflage"),
		level: 6,
		cd: Always(90),
		effects: Always(vec![ALL_DAMAGE_REDUCTION]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(20),
		category: Category::Personal
	};

	pub static ref BOLIDE: Ability = Ability {
		name: Always("Superbolide"),
		level: 50,
		cd: Always(360),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10),
		category: Category::Personal
	};

	static ref STONE: Ability = Ability {
		name: Traited("Heart of Stone", 82, "Heart of Corundum"),
		level: 68,
		cd: Always(25),
		effects: Traited(
			vec![ALL_DAMAGE_REDUCTION],
			82,
			vec![
				ALL_DAMAGE_REDUCTION,
				Effect::Heal
			]
		),
		requires_enemy: false,
		charges: Always(1),
		duration: Traited(7, 82, 8),
		category: Category::Personal
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*tank::REPRISAL,
		&*tank::RAMPART,
		&*HOL,
		&*NEBULA,
		&*CAMO,
		&*BOLIDE,
		&*STONE
	]
}
