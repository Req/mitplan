
use crate::{
	ability::{
		Ability,
		Category,
		Effect,
		Syncable::{
			Always,
			Traited
		}
	},
	jobs::caster
};

lazy_static! {
	static ref AEGIS: Ability = Ability {
		name: Always("Radiant Aegis"),
		level: 2,
		cd: Always(60),
		effects: Always(vec![Effect::Shield]),
		requires_enemy: false,
		charges: Traited(1, 88, 2),
		duration: Always(30),
		category: Category::Personal
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*caster::ADDLE,
		&*AEGIS
	]
}
