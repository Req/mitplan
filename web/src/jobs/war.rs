
use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Effect,
		Syncable::{
			Always,
			Traited
		}
	},
	jobs::tank
};

lazy_static! {
	static ref SENTINEL: Ability = tank::generic_sentinel("Vengeance", 38);

	static ref SHAKE: Ability = Ability {
		name: Always("Shake It Off"),
		level: 68,
		cd: Always(90),
		effects: Always(vec![
			Effect::Shield,
			Effect::Heal
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::Default
	};

	static ref THRILL: Ability = Ability {
		name: Always("Thrill of Battle"),
		level: 30,
		cd: Always(90),
		effects: Always(vec![
			// it does technically heal you, and the healed health doesn't get removed upon expiration
			Effect::Heal,
			Effect::HealingBonus
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10),
		category: Category::Personal
	};

	pub static ref HOLMGANG: Ability = Ability {
		name: Always("Holmgang"),
		level: 42,
		cd: Always(240),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10),
		category: Category::Personal
	};

	// nascent shares with intuition so i'm not defining nascent
	static ref INTUITION: Ability = Ability {
		name: Traited("Raw Intuition", 82, "Bloodwhetting"),
		level: 56,
		cd: Always(25),
		effects: Traited(
			vec![ALL_DAMAGE_REDUCTION],
			82,
			vec![
				ALL_DAMAGE_REDUCTION,
				Effect::Heal,
				Effect::Shield
			]
		),
		requires_enemy: false,
		charges: Always(1),
		duration: Traited(6, 82, 8),
		category: Category::Personal
	};

	static ref EQUILIBRIUM: Ability = Ability {
		name: Always("Equilibrium"),
		level: 58,
		cd: Always(60),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(1),
		category: Category::Personal
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*tank::REPRISAL,
		&*tank::RAMPART,
		&*SENTINEL,
		&*SHAKE,
		&*THRILL,
		&*HOLMGANG,
		&*INTUITION,
		&*EQUILIBRIUM
	]
}
