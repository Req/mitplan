
use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Effect,
		Syncable::{
			Always,
			Traited
		}
	},
	jobs::healer
};

lazy_static! {
	static ref PRESENCE: Ability = Ability {
		name: Always("Presence of Mind"),
		level: 30,
		cd: Always(120),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::Personal
	};

	static ref BENE: Ability = Ability {
		name: Always("Benediction"),
		level: 50,
		cd: Always(180),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(1),
		category: Category::HealerThroughput
	};

	static ref ASSIZE: Ability = Ability {
		name: Always("Assize"),
		level: 56,
		cd: Always(45),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(1),
		// glare mages furious
		category: Category::HealerThroughput
	};

	static ref AIR: Ability = Ability {
		name: Always("Thin Air"),
		level: 58,
		cd: Always(60),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(2),
		duration: Always(12),
		category: Category::HealerThroughput
	};

	static ref TETRA: Ability = Ability {
		name: Always("Tetragrammaton"),
		level: 60,
		cd: Always(60),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Traited(1, 98, 2),
		duration: Always(1),
		category: Category::HealerThroughput
	};

	static ref BENISON: Ability = Ability {
		name: Always("Divine Benison"),
		level: 66,
		cd: Always(30),
		effects: Always(vec![Effect::Shield]),
		requires_enemy: false,
		charges: Traited(1, 88, 2),
		duration: Always(15),
		category: Category::HealerThroughput
	};

	static ref PLENARY: Ability = Ability {
		name: Always("Plenary Indulgence"),
		level: 70,
		cd: Always(60),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10),
		category: Category::HealerThroughput
	};

	static ref ASYLUM: Ability = Ability {
		name: Always("Asylum"),
		level: 52,
		cd: Always(90),
		effects: Traited(
			vec![Effect::Heal],
			78,
			vec![
				Effect::Heal,
				Effect::HealingBonus
			]
		),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(24),
		category: Category::Default
	};

	static ref TEMPERANCE: Ability = Ability {
		name: Always("Temperance"),
		level: 80,
		cd: Always(120),
		effects: Always(vec![
			ALL_DAMAGE_REDUCTION,
			Effect::HealingBonus
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(20),
		category: Category::Default
	};

	static ref AQUAVEIL: Ability = Ability {
		name: Always("Aquaveil"),
		level: 86,
		cd: Always(60),
		effects: Always(vec![ALL_DAMAGE_REDUCTION]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(8),
		category: Category::Personal
	};

	static ref LITURGY: Ability = Ability {
		name: Always("Liturgy of the Bell"),
		level: 90,
		cd: Always(180),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::HealerThroughput
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*healer::SWIFTCAST,
		&*PRESENCE,
		&*BENE,
		&*ASSIZE,
		&*TETRA,
		&*PLENARY,
		&*ASYLUM,
		&*TEMPERANCE,
		&*AQUAVEIL,
		&*LITURGY,
		&*AIR,
		&*BENISON
	]
}
