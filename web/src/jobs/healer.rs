
use crate::ability::{
	Ability,
	Category,
	Syncable::{
		Always,
		Traited
	}
};

lazy_static! {
	pub static ref SWIFTCAST: Ability = Ability {
		name: Always("Swiftcast"),
		level: 1,
		cd: Traited(60, 94, 40),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10),
		category: Category::Personal
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		// none of the healers actually share something in the same way that tanks share sentinel
		&*SWIFTCAST
	]
}
