
use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Effect,
		Syncable::Always
	},
	jobs::caster
};

lazy_static! {
	static ref BARRIER: Ability = Ability {
		name: Always("Magick Barrier"),
		level: 86,
		cd: Always(120),
		effects: Always(vec![
			ALL_DAMAGE_REDUCTION,
			Effect::Heal
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10),
		category: Category::Default
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*caster::ADDLE,
		&*BARRIER
	]
}
