
use crate::ability::{
	Ability,
	Category,
	MAGICAL_DAMAGE_REDUCTION,
	PHYSICAL_DAMAGE_REDUCTION,
	Syncable::{
		Always,
		Traited
	}
};

lazy_static! {
	pub static ref ADDLE: Ability = Ability {
		name: Always("Addle"),
		level: 1,
		cd: Always(90),
		effects: Always(vec![
			MAGICAL_DAMAGE_REDUCTION,
			PHYSICAL_DAMAGE_REDUCTION
		]),
		requires_enemy: true,
		charges: Always(1),
		duration: Traited(10, 98, 15),
		category: Category::Default
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*ADDLE
	]
}
