
use crate::{
	ability::{
		Ability,
		Category,
		Effect,
		Syncable::Always
	},
	jobs::ranged
};

lazy_static! {
	static ref SAMBA: Ability = ranged::generic_tactician("Shield Samba", 56);

	static ref WALTZ: Ability = Ability {
		name: Always("Curing Waltz"),
		level: 52,
		cd: Always(60),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(1),
		category: Category::HealerThroughput
	};

	static ref IMPROV: Ability = Ability {
		name: Always("Improvisation"),
		level: 80,
		cd: Always(120),
		effects: Always(vec![
			Effect::Heal,
			Effect::Shield
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15 + 30),
		category: Category::HealerThroughput
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*ranged::SECOND_WIND,
		&*SAMBA,
		&*WALTZ,
		&*IMPROV
	]
}
