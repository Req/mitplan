
use crate::{
	ability::{
		Ability,
		Category,
		Effect,
		Syncable::Always
	},
	jobs::melee
};

lazy_static! {
	static ref SHIFT: Ability = Ability {
		name: Always("Shade Shift"),
		level: 2,
		cd: Always(120),
		effects: Always(vec![Effect::Shield]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(20),
		category: Category::Personal
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*melee::BLOODBATH,
		&*melee::FEINT,
		&*melee::SECOND_WIND,
		&*SHIFT
	]
}
