
use crate::{
	ability::{
		Ability,
		Category,
		Effect,
		Syncable::Always
	},
	jobs::caster
};

lazy_static! {
	static ref COAT: Ability = Ability {
		name: Always("Tempera Coat"),
		level: 10,
		cd: Always(120),
		effects: Always(vec![Effect::Shield]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10),
		category: Category::Default
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*caster::ADDLE,
		&*COAT
	]
}
