
use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Effect,
		Syncable::Always
	},
	jobs::melee
};

lazy_static! {
	static ref MANTRA: Ability = Ability {
		name: Always("Mantra"),
		level: 42,
		cd: Always(90),
		effects: Always(vec![Effect::HealingBonus]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::Default
	};

	static ref ROE: Ability = Ability {
		name: Always("Riddle of Earth"),
		level: 64,
		cd: Always(120),
		effects: Always(vec![ALL_DAMAGE_REDUCTION, Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10),
		category: Category::Personal
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*melee::BLOODBATH,
		&*melee::FEINT,
		&*melee::SECOND_WIND,
		&*MANTRA,
		&*ROE
	]
}
