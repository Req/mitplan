
use crate::ability::{
	Ability,
	Category,
	Effect,
	MAGICAL_DAMAGE_REDUCTION,
	PHYSICAL_DAMAGE_REDUCTION,
	Syncable::{
		Always,
		Traited
	}
};

lazy_static! {
	pub static ref SECOND_WIND: Ability = Ability {
		name: Always("Second Wind"),
		level: 1,
		cd: Always(120),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(1),
		category: Category::Personal
	};

	pub static ref FEINT: Ability = Ability {
		name: Always("Feint"),
		level: 1,
		cd: Always(90),
		effects: Always(vec![
			MAGICAL_DAMAGE_REDUCTION,
			PHYSICAL_DAMAGE_REDUCTION
		]),
		requires_enemy: true,
		charges: Always(1),
		duration: Traited(10, 98, 15),
		category: Category::Default
	};

	pub static ref BLOODBATH: Ability = Ability {
		name: Always("Bloodbath"),
		level: 1,
		cd: Always(120),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: true,
		charges: Always(1),
		duration: Always(20),
		category: Category::Personal
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*SECOND_WIND,
		&*BLOODBATH,
		&*FEINT
	]
}
