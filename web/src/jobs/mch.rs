
use crate::{
	ability::Ability,
	jobs::ranged
};

lazy_static! {
	pub static ref TACTICIAN: Ability = ranged::generic_tactician("Tactician", 56);
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*ranged::SECOND_WIND,
		&*TACTICIAN
	]
}
