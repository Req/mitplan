
use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Effect,
		MAGICAL_DAMAGE_REDUCTION,
		Syncable::Always
	},
	jobs::tank
};

lazy_static! {
	static ref MISSIONARY: Ability = tank::generic_missionary("Dark Missionary", 76);
	static ref SHADOW_WALL: Ability = tank::generic_sentinel("Shadow Wall", 38);

	static ref DARK_MIND: Ability = Ability {
		name: Always("Dark Mind"),
		level: 45,
		cd: Always(60),
		effects: Always(vec![MAGICAL_DAMAGE_REDUCTION]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10),
		category: Category::Personal
	};

	pub static ref TBN: Ability = Ability {
		name: Always("The Blackest Night"),
		level: 70,
		cd: Always(15),
		effects: Always(vec![Effect::Shield]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(7),
		category: Category::Personal
	};

	pub static ref LIVING: Ability = Ability {
		name: Always("Living Dead"),
		level: 50,
		cd: Always(300),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(1),
		// xeno can be heard screaming off in the distance, enraged by mention of 20s LD
		duration: Always(10 + 10),
		category: Category::Personal
	};

	pub static ref OBLATION: Ability = Ability {
		name: Always("Oblation"),
		level: 82,
		cd: Always(60),
		effects: Always(vec![ALL_DAMAGE_REDUCTION]),
		requires_enemy: false,
		charges: Always(2),
		duration: Always(10),
		category: Category::Personal
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*tank::REPRISAL,
		&*tank::RAMPART,
		&*SHADOW_WALL,
		&*MISSIONARY,
		&*DARK_MIND,
		&*TBN,
		&*LIVING,
		&*OBLATION
	]
}
