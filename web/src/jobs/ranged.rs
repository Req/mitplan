
pub use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Effect,
		Syncable::{
			Always,
			Traited
		}
	},
	jobs::melee::SECOND_WIND
};

pub fn generic_tactician(name: &'static str, level: u32) -> Ability {
	Ability {
		name: Always(name),
		level,
		cd: Traited(120, 88, 90),
		effects: Always(vec![ALL_DAMAGE_REDUCTION]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::Default
	}
}

lazy_static! {
	// troubadour is received at 62 while the other two are at 56
	static ref GENERIC_TACTICIAN: Ability = generic_tactician("Generic Tactician", 62);
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*GENERIC_TACTICIAN,
		&*SECOND_WIND
	]
}
