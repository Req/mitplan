
use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Effect,
		Syncable::{
			Always,
			Traited
		}
	},
	jobs::healer
};

lazy_static! {
	static ref PHYSIS: Ability = Ability {
		name: Always("Physis"),
		level: 20,
		cd: Always(60),
		effects: Traited(
			vec![Effect::Heal],
			60,
			vec![
				Effect::Heal,
				Effect::HealingBonus
			]
		),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::HealerThroughput
	};

	static ref SOTERIA: Ability = Ability {
		name: Always("Soteria"),
		level: 35,
		cd: Traited(90, 94, 60),
		// this could be classified as a heal or a bonus, but i don't really like either choice
		effects: Always(vec![]),
		requires_enemy: true,
		charges: Always(1),
		duration: Always(15),
		category: Category::HealerThroughput,
	};

	// praise be upon it
	static ref KERACHOLE: Ability = Ability {
		name: Always("Kerachole"),
		level: 50,
		cd: Always(30),
		effects: Traited(
			vec![ALL_DAMAGE_REDUCTION],
			78,
			vec![
				ALL_DAMAGE_REDUCTION,
				Effect::Heal
			]
		),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::Default
	};

	static ref IXOCHOLE: Ability = Ability {
		name: Always("Ixochole"),
		level: 52,
		cd: Always(30),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(1),
		category: Category::HealerThroughput
	};

	static ref ZOE: Ability = Ability {
		name: Always("Zoe"),
		level: 56,
		cd: Traited(120, 88, 90),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(30),
		category: Category::HealerThroughput
	};

	static ref PEPSIS: Ability = Ability {
		name: Always("Pepsis"),
		level: 58,
		cd: Always(30),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(1),
		category: Category::HealerThroughput
	};

	static ref TAUROCHOLE: Ability = Ability {
		name: Always("Taurochole"),
		level: 62,
		cd: Always(45),
		effects: Always(vec![
			ALL_DAMAGE_REDUCTION,
			Effect::Heal
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		// this could very well belong under default
		category: Category::HealerThroughput
	};

	static ref HAIMA: Ability = Ability {
		name: Always("Haima"),
		level: 62,
		cd: Always(120),
		effects: Always(vec![
			Effect::Heal,
			Effect::Shield
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15 + 15),
		// panhaima is default but this is throughput because it's single target
		category: Category::HealerThroughput
	};

	static ref RHIZOMATA: Ability = Ability {
		name: Always("Rhizomata"),
		level: 74,
		cd: Always(90),
		effects: Always(vec![]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(1),
		category: Category::Personal
	};

	static ref HOLOS: Ability = Ability {
		name: Always("Holos"),
		level: 76,
		cd: Always(120),
		effects: Always(vec![
			ALL_DAMAGE_REDUCTION,
			Effect::Heal
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(20),
		category: Category::Default
	};

	static ref PANHAIMA: Ability = Ability {
		name: Always("Panhaima"),
		level: 80,
		cd: Always(120),
		effects: Always(vec![
			Effect::Heal,
			Effect::Shield
		]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15 + 15),
		category: Category::Default
	};

	static ref KRASIS: Ability = Ability {
		name: Always("Krasis"),
		level: 86,
		cd: Always(60),
		effects: Always(vec![Effect::HealingBonus]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(10),
		category: Category::HealerThroughput
	};

	static ref PNEUMA: Ability = Ability {
		name: Always("Pneuma"),
		level: 90,
		cd: Always(120),
		effects: Always(vec![Effect::Heal]),
		requires_enemy: true,
		charges: Always(1),
		duration: Always(1),
		category: Category::HealerThroughput
	};

	static ref EPROG: Ability = Ability {
		name: Always("Eukrasian Prognosis"),
		level: 30,
		cd: Always(1),
		effects: Always(vec![Effect::Shield]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(30),
		category: Category::Default
	};
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*healer::SWIFTCAST,
		&*PHYSIS,
		&*SOTERIA,
		&*KERACHOLE,
		&*IXOCHOLE,
		&*ZOE,
		&*PEPSIS,
		&*TAUROCHOLE,
		&*HAIMA,
		&*RHIZOMATA,
		&*HOLOS,
		&*PANHAIMA,
		&*KRASIS,
		&*PNEUMA,
		&*EPROG
	]
}
