
use crate::{
	ability::{
		Ability,
		ALL_DAMAGE_REDUCTION,
		Category,
		Effect,
		MAGICAL_DAMAGE_REDUCTION,
		Syncable::{
			Always,
			Traited
		}
	},
	jobs::{
		drk,
		gnb,
		pld,
		war
	}
};

lazy_static! {
	pub static ref REPRISAL: Ability = Ability {
		name: Always("Reprisal"),
		level: 1,
		cd: Always(60),
		effects: Always(vec![ALL_DAMAGE_REDUCTION]),
		requires_enemy: true,
		charges: Always(1),
		duration: Always(10),
		category: Category::Default
	};

	pub static ref RAMPART: Ability = Ability {
		name: Always("Rampart"),
		level: 1,
		cd: Always(90),
		effects: Traited(vec![ALL_DAMAGE_REDUCTION], 94, vec![ALL_DAMAGE_REDUCTION, Effect::HealingBonus]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(20),
		category: Category::Personal
	};
}

pub fn generic_missionary(name: &'static str, level: u32) -> Ability {
	Ability {
		name: Always(name),
		level,
		cd: Always(90),
		effects: Always(vec![MAGICAL_DAMAGE_REDUCTION]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::Default
	}
}

pub fn generic_sentinel(name: &'static str, level: u32) -> Ability {
	Ability {
		name: Always(name),
		level,
		cd: Always(120),
		effects: Always(vec![ALL_DAMAGE_REDUCTION]),
		requires_enemy: false,
		charges: Always(1),
		duration: Always(15),
		category: Category::Personal
	}
}

fn rename(ability: &Ability, name: &'static str) -> Ability {
	let mut ability = ability.clone();
	ability.name = Always(name);
	ability
}

lazy_static! {
	static ref GENERIC_SENTINEL: Ability = generic_sentinel("Generic Sentinel", 38);
	// this is a "best effort" compromise between all of the abilities
	static ref GENERIC_INTUITION: Ability = Ability {
		name: Always("Generic Raw Intuition"),
		level: 68, // heart of stone (highest)
		cd: Always(25),
		effects: Always(vec![ALL_DAMAGE_REDUCTION]),
		requires_enemy: false,
		charges: Always(1),
		// sheltron (5) is the lowest base, and tbn (7) the lowest max
		duration: Traited(5, 82, 7),
		category: Category::Personal
	};
	/*
	 * Strats (timing-wise) can be highly dependent on which tanks you have.
	 * Users should select all invulns that work with the strat.
	 */
	static ref GENERIC_HALLOWED: Ability = rename(&*pld::HALLOWED, "Generic Hallowed Ground");
	static ref GENERIC_LIVING: Ability = rename(&*drk::LIVING, "Generic Living Dead");
	static ref GENERIC_BOLIDE: Ability = rename(&*gnb::BOLIDE, "Generic Superbolide");
	static ref GENERIC_HOLMGANG: Ability = rename(&*war::HOLMGANG, "Generic Holmgang");
}

pub fn abilities() -> Vec<&'static Ability> {
	vec![
		&*REPRISAL,
		&*RAMPART,
		&*GENERIC_SENTINEL,
		&*GENERIC_INTUITION,
		&*GENERIC_HALLOWED,
		&*GENERIC_LIVING,
		&*GENERIC_BOLIDE,
		&*GENERIC_HOLMGANG
	]
}
