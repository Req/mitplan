
use wasm_bindgen::JsCast;

#[cfg(target_arch = "wasm32")]
#[allow(unused_macros)]
macro_rules! log {
	($($t:tt)*) => {
		web_sys::console::log_1(&format!($($t)*).into())
	}
}
#[cfg(not(target_arch = "wasm32"))]
#[allow(unused_macros)]
macro_rules! log {
	($($t:tt)*) => {
		println!($($t)*)
	}
}

/**
 * The templated type is assumed to be the correct type of the element (if it exists).
 * The validity of the cast is not checked.
 */
pub fn get_element_by_id<T: JsCast>(id: &str) -> Option<T> {
	web_sys::window().unwrap().document().unwrap().get_element_by_id(id).map(|e| e.dyn_into::<T>().unwrap())
}

pub fn alert(s: &str) {
	web_sys::window().unwrap().alert_with_message(s).unwrap();
}
