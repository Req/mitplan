
use wasm_bindgen_futures::spawn_local;
use web_sys::{
	HtmlElement,
	MouseEvent
};
use yew::{
	Context,
	Component,
	Html
};
use yew_agent::{
	Bridge,
	Bridged,
	Dispatched
};
use mitplan_shared::{
	api::ApiResult,
	amalgam::{
		Encounter,
		PhaseOffsets
	}
};
use crate::{
	agents::{
		ability::{
			AbilityBus,
			AbilityBusIntent
		},
		amalgam::{
			AmalgamBus,
			AmalgamBusIntent
		},
		amalgam_partial::PartialAmalgam,
		route::{
			Route,
			RouteBus,
			RouteBusEvent,
			RouteBusIntent
		}
	},
	api,
	components::{
		config::{
			filter::Filter,
			preset::Preset,
			state::State
		},
		timeline::Timeline
	},
	jobs::MAX_LEVEL,
	wrappers::{
		alert,
		get_element_by_id
	}
};

type LoadData = (Option<u32>, Encounter, Option<Vec<PhaseOffsets>>);

pub enum Message {
	ToggleReadOnly(Option<bool>),
	UsePreset(String),
	InitiateLoad(LoadData),
	RouteBridgeEvent(RouteBusEvent)
}

pub struct App {
	route_bridge: Box<dyn Bridge<RouteBus>>,
	last_load: Option<LoadData>
}

fn toggle_element_class(element: &str, class: &str, force: Option<bool>) {
	let e = get_element_by_id::<HtmlElement>(element).unwrap();
	let _ = if let Some(forced_value) = force {
		e.class_list().toggle_with_force(class, forced_value)
	} else {
		e.class_list().toggle(class)
	};
}

fn toggle_sidebar(force: Option<bool>) {
	// need to invert the forced value because the class has negative semantics
	toggle_element_class("sidebar", "sidebar--collapsed", force.map(|b| !b));
}

fn toggle_readonly(force: Option<bool>) {
	toggle_element_class("app-root", "readonly", force);
}

impl App {
	fn load_saved(&self, context: &Context<Self>, id: u32) {
		let link = &context.link();
		let initiate_load = link.callback(Message::InitiateLoad);
		let hide_sidebar = link.batch_callback(|_: ()| {
			toggle_sidebar(Some(false));
			None
		});
		let fail = link.callback(|_: ()| Message::RouteBridgeEvent(RouteBusEvent::RouteChanged(Route::Index)));
		spawn_local(async move {
			match api::get_timeline(id).await {
				ApiResult::Success((amalgam, encounter)) => {
					let mut partial = Box::new(PartialAmalgam::from(amalgam));
					partial.sanitize(&encounter);
					let offsets = partial.offsets.take();
					initiate_load.emit((Some(id), encounter, offsets));
					AmalgamBus::dispatcher().send(AmalgamBusIntent::Load(partial));
					hide_sidebar.emit(());
				},
				ApiResult::Error(error) => {
					alert(&error);
					fail.emit(());
				}
			}
		});
	}

	fn change_id(&self, context: &Context<Self>, id: u32) {
		if let Some((Some(old_id), _, _)) = self.last_load {
			if id == old_id {
				return;
			}
		}
		self.load_saved(context, id);
	}
}

impl Component for App {
	type Message = Message;
	type Properties = ();

	fn create(context: &Context<Self>) -> Self {
		let mut bridge = RouteBus::bridge(context.link().callback(Self::Message::RouteBridgeEvent));
		bridge.send(RouteBusIntent::Subscribe);
		Self {
			route_bridge: bridge,
			last_load: None
		}
	}

	fn update(&mut self, context: &Context<Self>, msg: Self::Message) -> bool {
		match msg {
			Self::Message::ToggleReadOnly(force) => {
				self.route_bridge.send(RouteBusIntent::ToggleReadOnly);
				toggle_readonly(force);
				false
			},
			Self::Message::InitiateLoad(data) => {
				let mut ability_dispatcher = AbilityBus::dispatcher();
				ability_dispatcher.send(AbilityBusIntent::Clear);
				ability_dispatcher.send(AbilityBusIntent::ChangeSync(data.1.sync));
				self.last_load = Some(data);
				true
			},
			Self::Message::UsePreset(name) => {
				let callback = context.link().callback(|encounter| Self::Message::InitiateLoad((None, encounter, None)));
				spawn_local(async move {
					match api::load_preset(&name).await {
						ApiResult::Success(encounter) => {
							AmalgamBus::dispatcher().send(AmalgamBusIntent::PresetChanged(name));
							callback.emit(encounter);
						},
						ApiResult::Error(e) => log!("Got error when loading preset: {}", e)
					}
				});
				false
			},
			Self::Message::RouteBridgeEvent(event) => match event {
				RouteBusEvent::CurrentRoute(route) => {
					match route {
						Route::Index => {},
						Route::Edit(id) => self.load_saved(context, id),
						Route::ReadOnly(id) => {
							toggle_readonly(Some(true));
							self.load_saved(context, id);
						}
					}
					false
				},
				RouteBusEvent::RouteChanged(route) => {
					match route {
						Route::Index => {
							toggle_readonly(Some(false));
							route.go_to();
						},
						Route::Edit(id) => {
							toggle_readonly(Some(false));
							self.change_id(context, id);
						},
						Route::ReadOnly(id) => {
							toggle_readonly(Some(true));
							self.change_id(context, id);
						}
					}
					false
				},
				RouteBusEvent::IdChanged(id) => {
					if let Some((Some(old_id), _, _)) = &mut self.last_load {
						*old_id = id;
					}
					false
				}
			}
		}
	}

	fn view(&self, context: &Context<Self>) -> Html {
		lazy_static! {
			static ref DEFAULT_STATE: LoadData = (
				None,
				Encounter {
					sync: MAX_LEVEL,
					phases: vec![]
				},
				None
			);
		}

		let link = &context.link();
		let state = if let Some(data) = &self.last_load {
			data
		} else {
			&*DEFAULT_STATE
		};
		let toggle_sidebar = link.batch_callback(|_: MouseEvent| {
			toggle_sidebar(None);
			None
		});
		html! {
			<>
				<div id="app-root" class="app">
					<div class="sidebar" id="sidebar">
						<div class="sidebar__contents">
							<div class="config">
								<Preset on_load={link.callback(Self::Message::UsePreset)}/>
								<hr/>
								<State/>
								<hr/>
								<Filter/>
							</div>
							<div class="flex-filler"></div>
							<a href="https://gitlab.com/Req/mitplan" target="_blank">{"Source"}</a>
						</div>
						<button class="sidebar__toggle colored-widget" onclick={toggle_sidebar.clone()}></button>
					</div>
					<main>
						<div class="fader" onclick={toggle_sidebar}></div>
						<button class="readonly-toggle" onclick={link.callback(|_: MouseEvent| Self::Message::ToggleReadOnly(None))}>{"Read-only"}</button>
						<Timeline encounter={state.1.clone()} offsets={state.2.clone()}/>
					</main>
				</div>
			</>
		}
	}
}
