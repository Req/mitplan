
use web_sys::MouseEvent;
use yew::{
	Component,
	Context,
	prelude::Html
};
use yew_agent::{
	Bridge,
	Bridged,
	Dispatched,
	Dispatcher
};
use crate::{
	ability::CATEGORIES,
	ability_manager::DEFAULT_FILTERS,
	agents::{
		ability::{
			AbilityBus,
			AbilityBusIntent
		},
		amalgam::{
			AmalgamBus,
			AmalgamBusEvent
		}
	}
};

pub enum Message {
	ToggleFilter(usize),
	ForceFilter(usize, bool),
	AmalgamBridgeEvent(AmalgamBusEvent)
}

pub struct Filter {
	bus: Dispatcher<AbilityBus>,
	_amalgam_bridge: Box<dyn Bridge<AmalgamBus>>,
	filters: [bool; 3]
}

impl Component for Filter {
	type Message = Message;
	type Properties = ();

	fn create(context: &Context<Self>) -> Self {
		let amalgam_bridge = AmalgamBus::bridge(context.link().callback(Self::Message::AmalgamBridgeEvent));
		Self {
			bus: AbilityBus::dispatcher(),
			_amalgam_bridge: amalgam_bridge,
			filters: DEFAULT_FILTERS
		}
	}

	fn update(&mut self, context: &Context<Self>, msg: Self::Message) -> bool {
		match msg {
			Self::Message::ToggleFilter(index) => {
				if index == 0 {
					return false;
				}
				context.link().send_message(Self::Message::ForceFilter(index, !self.filters[index]));
				false
			},
			Self::Message::ForceFilter(index, filter) => {
				self.filters[index] = filter;
				self.bus.send(AbilityBusIntent::ChangeFilter(index, self.filters[index]));
				true
			},
			Self::Message::AmalgamBridgeEvent(event) => match event {
				AmalgamBusEvent::LoadFrom(partial) => {
					if partial.filters.is_some() {
						for (index, filter) in partial.filters.unwrap().iter().enumerate() {
							context.link().send_message(Self::Message::ForceFilter(index, *filter));
						}
					}
					false
				},
				_ => false
			}
		}
	}

	fn view(&self, context: &Context<Self>) -> Html {
		html! {
			<div class="config__filters">
				<span>{"Ability Filters"}</span>
				<ul>
					{CATEGORIES.iter().enumerate().map(|(index, category)| {
						let on_change = context.link().callback(move |_: MouseEvent| Self::Message::ToggleFilter(index));
						html! {
							/*
							 * Yew panics when target != currentTarget, so it's easier to make custom "checkboxes"
							 * than to work around the standard <li><input/><label/></li> setup where you would put
							 * the event handler on the <li> so both the <input> and <label> can be clicked.
							 */
							<li
								onclick={on_change}
								data-checked={(self.filters[index] as u32).to_string()}
								data-disabled={((index == 0) as u32).to_string()}
							>{category.to_string()}</li>
						}
					}).collect::<Vec<Html>>()}
				</ul>
			</div>
		}
	}
}
