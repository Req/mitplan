
use wasm_bindgen::JsCast;
use web_sys::{
	HtmlElement,
	HtmlSelectElement,
	MouseEvent
};
use yew::{
	Callback,
	Component,
	Context,
	prelude::Html
};
use crate::wrappers::get_element_by_id;

const AVAILABLE_PRESETS: [&str; 16] = [
	"P12S-2",
	"P12S-1",
	"P11S",
	"P10S",
	"P9S",
	"P8S-1 (Snake)",
	"P8S-1 (Centaur)",
	"P6S",
	"P5S",
	"DSR",
	"DSR (door)",
	"P4S-2",
	"P4S-1",
	"P3S",
	"TEA",
	"UCOB",
];

pub enum Message {
	LoadPreset(String)
}

#[derive(PartialEq, Properties)]
pub struct Props {
	pub on_load: Callback<String>
}

pub struct Preset {}

impl Component for Preset {
	type Message = Message;
	type Properties = Props;

	fn create(_context: &Context<Self>) -> Self {
		Self {}
	}

	fn update(&mut self, context: &Context<Self>, msg: Self::Message) -> bool {
		match msg {
			Self::Message::LoadPreset(name) => {
				context.props().on_load.emit(name);
				false
			}
		}
	}

	fn view(&self, context: &Context<Self>) -> Html {
		let load_preset = context.link().callback(|_: MouseEvent| {
			let select = get_element_by_id::<HtmlSelectElement>("preset-selector").unwrap();
			let option = select.options().item(select.selected_index() as u32).unwrap();
			let option = option.dyn_into::<HtmlElement>().unwrap();
			Self::Message::LoadPreset(option.inner_text())
		});

		html! {
			<div class="config__preset">
				<span>{"Load Blank Timeline Preset"}</span>
				<select class="colored-widget" id="preset-selector">
					{for AVAILABLE_PRESETS.map(|preset| html! { <option>{preset}</option> })}
				</select>
				<button class="colored-widget--neutral" onclick={load_preset}>{"Load Timeline"}</button>
			</div>
		}
	}
}
