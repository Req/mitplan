
use wasm_bindgen_futures::spawn_local;
use web_sys::{
	HtmlElement,
	HtmlInputElement,
	MouseEvent
};
use yew::{
	Component,
	Context,
	prelude::Html
};
use yew_agent::{
	Bridge,
	Bridged,
	Dispatched
};
use mitplan_shared::{
	amalgam::Amalgam,
	api::ApiResult
};
use crate::{
	agents::{
		amalgam::{
			AmalgamBus,
			AmalgamBusEvent,
			AmalgamBusIntent,
			ApiIntent
		},
		route::{
			Path,
			RouteBus,
			RouteBusIntent
		}
	},
	api,
	wrappers::get_element_by_id
};

pub enum Message {
	BridgeEvent(AmalgamBusEvent),
	Save,
	Copy,
	Delete,
	InitApiCall(ApiIntent),
	EndApiCall(Option<String>)
}

pub struct State {
	bridge: Box<dyn Bridge<AmalgamBus>>,
	call_in_progress: bool
}

fn get_password() -> String {
	get_element_by_id::<HtmlInputElement>("state-password").unwrap().value()
}

impl State {
	fn do_api_call(&mut self, context: &Context<Self>, intent: ApiIntent, amalgam: Amalgam) -> bool {
		let id = Path::current().route().unwrap().id();
		if matches!(intent, ApiIntent::Save) && id.is_none() {
			return self.do_api_call(context, ApiIntent::Copy, amalgam);
		}
		let password = get_password();
		let end = context.link().callback(Message::EndApiCall);
		spawn_local(async move {
			match intent {
				ApiIntent::Copy => match api::create_timeline(amalgam, password).await {
					ApiResult::Success(id) => {
						RouteBus::dispatcher().send(RouteBusIntent::ChangeId(id));
						end.emit(None);
					},
					ApiResult::Error(error) => end.emit(Some(error))
				},
				ApiIntent::Delete => {
					if id.is_none() {
						end.emit(Some("Not saved - can't delete".to_string()));
						return;
					}
					match api::delete_timeline(id.unwrap(), password).await {
						ApiResult::Success(_) => {
							RouteBus::dispatcher().send(RouteBusIntent::New);
							// logically unreachable but probably reachable in practice
							end.emit(None);
						},
						ApiResult::Error(error) => end.emit(Some(error))
					}
				},
				// id is guaranteed to be Some because it's checked earlier
				ApiIntent::Save => match api::edit_timeline(id.unwrap(), amalgam, password).await {
					ApiResult::Success(_) => end.emit(None),
					ApiResult::Error(error) => end.emit(Some(error))
				}
			}
		});
		false
	}
}

impl Component for State {
	type Message = Message;
	type Properties = ();

	fn create(context: &Context<Self>) -> Self {
		let bridge = AmalgamBus::bridge(context.link().callback(Message::BridgeEvent));
		Self {
			bridge,
			call_in_progress: false
		}
	}

	fn update(&mut self, context: &Context<Self>, msg: Self::Message) -> bool {
		match msg {
			Self::Message::BridgeEvent(e) => match e {
				AmalgamBusEvent::Compiled(amalgam, intent) => self.do_api_call(context, intent, *amalgam),
				AmalgamBusEvent::LoadFrom(_) => false,
				AmalgamBusEvent::BeginCompile(_) => false
			},
			Self::Message::Save => {
				context.link().send_message(Self::Message::InitApiCall(ApiIntent::Save));
				false
			},
			Self::Message::Copy => {
				context.link().send_message(Self::Message::InitApiCall(ApiIntent::Copy));
				false
			},
			Self::Message::Delete => {
				context.link().send_message(Self::Message::InitApiCall(ApiIntent::Delete));
				false
			},
			Self::Message::InitApiCall(intent) => {
				if self.call_in_progress {
					return false;
				}
				self.call_in_progress = true;
				self.bridge.send(AmalgamBusIntent::Compile(intent));
				true
			},
			Self::Message::EndApiCall(error) => {
				self.call_in_progress = false;
				let error = error.unwrap_or_default();
				get_element_by_id::<HtmlElement>("config__state__errors").unwrap().set_inner_text(&error);
				true
			}
		}
	}

	fn view(&self, context: &Context<Self>) -> Html {
		let in_progress = if self.call_in_progress { "api-call-in-progress" } else { "" };
		html! {
			<div class={format!("config__state {}", in_progress)}>
				<span>{"Manage Timeline State"}</span>
				<input id="state-password" type="password" placeholder="Password" autocomplete="off"/>
				<button class="colored-widget--neutral" onclick={context.link().callback(|_: MouseEvent| Self::Message::Save)}>{"Overwrite"}</button>
				<button class="colored-widget--positive" onclick={context.link().callback(|_: MouseEvent| Self::Message::Copy)}>{"Save copy"}</button>
				<button class="colored-widget--negative" onclick={context.link().callback(|_: MouseEvent| Self::Message::Delete)}>{"Delete"}</button>
				<div id="state-oscillator" class="oscillator-container">
					<div class="oscillator"></div>
				</div>
				<span id="config__state__errors"></span>
			</div>
		}
	}
}
