
use wasm_bindgen::JsCast;
use web_sys::{
	HtmlInputElement,
	InputEvent,
	MouseEvent
};
use yew::{
	Callback,
	Component,
	Context,
	prelude::Html,
	Properties
};
use mitplan_shared::amalgam::{
	Mechanic,
	PhaseOffsets
};
use crate::{
	ability::Ability,
	ability_manager::State,
	components::common::{
		format_time,
		image_file_name
	},
	recharge::Period
};

pub enum Message {
	Use,
	Close,
	OffsetChanged(InputEvent)
}

#[derive(Debug, PartialEq)]
pub struct Offsets {
	period: Period,
	current_time: i32,
	upper_offset: String,
	lower_offset: String,
	upper_time: String,
	lower_time: String
}

impl Offsets {
	fn garbage_state() -> Self {
		Self {
			period: (0, 0),
			current_time: 0,
			upper_offset: String::new(),
			lower_offset: String::new(),
			upper_time: String::new(),
			lower_time: String::new()
		}
	}

	fn from(mechanic: &Mechanic, ability: &Ability, state: &State, phase_offsets: &PhaseOffsets) -> Result<Self, ()> {
		let mechanic_time = mechanic.adjusted_time(phase_offsets);
		let period = match state {
			State::Available(mut period) => {
				/*
				 * The ability is available and the mechanic occurs within [A, B].
				 * B cannot positively exceed the mechanic timestamp.
				 * A cannot negatively exceed `adjusted_B - ability.duration`.
				 * If B is invalid (i32::min), then the menu should be closed.
				 */
				if period.1 == i32::MIN {
					return Err(());
				}
				period.1 = period.1.min(mechanic_time);
				period.0 = period.0.max(period.1 - ability.duration());
				period
			},
			State::Unavailable(mut period) => {
				/*
				 * The ability is available within [A, B], but the mechanic occurs after B.
				 * A cannot negatively exceed `mechanic.time - ability.duration`.
				 * If B is invalid (i32::min), or if `mechanic.time - B` exceeds `ability.duration`,
				 * then the menu should be closed.
				 */
				let duration = ability.duration();
				if period.1 == i32::MIN || mechanic_time - period.1 > duration {
					return Err(());
				}
				period.0 = period.0.max(mechanic_time - duration);
				period
			},
			State::Selected(_) => return Err(())
		};
		let upper_offset = mechanic_time - period.1;
		Ok(Offsets {
			period,
			current_time: mechanic_time - upper_offset,
			upper_offset: format_time(upper_offset),
			lower_offset: format_time(upper_offset + (period.1 - period.0)),
			upper_time: format_time(period.1),
			lower_time: format_time(period.0)
		})
	}
}

#[derive(Clone, PartialEq, Properties)]
pub struct Props {
	pub ability: &'static Ability,
	pub mechanic: Mechanic,
	pub state: State,
	pub phase_offsets: PhaseOffsets,
	pub on_use: Callback<(&'static Ability, i32)>,
	pub on_close: Callback<()>
}

pub struct Menu {
	offsets: Offsets,
	closed: bool
}

impl Menu {
	fn close(&self, context: &Context<Self>) {
		context.props().on_close.emit(());
	}
}

impl Component for Menu {
	type Message = Message;
	type Properties = Props;

	fn create(context: &Context<Self>) -> Self {
		let props = &context.props();
		assert!(!matches!(props.state, State::Selected(..)));
		if let Ok(offsets) = Offsets::from(&props.mechanic, props.ability, &props.state, &props.phase_offsets) {
			Self {
				offsets,
				closed: false
			}
		} else {
			let this = Self {
				offsets: Offsets::garbage_state(),
				closed: false
			};
			this.close(context);
			this
		}
	}

	fn update(&mut self, context: &Context<Self>, msg: Self::Message) -> bool {
		match msg {
			Self::Message::Use => {
				let props = &context.props();
				props.on_use.emit((props.ability, self.offsets.current_time));
				self.close(context);
				false
			},
			Self::Message::Close => {
				self.closed = true;
				self.close(context);
				false
			},
			Self::Message::OffsetChanged(event) => {
				// no need to validate the value since it's coming from an input range
				let target: HtmlInputElement = event.target().unwrap().dyn_into().unwrap();
				let time = target.value().parse::<i32>().unwrap();
				if self.offsets.current_time != time {
					self.offsets.current_time = time;
					return true;
				}
				false
			}
		}
	}

	fn changed(&mut self, context: &Context<Self>) -> bool {
		let props = &context.props();
		if let Ok(offsets) = Offsets::from(&props.mechanic, props.ability, &props.state, &props.phase_offsets) {
			if self.offsets != offsets {
				self.offsets = offsets;
				return true;
			}
		} else {
			self.close(context);
			return true;
		}
		false
	}

	fn view(&self, context: &Context<Self>) -> Html {
		let props = &context.props();
		if let State::Selected(_) = props.state {
			self.close(context);
			return html!{};
		}
		let label = match props.state {
			State::Available(_) => format!("Usable up to {} early", self.offsets.lower_offset),
			State::Unavailable(_) => format!("Available {} before", self.offsets.upper_offset),
			State::Selected(_) => unreachable!()
		};

		let mechanic_time = props.mechanic.adjusted_time(&props.phase_offsets);

		html! {
			<div class="ability-menu">
				<div class="ability-menu__header">
					<div class="pseudo-image" alt={image_file_name(props.ability)}></div>
					<span>{format!("{} @ {}", props.mechanic.name, format_time(mechanic_time))}</span>
				</div>
				<span>{label}</span>
				<input type="range" min={self.offsets.period.0.to_string()} max={self.offsets.period.1.to_string()} step="1" value={self.offsets.current_time.to_string()} oninput={context.link().callback(Self::Message::OffsetChanged)}/>
				<div class="ability-menu__labels">
					<span>
						{self.offsets.lower_time.clone()}
						<br/>
						{format!("(-{})", self.offsets.lower_offset)}
					</span>
					<span>
						{format_time(self.offsets.current_time)}
						<br/>
						{format!("(-{})", format_time(mechanic_time - self.offsets.current_time))}
					</span>
					<span>
						{self.offsets.upper_time.clone()}
						<br/>
						{format!("(-{})", self.offsets.upper_offset)}
					</span>
				</div>
				<div class="ability-menu__buttons">
					<button class="symbol-button--positive" onclick={context.link().callback(|_: MouseEvent| Self::Message::Use)}>{"✓"}</button>
					<button class="symbol-button--negative" onclick={context.link().callback(|_: MouseEvent| Self::Message::Close)}>{"✗"}</button>
				</div>
			</div>
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use mitplan_shared::amalgam::MechanicId;
	use crate::jobs::sch::SOIL;

	#[test]
	fn period_info() {
		let mechanic = Mechanic {
			name: "a mechanic".to_string(),
			time: 45,
			tags: vec![],
			id: MechanicId::create("", "", 0, &mut || {0})
		};
		let zero_offset = PhaseOffsets {
			start_offset: 0,
			end_offset: 0
		};

		assert!(Offsets::from(&mechanic, &*SOIL, &State::Selected(0), &zero_offset).is_err());

		let offsets = Offsets::from(&mechanic, &*SOIL, &State::Available((20, 50)), &zero_offset).unwrap();
		assert_eq!(offsets, Offsets {
			period: (30, 45),
			current_time: 45,
			upper_offset: "00:00".to_string(),
			lower_offset: "00:15".to_string(),
			upper_time: "00:45".to_string(),
			lower_time: "00:30".to_string()
		});

		let offsets = Offsets::from(&mechanic, &*SOIL, &State::Unavailable((5, 35)), &zero_offset).unwrap();
		assert_eq!(offsets, Offsets {
			period: (30, 35),
			current_time: 35,
			upper_offset: "00:10".to_string(),
			lower_offset: "00:15".to_string(),
			upper_time: "00:35".to_string(),
			lower_time: "00:30".to_string()
		});
	}
}
