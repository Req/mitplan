
use yew::{
	Callback,
	Component,
	Context,
	MouseEvent,
	prelude::Html,
	Properties
};
use mitplan_shared::job::Job;

lazy_static! {
	static ref JOBS: Vec<Vec<Job>> = vec![
		vec![Job::Tank, Job::PLD, Job::WAR, Job::DRK, Job::GNB],
		vec![Job::Healer, Job::WHM, Job::SCH, Job::AST, Job::SGE],
		vec![Job::Melee, Job::MNK, Job::DRG, Job::NIN, Job::SAM, Job::RPR, Job::VPR],
		vec![Job::Ranged, Job::BRD, Job::MCH, Job::DNC],
		vec![Job::Caster, Job::BLM, Job::SMN, Job::RDM, Job::PCT, Job::DUM]
	];
}

pub enum Message {
	ChangeJob(Job)
}

#[derive(PartialEq, Properties)]
pub struct Props {
	pub index: usize,
	pub job: Job,
	pub on_change: Callback<(usize, Job)>
}

pub struct JobSelector {}

impl Component for JobSelector {
	type Message = Message;
	type Properties = Props;

	fn create(_context: &Context<Self>) -> Self {
		Self {}
	}

	fn update(&mut self, context: &Context<Self>, msg: Self::Message) -> bool {
		match msg {
			Self::Message::ChangeJob(job) => {
				let props = &context.props();
				props.on_change.emit((props.index, job));
				false
			}
		}
	}

	fn view(&self, context: &Context<Self>) -> Html {
		html! {
			<button class="job-selector">
				<div class="pseudo-image" alt={context.props().job.to_string()}></div>
				<div class="job-selector__popup">
					{for JOBS.iter().map(|role| html! {
						<div class="job-selector__popup__role-group">
							{for role.iter().map(|job| html! {
								<div class="pseudo-image" alt={job.to_string()} onclick={context.link().callback(move |_: MouseEvent| Self::Message::ChangeJob(*job))}></div>
							})}
						</div>
					})}
				</div>
			</button>
		}
	}
}
