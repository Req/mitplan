
mod cell;
mod job_selector;
mod menu;
mod phase;
mod phase_time;

use std::rc::Rc;
use yew::{
	Context,
	Component,
	prelude::Html,
	Properties
};
use yew_agent::{
	Bridge,
	Bridged,
	Dispatched,
	Dispatcher
};
use mitplan_shared::{
	amalgam::{
		Encounter,
		PhaseOffsets
	},
	job::Job
};
use crate::{
	agents::{
		ability::{
			AbilityBus,
			AbilityBusIntent
		},
		amalgam::{
			AmalgamBus,
			AmalgamBusEvent,
			AmalgamBusIntent
		}
	},
	components::timeline::{
		job_selector::JobSelector,
		phase::Phase
	}
};

pub enum Message {
	AmalgamBridgeEvent(AmalgamBusEvent),
	ChangeJob(usize, Job),
	ChangePhaseEnd(i32, i32)
}

pub struct Timeline {
	amalgam_bridge: Box<dyn Bridge<AmalgamBus>>,
	ability_bus: Dispatcher<AbilityBus>,
	phase_offsets: Vec<PhaseOffsets>,
	jobs: [Job; 8]
}

#[derive(PartialEq, Properties)]
pub struct Props {
	pub encounter: Encounter,
	pub offsets: Option<Vec<PhaseOffsets>>,
	#[prop_or_default]
	pub jobs: [Job; 8]
}

impl Props {
	fn make_offsets(&self) -> Vec<PhaseOffsets> {
		if let Some(offsets) = &self.offsets {
			offsets.clone()
		} else {
			vec![PhaseOffsets::default(); self.encounter.phases.len()]
		}
	}
}

impl Component for Timeline {
	type Message = Message;
	type Properties = Props;

	fn create(context: &Context<Self>) -> Self {
		let props = &context.props();
		let amalgam_bridge = AmalgamBus::bridge(context.link().callback(Self::Message::AmalgamBridgeEvent));
		Self {
			amalgam_bridge,
			ability_bus: AbilityBus::dispatcher(),
			phase_offsets: props.make_offsets(),
			jobs: props.jobs
		}
	}

	fn update(&mut self, context: &Context<Self>, msg: Self::Message) -> bool {
		match msg {
			Self::Message::ChangeJob(index, job) => {
				assert!(index < 8);
				if self.jobs[index] != job {
					self.jobs[index] = job;
					self.ability_bus.send(AbilityBusIntent::ChangeJob(index, job));
					return true;
				}
				false
			},
			Self::Message::ChangePhaseEnd(from, to) => {
				if from == to {
					return false;
				}
				let delta = to - from;
				let props = &context.props();
				/*
				 * There is zero support for changing the end so drastically that the phase ends before it starts (or similar).
				 * Ideally, PhaseTime is made aware of the bounds within which it can work, but that would require
				 * Phase to know about phases other than its own, so while ideal it's not particularly realistic.
				 */

				if let Some(index) = props.encounter.phases.iter().enumerate().position(|(index, phase)| phase.adjusted_end(&self.phase_offsets[index]) == from) {
					self.phase_offsets[index].end_offset += delta;
					// mechanics in the originating phase aren't affected
					for index in index + 1..props.encounter.phases.len() {
						self.phase_offsets[index].start_offset += delta;
					}
					let phase = &props.encounter.phases[index];
					// zero support for phases with no mechanics
					if !phase.mechanics.is_empty() {
						self.ability_bus.send(AbilityBusIntent::ShiftAfterBy(phase.mechanics.last().unwrap().id, from, to));
					}
					true
				} else {
					log!("Warning - trying to change the end time for a phase that doesn't exist in the timeline.");
					/*
					 * This is probably unreachable, but I'm implementing it anyway.
					 * The change can't propagate to the ability manager without a reference mechanic,
					 * which is impossible to obtain without an originating phase. Phases/mechanics
					 * also can't safely be updated, so the only solution is to force a rerender to
					 * force a correct state onto the offending downstream PhaseTime.
					 */
					true
				}
			},
			Self::Message::AmalgamBridgeEvent(event) => match event {
				AmalgamBusEvent::BeginCompile(mut partial) => {
					let partial = unsafe { Rc::get_mut_unchecked(&mut partial) };
					partial.jobs = Some(self.jobs);
					partial.offsets = Some(self.phase_offsets.clone());
					self.amalgam_bridge.send(AmalgamBusIntent::NotifyPartial);
					false
				},
				AmalgamBusEvent::LoadFrom(mut partial) => {
					let partial = unsafe { Rc::get_mut_unchecked(&mut partial) };
					if partial.jobs.is_some() {
						for (index, job) in partial.jobs.unwrap().iter().enumerate() {
							context.link().send_message(Self::Message::ChangeJob(index, *job));
						}
					}
					false
				},
				AmalgamBusEvent::Compiled(_, _) => false
			}
		}
	}

	fn changed(&mut self, context: &Context<Self>) -> bool {
		let offsets = context.props().make_offsets();
		if self.phase_offsets != offsets {
			self.phase_offsets = offsets;
			return true;
		}
		false
	}

	fn view(&self, context: &Context<Self>) -> Html {
		let link = &context.link();
		let change_job = link.callback(|(index, job)| Self::Message::ChangeJob(index, job));
		let change_end = link.callback(|(from, to)| Self::Message::ChangePhaseEnd(from, to));
		html! {
			<div class="timeline">
				<table>
					<tbody>
						<tr>
							<td></td>
							<td></td>
							{for self.jobs.iter().enumerate().map(|(index, job)| html! {
								<td class="job-selector-container highlight-side-borders">
									<JobSelector index={index} job={*job} on_change={change_job.clone()}/>
								</td>
							})}
						</tr>
					</tbody>
					{for context.props().encounter.phases.iter().enumerate().map(|(index, phase)| html! {
						<Phase phase={phase.clone()} offsets={self.phase_offsets[index]} on_end_change={change_end.clone()}/>
					})}
				</table>
			</div>
		}
	}
}
