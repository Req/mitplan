
use web_sys::MouseEvent;
use yew::{
	Callback,
	Component,
	Context,
	prelude::Html,
	Properties
};
use mitplan_shared::amalgam::PhaseOffsets;
use crate::components::{
	common::{
		format_time,
		junk_cells
	},
	timeline::{
		cell::Cell,
		phase_time::PhaseTime
	}
};

pub enum Message {
	ToggleCollapse
}

#[derive(PartialEq, Properties)]
pub struct Props {
	// i haven't checked, but i assume partialeq will evaluate by order of appearance in the struct,
	// so putting offsets before phase makes it likely to short circuit the hefty phase partialeq
	pub offsets: PhaseOffsets,
	pub on_end_change: Callback<(i32, i32)>,
	pub phase: mitplan_shared::amalgam::Phase,
}

pub struct Phase {
	collapsed: bool
}

impl Component for Phase {
	type Message = Message;
	type Properties = Props;

	fn create(_link: &Context<Self>) -> Self {
		Self {
			collapsed: false
		}
	}

	fn update(&mut self, _context: &Context<Self>, msg: Self::Message) -> bool {
		match msg {
			Self::Message::ToggleCollapse => {
				self.collapsed = !self.collapsed;
				true
			}
		}
	}

	fn view(&self, context: &Context<Self>) -> Html {
		let props = &context.props();
		let toggle_collapse = context.link().callback(|_: MouseEvent| Self::Message::ToggleCollapse);
		let collapsed = if self.collapsed { "timeline__collapse-phase" } else { "" };
		let adjusted_end = props.phase.adjusted_end(&props.offsets);
		let adjusted_enrage = props.phase.adjusted_enrage(&props.offsets);
		html! {
			<tbody class={collapsed}>
				<tr class="timeline__phase" onclick={toggle_collapse}>
					<th>{format!("Phase: {}", props.phase.name)}</th>
					{for junk_cells(9)}
				</tr>
				{for props.phase.mechanics.iter().map(|mechanic| {
					let adjusted_time = mechanic.adjusted_time(&props.offsets);
					let disabled = if adjusted_time > adjusted_end { "timeline__mechanic--disabled" } else { "" };
					html! {
						<tr class={format!("timeline__mechanic {}", disabled)}>
							<td>{format_time(adjusted_time)}</td>
							<td>{&mechanic.name}</td>
							{for (0..8).map(|index| html! {
								<Cell index={index} mechanic={mechanic.clone()} offsets={props.offsets}/>
							})}
						</tr>
					}
				})}
				<tr class="timeline__end">
					<td>
						<PhaseTime end={adjusted_end} enrage={adjusted_enrage} on_change={props.on_end_change.clone()}/>
					</td>
					<td>{"End / Enrage"}</td>
					{for junk_cells(8)}
				</tr>
			</tbody>
		}
	}
}
