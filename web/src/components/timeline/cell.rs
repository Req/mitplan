
use web_sys::MouseEvent;
use yew::{
	Component,
	Context,
	prelude::Html,
	Properties
};
use yew_agent::{
	Bridge,
	Bridged
};
use mitplan_shared::amalgam::{
	Mechanic,
	PhaseOffsets
};
use crate::{
	ability::Ability,
	ability_manager::{
		AbilityState,
		Identity,
		State
	},
	agents::ability::{
		AbilityBus,
		AbilityBusEvent,
		AbilityBusIntent
	},
	components::{
		common::image_file_name,
		timeline::menu::Menu
	},
	mechanic_ability_filter::mechanic_ability_filter
};

pub enum Message {
	BridgeEvent(AbilityBusEvent),
	ToggleAbility(&'static Ability),
	ChangeMenuTarget(MouseEvent, Option<&'static Ability>),
	MenuUse((&'static Ability, i32)),
	MenuClose
}

#[derive(PartialEq, Properties)]
pub struct Props {
	pub index: usize,
	pub mechanic: Mechanic,
	pub offsets: PhaseOffsets
}

pub struct Cell {
	bridge: Box<dyn Bridge<AbilityBus>>,
	state: Vec<AbilityState>,
	menu_target: Option<&'static Ability>
}

impl Cell {
	fn identity(&self, context: &Context<Self>) -> Identity {
		let props = &context.props();
		(props.index, props.mechanic.id)
	}
}

fn state_string(state: State, mechanic: &Mechanic, ability: &Ability) -> &'static str {
	if !mechanic_ability_filter(mechanic, ability) {
		return "disabled";
	}
	match state {
		State::Selected(_) => "selected",
		State::Unavailable(_) => "unavailable",
		State::Available(_) => "available"
	}
}

impl Component for Cell {
	type Message = Message;
	type Properties = Props;

	fn create(context: &Context<Self>) -> Self {
		let bridge = AbilityBus::bridge(context.link().callback(Message::BridgeEvent));
		let mut this = Self {
			bridge,
			state: vec![],
			menu_target: None
		};
		this.bridge.send(AbilityBusIntent::Refresh);
		this
	}

	fn update(&mut self, context: &Context<Self>, msg: Self::Message) -> bool {
		let props = &context.props();
		match msg {
			Self::Message::BridgeEvent(event) => match event {
				AbilityBusEvent::Refresh(manager, index) => {
					if index.is_some() && index.unwrap() != props.index {
						return false;
					}
					let state = manager.request_state(self.identity(context), props.mechanic.adjusted_time(&props.offsets));
					if self.state != state {
						self.state = state;
						self.menu_target = None;
						return true;
					}
					false
				}
			},
			Self::Message::ToggleAbility(ability) => {
				self.bridge.send(AbilityBusIntent::ToggleAbility(self.identity(context), ability, props.mechanic.adjusted_time(&props.offsets)));
				false
			},
			Self::Message::ChangeMenuTarget(event, ability) => {
				if event.button() == 2 {
					event.prevent_default();
					if self.menu_target != ability {
						self.menu_target = ability;
						return true;
					}
				}
				false
			},
			Self::Message::MenuUse((ability, time)) => {
				self.bridge.send(AbilityBusIntent::ToggleAbility(self.identity(context), ability, time));
				false
			},
			Self::Message::MenuClose => {
				self.menu_target = None;
				true
			}
		}
	}

	fn changed(&mut self, _context: &Context<Self>) -> bool {
		self.menu_target = None;
		self.bridge.send(AbilityBusIntent::Refresh);
		false
	}

	fn view(&self, context: &Context<Self>) -> Html {
		let props = &context.props();
		let menu = if let Some(ability) = self.menu_target {
			let on_use = context.link().callback(Self::Message::MenuUse);
			let on_close = context.link().callback(|_: ()| Self::Message::MenuClose);
			let state = self.state.iter().find(|(rhs, _)| &ability == rhs).unwrap().1;
			html! {
				<Menu ability={ability} mechanic={props.mechanic.clone()} phase_offsets={props.offsets} state={state} on_use={on_use} on_close={on_close}/>
			}
		} else {
			html!{}
		};
		html! {
			<td class="ability-cell">
				{menu}
				<div class="ability-cell__contents">
					{for self.state.iter().map(|(ability, state)| {
						let highlight = if self.menu_target == Some(ability) {
							"highlight-ability-cell"
						} else {
							""
						};
						let name = image_file_name(ability);
						/*
						* Abilities are 'static. Here, they're reduced to the lifetime of &self,
						* so we need to promote them back up to 'static before they can be passed
						* around inside messages.
						*/
						let ability: &'static Ability = unsafe { std::mem::transmute(*ability) };
						let toggle = match state {
							State::Unavailable(_) => context.link().batch_callback(|_: MouseEvent| None),
							_ => context.link().batch_callback(move |_: MouseEvent| Some(Self::Message::ToggleAbility(ability)))
						};
						// disable opening a menu for something that's selected
						let (menu, offset) = if let State::Selected(time) = state {
							let callback = context.link().batch_callback(move |e: MouseEvent| {
								e.prevent_default();
								None
							});
							(callback, (-(props.mechanic.adjusted_time(&props.offsets) - time)).to_string())
						} else {
							let callback = context.link().batch_callback(move |e: MouseEvent| Some(Self::Message::ChangeMenuTarget(e, Some(ability))));
							(callback, "0".to_string())
						};
						let state_type = state_string(*state, &props.mechanic, ability);
						html! {
							<div class={format!("pseudo-image {}", highlight)} alt={name} onclick={toggle} oncontextmenu={menu} data-state={state_type} data-offset={offset}></div>
						}
					})}
				</div>
			</td>
		}
	}
}
