
use wasm_bindgen::JsCast;
use web_sys::{
	Event,
	HtmlInputElement,
	InputEvent,
	MouseEvent
};
use yew::{
	Callback,
	Component,
	Context,
	prelude::Html,
	Properties
};
use parser_shared::parse_timestamp;
use crate::components::common::format_time;

pub enum Message {
	Edit,
	Confirm,
	Cancel,
	Change(InputEvent)
}

#[derive(PartialEq, Properties)]
pub struct Props {
	pub end: i32,
	pub enrage: i32,
	pub on_change: Callback<(i32, i32)>
}

pub struct PhaseTime {
	editing: bool,
	current_end: String,
	valid: bool
}

impl Component for PhaseTime {
	type Message = Message;
	type Properties = Props;

	fn create(context: &Context<Self>) -> Self {
		Self {
			current_end: format_time(context.props().end),
			editing: false,
			valid: true
		}
	}

	fn update(&mut self, context: &Context<Self>, msg: Self::Message) -> bool {
		match msg {
			Self::Message::Edit => {
				self.editing = true;
				true
			},
			Self::Message::Confirm => {
				if !self.valid {
					return false;
				}
				self.editing = false;
				let new_end = parse_timestamp(&self.current_end).unwrap() as i32;
				let props = &context.props();
				props.on_change.emit((props.end, new_end));
				true
			},
			Self::Message::Cancel => {
				self.current_end = format_time(context.props().end);
				self.editing = false;
				self.valid = true;
				true
			},
			Self::Message::Change(event) => {
				let event = event.dyn_into::<Event>().unwrap();
				let target = event.target().unwrap().dyn_into::<HtmlInputElement>().unwrap();
				let value = target.value();
				for c in value.chars() {
					if !(('0'..='9').contains(&c) || c == ':') {
						// invalid character, so don't update the state but force a rerender to remove it
						return true;
					}
				}

				// contains only allowed characters, but may not have the correct format (yet)
				self.valid = parse_timestamp(&value).is_ok();
				self.current_end = value;
				true
			}
		}
	}

	fn changed(&mut self, context: &Context<Self>) -> bool {
		self.current_end = format_time(context.props().end);
		true
	}

	fn view(&self, context: &Context<Self>) -> Html {
		let props = &context.props();
		html! {
			{if self.editing {
				html! {
					<div class={format!("timeline__end__container {}", if self.valid { "" } else { "timeline__end__container--invalid" })}>
						<button class="symbol-button--positive" onclick={context.link().callback(|_: MouseEvent| Self::Message::Confirm)}>{"✓"}</button>
						<button class="symbol-button--negative" onclick={context.link().callback(|_: MouseEvent| Self::Message::Cancel)}>{"✗"}</button>
						<input type="text" value={self.current_end.clone()} oninput={context.link().callback(Self::Message::Change)}/>
					</div>
				}
			} else {
				let time = if props.end == props.enrage {
					format_time(props.end)
				} else {
					format!("{} / {}", format_time(props.end), format_time(props.enrage))
				};
				html! {
					<div class="timeline__end__container">
						<button class="symbol-button--neutral" onclick={context.link().callback(|_: MouseEvent| Self::Message::Edit)}>{"⚙"}</button>
						<span>{time}</span>
					</div>
				}
			}}
		}
	}
}
