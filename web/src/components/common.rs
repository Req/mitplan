
use yew::prelude::Html;
use crate::ability::Ability;

pub fn junk_cells(count: usize) -> Vec<Html> {
	(0..count).map(|_| html! { <td></td> }).collect()
}

pub fn sign_of(x: i32) -> &'static str {
	if x < 0 {
		"-"
	} else {
		""
	}
}

pub fn format_time(time: i32) -> String {
	let minutes = time / 60;
	let seconds = time % 60;
	format!("{}{:02}:{:02}", sign_of(time), minutes.abs(), seconds.abs())
}

pub fn image_file_name(ability: &Ability) -> String {
	ability.name().to_string().to_lowercase().replace(" ", "_").replace("'", "")
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn time() {
		assert_eq!(format_time(0), "00:00");
		assert_eq!(format_time(72), "01:12");
		assert_eq!(format_time(-72), "-01:12");
	}
}
