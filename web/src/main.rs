
#![feature(get_mut_unchecked)]
#![feature(map_try_insert)]

#![allow(clippy::tabs_in_doc_comments)]

#[macro_use]
extern crate async_recursion;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate yew;

#[macro_use]
mod wrappers;
mod ability;
mod ability_manager;
mod agents;
mod api;
mod components;
mod jobs;
mod mechanic_ability_filter;
mod recharge;

use crate::components::app::App;

fn main() {
	yew::start_app::<App>();
}
