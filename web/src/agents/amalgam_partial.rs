
use std::{
	collections::HashSet,
	rc::Rc
};
use mitplan_shared::{
	amalgam::{
		Amalgam,
		Encounter,
		PhaseOffsets,
		TimelineItem
	},
	job::Job
};

#[derive(Debug, Default)]
pub struct PartialAmalgam {
	pub preset: Option<String>,
	// to be taken by Timeline (for job selectors) and the manager
	pub jobs: Option<[Job; 8]>,
	// to be taken by the manager
	pub items: Option<[Vec<TimelineItem>; 8]>,
	// to be taken by the Config subcomponent and propagated to the manager as normal
	pub filters: Option<[bool; 3]>,
	// to be taken by App to pass down to Timeline
	pub offsets: Option<Vec<PhaseOffsets>>
}

impl PartialAmalgam {
	pub fn from(amalgam: Amalgam) -> Self {
		Self {
			preset: Some(amalgam.preset),
			jobs: Some(amalgam.jobs),
			items: Some(amalgam.items),
			filters: Some(amalgam.filters),
			offsets: Some(amalgam.offsets)
		}
	}

	pub fn is_complete(&self) -> bool {
		self.preset.is_some() && self.jobs.is_some() && self.items.is_some() && self.filters.is_some() && self.offsets.is_some()
	}

	pub fn create(&mut self) -> Amalgam {
		assert!(self.is_complete());
		Amalgam {
			preset: self.preset.take().unwrap(),
			jobs: self.jobs.take().unwrap(),
			items: self.items.take().unwrap(),
			filters: self.filters.take().unwrap(),
			offsets: self.offsets.take().unwrap()
		}
	}

	/*
	 * A preset may be edited and things may be removed.
	 * It's easier to just remove usages for removed mechanics
	 * at runtime and hope the user resaves the correct data than
	 * it is to manually run an update on all saved timelines.
	 *
	 * Offsets (or lack thereof) also need to be validated.
	 * A timeline can be incomplete initially and phases added
	 * later, so missing data needs to be added to prevent OOB errors.
	 */
	pub fn sanitize(&mut self, encounter: &Encounter) {
		/*
		 * Adding missing offsets. Extras aren't removed because we can't (currently)
		 * tell which offset corresponds to which phase to know which to remove.
		 */
		if let Some(offsets) = &mut self.offsets {
			if offsets.len() < encounter.phases.len() {
				offsets.resize(encounter.phases.len(), Default::default());
			}
		}

		// removing usages on mechanics that no longer exist
		if let Some(columns) = &mut self.items {
			let mut set = HashSet::<u128>::with_capacity(encounter.phases.len() * 16);
			for phase in &encounter.phases {
				for mechanic in &phase.mechanics {
					set.insert(mechanic.id.hash);
				}
			}

			for items in columns {
				items.retain(|item| set.contains(&item.mechanic.hash));
			}
		}
	}
}

pub type PartialAmalgamRef = Rc<PartialAmalgam>;
