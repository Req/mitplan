
use std::collections::{
	HashMap,
	HashSet
};
use serde::{
	Deserialize,
	Serialize
};
use wasm_bindgen::{
	closure::Closure,
	JsCast,
	JsValue
};
use yew_agent::{
	Agent,
	AgentLink,
	Context,
	HandlerId
};

/*
TODO
- in app, check current route when loading
- have readonly delegate to this
- have saving delegate to this
- implement pushing/popping history state
*/

#[derive(Clone, Copy, Debug, Deserialize, PartialEq, Serialize)]
pub enum Route {
	Index,
	Edit(u32),
	ReadOnly(u32)
}

impl Default for Route {
	fn default() -> Self {
		Self::Index
	}
}

impl Route {
	pub fn id(&self) -> Option<u32> {
		match self {
			Self::Index => None,
			Self::Edit(id) => Some(*id),
			Self::ReadOnly(id) => Some(*id)
		}
	}

	pub fn url(&self) -> String {
		match self {
			Self::Index => String::from("/"),
			Self::Edit(id) => format!("/edit/{}", id),
			Self::ReadOnly(id) => format!("/readonly/{}", id)
		}
	}

	pub fn go_to(&self) {
		let url = self.url();
		web_sys::window().unwrap().document().unwrap().location().unwrap().set_href(&url).unwrap();
	}

	pub fn push(&self) {
		let history = web_sys::window().unwrap().history().unwrap();
		history.push_state_with_url(
			// not actually used for anything when popped, but may as well keep it
			&JsValue::from_serde(self).unwrap(),
			"",
			Some(&self.url())
		).unwrap();
	}
}

#[derive(Debug, PartialEq)]
pub struct Path {
	pub href: String,
	pub origin: String,
	pub path: String,
	pub hash: String,
	pub args: HashMap<String, String>
}

impl Path {
	#[cfg(target_arch = "wasm32")]
	pub fn current() -> Path {
		let location = &web_sys::window().unwrap().document().unwrap().location().unwrap();
		let href = location.href().unwrap();
		let origin = location.origin().unwrap();
		let path = location.pathname().unwrap();
		let hash = location.hash().unwrap().trim_start_matches('#').to_string();

		let mut args = HashMap::new();
		let search = location.search().unwrap();
		for segment in search.trim_start_matches('?').split('&') {
			if !segment.is_empty() {
				let parts = segment.split('=').collect::<Vec<&str>>();
				match parts.len() {
					1 => args.insert(parts[0].to_string(), String::new()),
					2 => args.insert(parts[0].to_string(), parts[1].to_string()),
					_ => None
				};
			}
		}

		Path {
			href,
			origin,
			path,
			hash,
			args
		}
	}

	#[cfg(not(target_arch = "wasm32"))]
	pub fn current() -> Path {
		Path {
			href: String::from("http://localhost"),
			origin: String::from("localhost"),
			path: String::new(),
			hash: String::new(),
			args: HashMap::new()
		}
	}

	pub fn route(&self) -> Option<Route> {
		let path = &self.path[1..];
		if path.is_empty() {
			return if self.hash.is_empty() && self.args.is_empty() {
				Some(Route::Index)
			} else {
				None
			};
		}

		if let Some(index) = path.find('/') {
			let mode = &path[..index];
			if let Ok(id) = path[index + 1..].parse() {
				return match mode {
					"edit" => Some(Route::Edit(id)),
					"readonly" => Some(Route::ReadOnly(id)),
					_ => None
				};
			}
		}
		None
	}
}

pub enum RouteBusIntent {
	/**
	 * Consider this to have the effect of loading the default page.
	 */
	New,
	/**
	 * Does *not* result in a route event firing.
	 */
	ChangeId(u32),
	ToggleReadOnly,
	Subscribe
}

#[derive(Clone, Copy)]
pub enum RouteBusEvent {
	/**
	 * Fired upon subscription. Subscribers may or may not
	 * take action, as this event doesn't indicate the route
	 * actually changing.
	 */
	CurrentRoute(Route),
	/**
	 * Fired upon the route (and thus the page url) changing.
	 * Subscribers should take action, if applicable.
	 */
	RouteChanged(Route),
	/**
	 * Fired in response to ChangeId. Subscribers should take
	 * note of the new ID but not perform any serious action.
	 */
	IdChanged(u32)
}

pub enum RouteBusMessage {
	ChangeRoute(Route)
}

pub struct RouteBus {
	link: AgentLink<RouteBus>,
	route: Route,
	subscribers: HashSet<HandlerId>,
	_on_pop_state: Option<Closure<dyn FnMut(&JsValue)>>
}

impl RouteBus {
	fn tell_subscribers(&self, event: RouteBusEvent) {
		for sub in &self.subscribers {
			if sub.is_respondable() {
				self.link.respond(*sub, event);
			}
		}
	}
}

impl Agent for RouteBus {
	type Reach = Context<Self>;
	type Message = RouteBusMessage;
	type Input = RouteBusIntent;
	type Output = RouteBusEvent;

	fn create(link: AgentLink<Self>) -> Self {
		match Path::current().route() {
			Some(route) => {
				let notifier = link.callback(Self::Message::ChangeRoute);
				let callback = Closure::wrap(Box::new(move |_: &JsValue| {
					notifier.emit(Path::current().route().unwrap_or_default());
				}) as Box<dyn FnMut(&JsValue)>);
				web_sys::window().unwrap().set_onpopstate(Some(callback.as_ref().unchecked_ref()));

				Self {
					link,
					route,
					subscribers: HashSet::new(),
					_on_pop_state: Some(callback)
				}
			},
			None => {
				link.send_input(Self::Input::New);
				// this is getting clobbered shortly
				Self {
					link,
					route: Route::default(),
					subscribers: HashSet::new(),
					_on_pop_state: None
				}
			}
		}
	}

	fn update(&mut self, msg: Self::Message) {
		match msg {
			Self::Message::ChangeRoute(route) => {
				if matches!(route, Route::Index) || route != self.route {
					self.tell_subscribers(Self::Output::RouteChanged(route));
				}
				self.route = route;
			}
		}
	}

	fn handle_input(&mut self, msg: Self::Input, id: HandlerId) {
		match msg {
			Self::Input::New => Route::Index.go_to(),
			Self::Input::ChangeId(id) => {
				if self.route.id() != Some(id) {
					self.route = Route::Edit(id);
					self.route.push();
					self.tell_subscribers(RouteBusEvent::IdChanged(id));
				}
			},
			Self::Input::ToggleReadOnly => {
				match self.route {
					Route::Edit(id) => {
						self.route = Route::ReadOnly(id);
						self.route.push();
					},
					Route::ReadOnly(id) => {
						self.route = Route::Edit(id);
						self.route.push();
					},
					Route::Index => {}
				}
			},
			Self::Input::Subscribe => {
				self.subscribers.insert(id);
				self.link.respond(id, RouteBusEvent::CurrentRoute(self.route));
			}
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn parse_route_from_path() {
		let path = Path {
			href: String::from("https://domain.tld/"),
			origin: String::from("https://domain.tld"),
			path: String::from("/"),
			hash: String::new(),
			args: HashMap::new()
		};
		assert_eq!(path.route(), Some(Route::Index));

		let path = Path {
			href: String::from("https://domain.tld/edit/5"),
			origin: String::from("https://domain.tld"),
			path: String::from("/edit/5"),
			hash: String::new(),
			args: HashMap::new()
		};
		assert_eq!(path.route(), Some(Route::Edit(5)));

		let path = Path {
			href: String::from("https://domain.tld/readonly/5"),
			origin: String::from("https://domain.tld"),
			path: String::from("/readonly/5"),
			hash: String::new(),
			args: HashMap::new()
		};
		assert_eq!(path.route(), Some(Route::ReadOnly(5)));
	}
}
