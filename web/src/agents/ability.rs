
use std::{
	collections::HashSet,
	rc::Rc
};
use yew_agent::{
	Agent,
	AgentLink,
	Bridge,
	Bridged,
	Context,
	Dispatched,
	HandlerId
};
use mitplan_shared::{
	amalgam::{
		MechanicId,
		TimelineItem
	},
	job::Job
};
use crate::{
	ability::{
		Ability,
		set_ability_resolver_sync_level
	},
	ability_manager::{
		AbilityManager,
		Identity
	},
	agents::amalgam::{
		AmalgamBus,
		AmalgamBusEvent,
		AmalgamBusIntent
	}
};

pub enum AbilityBusIntent {
	// chains into Event::Refresh for the originating subscriber
	Refresh,
	RefreshColumn(usize),
	RefreshAll,
	Clear,
	ChangeJob(usize, Job),
	ChangeSync(u32),
	ChangeFilter(usize, bool),
	ToggleAbility(Identity, &'static Ability, i32),
	ShiftAfterBy(MechanicId, i32, i32),
	AmalgamBridgeEvent(AmalgamBusEvent),
	LoadFromImpl(Option<[Vec<TimelineItem>; 8]>, Option<[Job; 8]>)
}

pub enum AbilityBusEvent {
	Refresh(&'static AbilityManager, Option<usize>)
}

pub struct AbilityBus {
	link: AgentLink<AbilityBus>,
	subscribers: HashSet<HandlerId>,
	manager: AbilityManager,
	amalgam_bridge: Box<dyn Bridge<AmalgamBus>>
}

impl AbilityBus {
	/**
	 * Passing references around via the bus obviously requires lifetimes.
	 * Agent is defined as 'static, so the manager contained in AbilityBus
	 * is effectively 'static', but Agent cannot (or appears to be unable to,
	 * but maybe I'm bad at lifetimes) be constrained by a lifetime parameter.
	 * This is a wrapper to allow access to the manager as 'static.
	 */
	 fn static_manager(&self) -> &'static AbilityManager {
		unsafe {
			std::mem::transmute(&self.manager)
		}
	}
}

impl Agent for AbilityBus {
	type Reach = Context<Self>;
	type Message = ();
	type Input = AbilityBusIntent;
	type Output = AbilityBusEvent;

	fn create(link: AgentLink<Self>) -> Self {
		let amalgam_bridge = AmalgamBus::bridge(link.callback(|event| AbilityBus::dispatcher().send(AbilityBusIntent::AmalgamBridgeEvent(event))));
		Self {
			link,
			subscribers: HashSet::new(),
			manager: AbilityManager::new(),
			amalgam_bridge
		}
	}

	fn update(&mut self, _msg: Self::Message) {}

	fn handle_input(&mut self, msg: Self::Input, id: HandlerId) {
		match msg {
			Self::Input::Refresh => self.link.respond(id, Self::Output::Refresh(self.static_manager(), None)),
			Self::Input::RefreshColumn(index) => {
				let manager = self.static_manager();
				for sub in self.subscribers.iter() {
					if sub.is_respondable() {
						self.link.respond(*sub, Self::Output::Refresh(manager, Some(index)));
					}
				}
			},
			Self::Input::RefreshAll => {
				let manager = self.static_manager();
				for sub in self.subscribers.iter() {
					if sub.is_respondable() {
						self.link.respond(*sub, Self::Output::Refresh(manager, None));
					}
				}
			},
			Self::Input::Clear => self.manager.clear(),
			Self::Input::ChangeJob(index, job) => {
				if self.manager.change_job(index, job) {
					self.link.send_input(Self::Input::RefreshColumn(index));
				}
			},
			Self::Input::ChangeSync(sync) => {
				if self.manager.change_sync(sync) {
					set_ability_resolver_sync_level(sync);
					self.link.send_input(Self::Input::RefreshAll);
				}
			},
			Self::Input::ChangeFilter(index, filter) => {
				if self.manager.change_filter(index, filter) {
					self.link.send_input(Self::Input::RefreshAll);
				}
			},
			Self::Input::ToggleAbility(identity, ability, time) => {
				let success = if self.manager.is_used_at(identity, ability) {
					self.manager.try_removing(identity, ability)
				} else {
					self.manager.try_using(identity, ability, time)
				};
				if success {
					self.link.send_input(Self::Input::RefreshColumn(identity.0));
				}
			},
			Self::Input::ShiftAfterBy(id, from, to) => {
				self.manager.shift_after_by(id, from, to);
				/*
				 * The timeline updating (and rerendering) will cause Cell::change to send Refresh,
				 * should cause all changes to propagate correctly as long as the rerender is done
				 * after this message propagates, but assuming that would require knowledge of how
				 * Yew delegates to web workers (which I don't have). Instead, we're forcing a
				 * probably-unnecessary update upon every Cell.
				 */
				self.link.send_input(Self::Input::RefreshAll);
			},
			Self::Input::AmalgamBridgeEvent(event) => match event {
				AmalgamBusEvent::BeginCompile(mut partial) => {
					let (filters, items) = self.manager.dump();
					let partial = unsafe { Rc::get_mut_unchecked(&mut partial) };
					partial.filters = Some(filters);
					partial.items = Some(items);
					self.amalgam_bridge.send(AmalgamBusIntent::NotifyPartial);
				},
				AmalgamBusEvent::LoadFrom(mut partial) => {
					let partial = unsafe { Rc::get_mut_unchecked(&mut partial) };
					self.link.send_input(Self::Input::LoadFromImpl(partial.items.take(), partial.jobs))
				},
				AmalgamBusEvent::Compiled(_, _) => {}
			},
			Self::Input::LoadFromImpl(items, jobs) => {
				if let Some(jobs) = jobs {
					for (index, job) in jobs.iter().enumerate() {
						self.manager.change_job(index, *job);
					}
				}
				if let Some(items) = items {
					self.manager.restore(items);
				}
				self.link.send_input(Self::Input::RefreshAll);
			}
		}
	}

	fn connected(&mut self, id: HandlerId) {
		self.subscribers.insert(id);
	}

	fn disconnected(&mut self, id: HandlerId) {
		self.subscribers.remove(&id);
	}
}
