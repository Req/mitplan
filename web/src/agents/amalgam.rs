
use std::{
	collections::HashSet,
	rc::Rc
};
use yew_agent::{
	Agent,
	AgentLink,
	Context,
	HandlerId
};
use mitplan_shared::amalgam::Amalgam;
pub use crate::agents::amalgam_partial::*;

pub enum ApiIntent {
	Copy,
	Save,
	Delete
}

pub enum AmalgamBusIntent {
	Compile(ApiIntent),
	NotifyPartial,
	PresetChanged(String),
	Load(Box<PartialAmalgam>)
}

pub enum AmalgamBusEvent {
	BeginCompile(PartialAmalgamRef),
	Compiled(Box<Amalgam>, ApiIntent),
	LoadFrom(PartialAmalgamRef)
}

pub struct AmalgamBus {
	link: AgentLink<AmalgamBus>,
	subscribers: HashSet<HandlerId>,
	requester: Option<(HandlerId, ApiIntent)>,
	partial: PartialAmalgamRef,
	preset_name: String
}

impl Agent for AmalgamBus {
	type Reach = Context<Self>;
	type Message = ();
	type Input = AmalgamBusIntent;
	type Output = AmalgamBusEvent;

	fn create(link: AgentLink<Self>) -> Self {
		Self {
			link,
			subscribers: HashSet::new(),
			partial: PartialAmalgamRef::default(),
			requester: None,
			preset_name: String::new()
		}
	}

	fn update(&mut self, _msg: Self::Message) {}

	fn handle_input(&mut self, msg: Self::Input, id: HandlerId) {
		match msg {
			Self::Input::Compile(intent) => {
				self.requester = Some((id, intent));
				self.partial = PartialAmalgamRef::from(PartialAmalgam {
					preset: Some(self.preset_name.clone()),
					..Default::default()
				});
				for sub in &self.subscribers {
					if sub.is_respondable() {
						self.link.respond(*sub, Self::Output::BeginCompile(self.partial.clone()));
					}
				}
			},
			Self::Input::Load(mut amalgam) => {
				self.preset_name = amalgam.preset.take().unwrap();
				self.partial = PartialAmalgamRef::from(amalgam);
				for sub in &self.subscribers {
					if sub.is_respondable() {
						self.link.respond(*sub, Self::Output::LoadFrom(self.partial.clone()));
					}
				}
			},
			Self::Input::PresetChanged(name) => self.preset_name = name,
			Self::Input::NotifyPartial => {
				if self.partial.is_complete() {
					if let Some(partial) = Rc::get_mut(&mut self.partial) {
						let amalgam = partial.create();
						let requester = self.requester.take().unwrap();
						self.link.respond(requester.0, Self::Output::Compiled(Box::new(amalgam), requester.1));
					}
				}
			}
		}
	}

	fn connected(&mut self, id: HandlerId) {
		self.subscribers.insert(id);
	}

	fn disconnected(&mut self, id: HandlerId) {
		self.subscribers.remove(&id);
	}
}
