
use std::{
	cmp::Ordering,
	string::ToString,
};
use crate::jobs::MAX_LEVEL;

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum DamageType {
	Physical,
	Magical,
	Any
}

#[derive(Clone, Debug)]
pub enum Effect {
	DamageReduction { required_type: DamageType },
	HealingBonus,
	Shield,
	Heal
}

// shortcuts to reduce verbosity in ability definitions
pub const PHYSICAL_DAMAGE_REDUCTION: Effect = Effect::DamageReduction {
	required_type: DamageType::Physical
};
pub const MAGICAL_DAMAGE_REDUCTION: Effect = Effect::DamageReduction {
	required_type: DamageType::Magical
};
pub const ALL_DAMAGE_REDUCTION: Effect = Effect::DamageReduction {
	required_type: DamageType::Any
};

#[repr(u8)]
#[derive(Clone, Copy, Debug, PartialEq, PartialOrd)]
pub enum Category {
	Default = 0,
	HealerThroughput = 1,
	Personal = 2
}
pub static CATEGORIES: [Category; 3] = [
	Category::Default,
	Category::HealerThroughput,
	Category::Personal
];

impl ToString for Category {
	fn to_string(&self) -> String {
		match self {
			Self::Default => "Default".to_string(),
			Self::HealerThroughput => "Healer Throughput".to_string(),
			Self::Personal => "Personal".to_string()
		}
	}
}

#[derive(Clone, Debug)]
pub enum Syncable<T> {
	Always(T),
	Traited(T, u32, T),
	DoubleTraited(T, u32, T, u32, T)
}

static mut CURRENT_SYNC: u32 = MAX_LEVEL;

pub fn set_ability_resolver_sync_level(sync: u32) {
	unsafe {
		CURRENT_SYNC = sync;
	}
}

fn current_sync() -> u32 {
	unsafe {
		CURRENT_SYNC
	}
}

impl<T> Syncable<T> {
	fn resolve(&self) -> &T {
		match self {
			Self::Always(value) => value,
			Self::Traited(before, breakpoint, after) => {
				if current_sync() < *breakpoint {
					before
				} else {
					after
				}
			},
			Self::DoubleTraited(base, break1, mid, break2, last) => {
				let sync = current_sync();
				if sync > *break2 {
					last
				} else if sync > *break1 {
					mid
				} else {
					base
				}
			},
		}
	}
}

#[derive(Clone, Debug)]
pub struct Ability {
	pub name: Syncable<&'static str>,
	// role abilities have a requirement of 1 because syncing doesn't disable them
	pub level: u32,
	pub cd: Syncable<u32>,
	pub effects: Syncable<Vec<Effect>>,
	pub requires_enemy: bool,
	/*
	 * Instant things like indom have a duration of 1s.
	 * Things with multiple phases, such as veil (pre-/post-popped) and horoscope (helios'd)
	 * have a duration equal to the sum of the phase's durations.
	 * Example: normal horoscope lasts for 10s and horoscope helios lasts for 30s, so it has a duration of 40s.
	 */
	pub duration: Syncable<i32>,
	pub charges: Syncable<u32>,
	// this could change when synced, but i'd like to keep it in a consistent category for users
	pub category: Category
}

impl Ability {
	pub fn name(&self) -> &'static str {
		self.name.resolve()
	}

	pub fn cd(&self) -> u32 {
		*self.cd.resolve()
	}

	pub fn effects(&self) -> &Vec<Effect> {
		self.effects.resolve()
	}

	pub fn charges(&self) -> u32 {
		*self.charges.resolve()
	}

	pub fn duration(&self) -> i32 {
		*self.duration.resolve()
	}
}

impl PartialEq for Ability {
	fn eq(&self, other: &Self) -> bool {
		self.name.resolve() == other.name.resolve()
	}
}

// abilities are sorted primarily by category so that categories stay grouped together in the ui
impl PartialOrd for Ability {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		if let Some(ordering) = self.category.partial_cmp(&other.category) {
			return match ordering {
				Ordering::Equal => self.name.resolve().partial_cmp(other.name.resolve()),
				other => Some(other)
			};
		}
		unreachable!();
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use crate::jobs::pld::SHELTRON;

	#[test]
	fn traiting_obeys_sync() {
		set_ability_resolver_sync_level(70);
		assert_eq!(SHELTRON.duration(), 5);
		set_ability_resolver_sync_level(80);
		assert_eq!(SHELTRON.duration(), 6);
		set_ability_resolver_sync_level(90);
		assert_eq!(SHELTRON.duration(), 8);
	}
}
