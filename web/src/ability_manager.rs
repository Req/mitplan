
use std::{
	cmp::{
		Ord,
		Ordering
	},
	collections::{
		HashMap,
		HashSet
	}
};
use mitplan_shared::{
	amalgam::{
		MechanicId,
		TimelineItem
	},
	job::Job
};
use crate::{
	ability::Ability,
	jobs::{
		ABILITIES,
		MAX_LEVEL
	},
	recharge::{
		Period,
		Tracker
	}
};

#[derive(Clone, Copy, Debug, PartialEq)]
pub enum State {
	Selected(i32),
	Available(Period),
	// contains the earliest available period
	Unavailable(Period)
}

pub type AbilityState = (&'static Ability, State);

#[derive(Clone, Copy, Debug, Eq)]
pub struct Usage {
	mechanic: MechanicId,
	time: i32
}

impl PartialEq for Usage {
	fn eq(&self, other: &Self) -> bool {
		self.time == other.time
	}
}

impl PartialOrd for Usage {
	fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
		self.time.partial_cmp(&other.time)
	}
}

impl Ord for Usage {
	fn cmp(&self, other: &Self) -> Ordering {
		self.partial_cmp(other).unwrap()
	}
}

#[derive(Debug)]
struct AbilityManagerImpl {
	ability: &'static Ability,
	// likely to be rather small, so linear searching is fine and potentially optimal
	timeline: Vec<Usage>,
	availability: Vec<Period>
}

impl AbilityManagerImpl {
	fn new(ability: &'static Ability) -> Self {
		Self {
			ability,
			timeline: vec![],
			availability: vec![(i32::MIN, i32::MAX)]
		}
	}

	fn shift_after_by(&mut self, id: MechanicId, from: i32, to: i32) {
		if self.timeline.is_empty() {
			return;
		}

		let delta = to - from;
		if delta < 0 {
			// removing clipped usages, exclusive of any on the lower bound
			self.timeline.retain(|usage| !(to < usage.time && usage.time <= from));
		}

		/*
		 * This could be done more efficiently in a way that doesn't require rebuilding the cache every time.
		 * Doing so would significantly complicate the code, and I would rather this somewhat-complicated
		 * operation be as easy to maintain as possible.
		 */
		let mut new = Self::new(self.ability);
		for usage in &self.timeline {
			if usage.mechanic <= id {
				new.try_using(usage.mechanic, usage.time);
			} else {
				new.try_using(usage.mechanic, usage.time + delta);
			}
		}
		*self = new;
	}

	fn try_using(&mut self, mechanic: MechanicId, time: i32) -> bool {
		if !self.is_available(time) {
			return false;
		}

		let usage = Usage {
			mechanic,
			time
		};
		// an insertion sort probably be better but afaik no std implementation exists
		self.timeline.insert(self.timeline.binary_search(&usage).unwrap_err(), usage);
		self.rebuild_cache();
		true
	}

	fn is_used_at(&self, mechanic: MechanicId) -> bool {
		self.timeline.iter().any(|usage| usage.mechanic == mechanic)
	}

	fn usage_at(&self, mechanic: MechanicId) -> Option<i32> {
		self.timeline.iter().find(|usage| usage.mechanic == mechanic).map(|usage| usage.time)
	}

	fn try_removing(&mut self, mechanic: MechanicId) -> bool {
		match self.timeline.iter().position(|usage| usage.mechanic == mechanic) {
			Some(index) => {
				self.timeline.remove(index);
				self.rebuild_cache();
				true
			},
			None => false
		}
	}

	fn is_available(&self, time: i32) -> bool {
		self.available_period_at(time).is_ok()
	}

	/**
	 * @return Success(the period for which the ability is available at this time) or
	 * Err(the period before the ability became unavailable at this time)
	 */
	fn available_period_at(&self, time: i32) -> Result<Period, Period> {
		let mut last: Period = (i32::MIN, i32::MIN);
		for period in &self.availability {
			if period.0 <= time && time <= period.1 {
				return Ok(*period);
			}
			if period.0 < time && period.1 < time {
				last = *period;
			}
		}
		Err(last)
	}

	fn rebuild_cache(&mut self) {
		let mut tracker = Tracker::begin(self.ability.charges(), self.ability.cd());
		for usage in &self.timeline {
			tracker.use_at(usage.time);
		}
		self.availability = tracker.end();


		let mut markers = Vec::<i32>::with_capacity((self.timeline.len() + 1) * 2);
		markers.push(i32::MIN);
		let cd = self.ability.cd() as i32;
		for usage in &self.timeline {
			markers.push(usage.time - cd);
			markers.push(usage.time + cd);
		}
		markers.push(i32::MAX);
	}
}

/**
 * The timeline sets parameters for the manager.
 *
 * Listeners connect to the manager. The manager informs them of their state,
 * and they request the manager to change their state.
 *
 * This creates a authority structure of timeline > manager > listeners.
 *
 * Internally, this is a wrapper for many AbilityManagerImpls that
 * provides a centralized interface to communicate with them.
 */
pub struct AbilityManager {
	// for each column (representing a job), there is a map of (ability name, ability manager impl)
	impls: [HashMap<&'static str, AbilityManagerImpl>; 8],
	jobs: [Job; 8],
	filters: [bool; 3],
	sync: u32
}

pub type Identity = (usize, MechanicId);

pub const DEFAULT_FILTERS: [bool; 3] = [true, false, false];

pub type PartialDump = Vec<TimelineItem>;
pub type StateDump = ([bool; 3], [PartialDump; 8]);

impl AbilityManager {
	pub fn new() -> Self {
		Self {
			impls: [HashMap::new(), HashMap::new(), HashMap::new(), HashMap::new(), HashMap::new(), HashMap::new(), HashMap::new(), HashMap::new()],
			jobs: [Job::DUM; 8],
			sync: MAX_LEVEL,
			filters: DEFAULT_FILTERS
		}
	}

	fn dump_index(&self, index: usize) -> PartialDump {
		let mut usages: PartialDump = vec![];
		for (ability, manager) in &self.impls[index] {
			usages.reserve(manager.timeline.len());
			for usage in &manager.timeline {
				usages.push(TimelineItem {
					ability: ability.to_string(),
					mechanic: usage.mechanic,
					time: usage.time
				});
			}
		}
		usages
	}

	pub fn dump(&self) -> StateDump {
		// wtb collect<[T; N]>()
		(self.filters, [
			self.dump_index(0),
			self.dump_index(1),
			self.dump_index(2),
			self.dump_index(3),
			self.dump_index(4),
			self.dump_index(5),
			self.dump_index(6),
			self.dump_index(7)
		])
	}

	pub fn restore(&mut self, dumps: [PartialDump; 8]) {
		for (index, dump) in dumps.iter().enumerate() {
			for item in dump {
				if let Some(imp) = self.impls[index].get_mut(&*item.ability) {
					imp.try_using(item.mechanic, item.time);
				}
			}
		}
	}

	pub fn clear(&mut self) {
		for set in &mut self.impls {
			for imp in &mut set.values_mut() {
				imp.timeline.clear();
				imp.rebuild_cache();
			}
		}
	}

	pub fn shift_after_by(&mut self, id: MechanicId, from: i32, to: i32) {
		for set in &mut self.impls {
			for imp in set.values_mut() {
				imp.shift_after_by(id, from, to);
			}
		}
	}

	pub fn change_job(&mut self, index: usize, job: Job) -> bool {
		assert!(index < 8);
		if self.jobs[index] != job {
			self.jobs[index] = job;

			let mut new_ability_map = HashSet::<&'static str>::new();
			let abilities = ABILITIES.get(&job).unwrap();
			for ability in abilities {
				new_ability_map.insert(ability.name());
			}

			let map = &mut self.impls[index];
			/*
			 * Only removing abilities that don't exist for the new job.
			 * This lets the user, for example, swap melee and keep Feint assignments.
			 */
			map.retain(|ability, _| {
				new_ability_map.contains(ability)
			});
			for ability in abilities {
				map.try_insert(ability.name(), AbilityManagerImpl::new(ability)).ok();
			}

			return true;
		}
		false
	}

	pub fn change_filter(&mut self, index: usize, filter: bool) -> bool {
		assert!(index < 8);
		if self.filters[index] != filter {
			self.filters[index] = filter;
			return true;
		}
		false
	}

	pub fn change_sync(&mut self, sync: u32) -> bool {
		if self.sync != sync {
			self.sync = sync;
			return true;
		}
		false
	}

	#[allow(unused)]
	#[cfg(test)]
	fn impl_for(&self, identity: Identity, ability: &Ability) -> Option<&AbilityManagerImpl> {
		self.impls[identity.0].get(ability.name())
	}

	pub fn try_using(&mut self, identity: Identity, ability: &Ability, time: i32) -> bool {
		match self.impls[identity.0].get_mut(ability.name()) {
			Some(imp) => imp.try_using(identity.1, time),
			None => false
		}
	}

	pub fn try_removing(&mut self, identity: Identity, ability: &Ability) -> bool {
		match self.impls[identity.0].get_mut(ability.name()) {
			Some(imp) => imp.try_removing(identity.1),
			None => false
		}
	}

	#[allow(unused)]
	pub fn is_available(&self, identity: Identity, ability: &Ability, time: i32) -> bool {
		match self.impls[identity.0].get(ability.name()) {
			Some(imp) => imp.is_available(time),
			None => false
		}
	}

	pub fn is_used_at(&self, identity: Identity, ability: &Ability) -> bool {
		match self.impls[identity.0].get(ability.name()) {
			Some(imp) => imp.is_used_at(identity.1),
			None => false
		}
	}

	pub fn request_state(&self, identity: Identity, time: i32) -> Vec<AbilityState> {
		let mut states = self.impls[identity.0].values().filter_map(|manager| {
			let ability = manager.ability;
			if ability.level > self.sync || !self.filters[ability.category as usize] {
				return None;
			}
			if let Some(time) = manager.usage_at(identity.1) {
				return Some((ability, State::Selected(time)));
			}
			match manager.available_period_at(time) {
				Ok(period) => Some((ability, State::Available(period))),
				Err(period) => Some((ability, State::Unavailable(period)))
			}
		}).collect::<Vec<AbilityState>>();
		states.sort_by(|lhs, rhs| lhs.0.partial_cmp(rhs.0).unwrap());
		states
	}
}

#[cfg(test)]
mod tests {
	use super::*;
	use mitplan_shared::{
		amalgam::{
			Encounter,
			Mechanic,
			Phase
		},
		job::Job
	};
	use crate::{
		ability::Category,
		jobs::{
			sch::SOIL,
			pld::INTERVENTION,
			MAX_LEVEL
		}
	};

	fn new_ex(sync: u32, jobs: Vec<Job>) -> AbilityManager {
		let mut this = AbilityManager::new();
		this.change_sync(sync);
		for (index, job) in jobs.iter().enumerate() {
			assert!(index < 8);
			this.change_job(index, *job);
		}
		this
	}

	fn batch_mechanics(phase: &str, count: usize, start_time: i32) -> Vec<Mechanic> {
		let mut x = start_time as u32 / 5;
		let mut generator = move || {
			x += 1;
			x
		};
		(0..count).map(|index| {
			let time = start_time + index as i32 * 5;
			let name = time.to_string();
			Mechanic {
				id: MechanicId::create(phase, &name, time, &mut generator),
				name,
				time: time,
				tags: vec![]
			}
		}).collect()
	}

	lazy_static! {
		/*
		 * While this encounter has dummy mechanics, they
		 * are mostly unused. Typically, the first one is used
		 * in all cases and time offsets are instead used to
		 * make tests cleaner. This is """legal""" until
		 * usages are being removed, at which point it
		 * creates ambiguity.
		 */
		static ref ENCOUNTER: Encounter = Encounter {
			sync: MAX_LEVEL,
			phases: vec![
				Phase {
					name: "1".to_string(),
					enrage: 30,
					mechanics: batch_mechanics("1", 30 / 5, 0)
				},
				Phase {
					name: "2".to_string(),
					enrage: 60,
					mechanics: batch_mechanics("2", 60 / 5, 30)
				}
			]
		};
	}

	#[test]
	fn basic_availability_check() {
		let mut manager = new_ex(80, vec![Job::PLD]);
		let identity: Identity = (0, ENCOUNTER.phases[0].mechanics[0].id);
		// no usage, so should be available
		assert!(manager.is_available(identity, &*INTERVENTION, 0));
		assert!(manager.try_using(identity, &*INTERVENTION, 0));
		// should no longer be available
		assert!(!manager.is_available(identity, &*INTERVENTION, 0));
		assert!(!manager.is_available(identity, &*INTERVENTION, 9));
		assert!(!manager.is_available(identity, &*INTERVENTION, -9));
		// cd should be up
		assert!(manager.is_available(identity, &*INTERVENTION, 10));
		assert!(manager.is_available(identity, &*INTERVENTION, -10));
		// edge case checking
		assert!(manager.is_available(identity, &*INTERVENTION, i32::MIN));
		assert!(manager.is_available(identity, &*INTERVENTION, i32::MAX));
	}

	#[test]
	fn multiple_usages() {
		let mut manager = new_ex(80, vec![Job::PLD, Job::SCH]);

		let identity: Identity = (0, ENCOUNTER.phases[0].mechanics[0].id);
		assert!(manager.try_using(identity, &*INTERVENTION, 20));
		assert!(manager.try_using(identity, &*INTERVENTION, 50));
		assert!(manager.is_available(identity, &*INTERVENTION, 5));
		assert!(!manager.is_available(identity, &*INTERVENTION, 21));
		assert!(manager.is_available(identity, &*INTERVENTION, 35));
		assert!(!manager.is_available(identity, &*INTERVENTION, 49));
		assert!(manager.is_available(identity, &*INTERVENTION, 65));

		// >2 back-to-back usages
		assert!(manager.try_using(identity, &*INTERVENTION, 30));
		assert!(manager.try_using(identity, &*INTERVENTION, 40));

		// a previous bug involving multiple usages with time between them, due to how marker matching worked
		let identity: Identity = (1, identity.1);
		assert!(manager.try_using(identity, &*SOIL, 35));
		assert!(manager.try_using(identity, &*SOIL, 111));
		assert!(manager.try_using(identity, &*SOIL, 74));
		assert!(!manager.is_available(identity, &*SOIL, 71));
		assert!(!manager.is_available(identity, &*SOIL, 80));
	}

	#[test]
	fn removing_usages() {
		let mut manager = new_ex(80, vec![Job::PLD]);
		assert!(manager.try_using((0, ENCOUNTER.phases[1].mechanics[2].id), &*INTERVENTION, ENCOUNTER.phases[1].mechanics[2].time));
		assert!(manager.try_using((0, ENCOUNTER.phases[1].mechanics[4].id), &*INTERVENTION, ENCOUNTER.phases[1].mechanics[4].time));
		assert!(manager.try_using((0, ENCOUNTER.phases[1].mechanics[6].id), &*INTERVENTION, ENCOUNTER.phases[1].mechanics[6].time));
		assert!(manager.try_removing((0, ENCOUNTER.phases[1].mechanics[4].id), &*INTERVENTION));
		assert!(manager.is_available((0, ENCOUNTER.phases[1].mechanics[4].id), &*INTERVENTION, 0));
	}

	#[test]
	fn state() {
		let mut manager = new_ex(80, vec![Job::PLD, Job::SCH]);
		manager.change_filter(Category::Personal as usize, true);
		assert!(manager.try_using((0, ENCOUNTER.phases[1].mechanics[4].id), &*INTERVENTION, ENCOUNTER.phases[1].mechanics[4].time));

		// should be selected for the mechanic it was just used at
		let state = manager.request_state((0, ENCOUNTER.phases[1].mechanics[4].id), ENCOUNTER.phases[1].mechanics[4].time);
		match state.iter().find(|(ability, _)| *ability == &*INTERVENTION) {
			Some((_, state)) => assert_eq!(*state, State::Selected(ENCOUNTER.phases[1].mechanics[4].time)),
			None => assert!(false)
		}

		// should be unavailable just before the usage
		let state = manager.request_state((0, ENCOUNTER.phases[1].mechanics[3].id), ENCOUNTER.phases[1].mechanics[3].time);
		match state.iter().find(|(ability, _)| *ability == &*INTERVENTION) {
			Some((_, state)) => assert!(matches!(*state, State::Unavailable { .. })),
			None => assert!(false)
		}

		// should be available long before the usage
		let state = manager.request_state((0, ENCOUNTER.phases[1].mechanics[0].id), ENCOUNTER.phases[1].mechanics[0].time);
		match state.iter().find(|(ability, _)| *ability == &*INTERVENTION) {
			Some((_, state)) => assert!(matches!(*state, State::Available { .. })),
			None => assert!(false)
		}
	}

	#[test]
	fn end_shifting() {
		let mut manager = new_ex(80, vec![Job::PLD]);
		manager.change_filter(Category::Personal as usize, true);

		// at 25s, p1 (which ends at 30s)
		assert!(manager.try_using((0, ENCOUNTER.phases[0].mechanics[5].id), &*INTERVENTION, ENCOUNTER.phases[0].mechanics[5].time));
		// at 35s, p2
		assert!(manager.try_using((0, ENCOUNTER.phases[1].mechanics[1].id), &*INTERVENTION, ENCOUNTER.phases[1].mechanics[1].time));
		// should fail to reapply second usage
		manager.shift_after_by(ENCOUNTER.phases[0].mechanics[5].id, 30, 29);
		assert!(manager.is_used_at((0, ENCOUNTER.phases[0].mechanics[5].id), &*INTERVENTION));
		assert!(!manager.is_used_at((0, ENCOUNTER.phases[1].mechanics[1].id), &*INTERVENTION));

		// same setup
		let mut manager = new_ex(80, vec![Job::PLD]);
		manager.change_filter(Category::Personal as usize, true);
		assert!(manager.try_using((0, ENCOUNTER.phases[0].mechanics[5].id), &*INTERVENTION, ENCOUNTER.phases[0].mechanics[5].time));
		assert!(manager.try_using((0, ENCOUNTER.phases[1].mechanics[1].id), &*INTERVENTION, ENCOUNTER.phases[1].mechanics[1].time));
		// should remove first usage because the shift passes over it
		manager.shift_after_by(ENCOUNTER.phases[0].mechanics[5].id, 30, 24);
		assert!(!manager.is_used_at((0, ENCOUNTER.phases[0].mechanics[5].id), &*INTERVENTION));
		assert!(manager.is_used_at((0, ENCOUNTER.phases[1].mechanics[1].id), &*INTERVENTION));

		// same setup
		let mut manager = new_ex(80, vec![Job::PLD]);
		manager.change_filter(Category::Personal as usize, true);
		assert!(manager.try_using((0, ENCOUNTER.phases[0].mechanics[5].id), &*INTERVENTION, ENCOUNTER.phases[0].mechanics[5].time));
		assert!(manager.try_using((0, ENCOUNTER.phases[1].mechanics[1].id), &*INTERVENTION, ENCOUNTER.phases[1].mechanics[1].time));
		// should keep both usages when extending
		manager.shift_after_by(ENCOUNTER.phases[0].mechanics[5].id, 30, 31);
		assert!(manager.is_used_at((0, ENCOUNTER.phases[0].mechanics[5].id), &*INTERVENTION));
		assert!(manager.is_used_at((0, ENCOUNTER.phases[1].mechanics[1].id), &*INTERVENTION));
	}
}
